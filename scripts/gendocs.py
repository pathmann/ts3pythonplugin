#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from optparse import OptionParser
import sys, os

VERBOSE = None

def vprint(str):
    if VERBOSE:
        print(str)

def processFile(filename, outdir):
    vprint("Extracting from %s" % filename)

    docs = ""

    with open(filename, "r") as inf:
        incom = False
        for line in inf:
            if not incom and line.strip() == "/*":
                incom = True
            elif incom and line.strip() == "*/":
                incom = False
            elif incom:
                docs += line

    with open(os.path.join(outdir, "".join(os.path.basename(filename).split(".")[:-1]) + ".py"), "w") as outf:
        outf.write(docs)


if __name__ == "__main__":
    optparser = OptionParser("gendocse.py [Options] file1 file2 ...")
    optparser.add_option("-o", "--output", dest="outputdir", default="./", help="The directory where outfiles should be placed in")
    optparser.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="verbosely message output")

    (options, args) = optparser.parse_args()

    VERBOSE = options.verbose

    if not os.path.isdir(options.outputdir):
        vprint("Creating output directory")
        os.mkdir(options.outputdir)

    for file in args:
        processFile(file, options.outputdir)
