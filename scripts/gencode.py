#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys, os
from optparse import OptionParser
import simplec
import time

VERBOSE = None

DEFINES_FILE_EXCLUSIONS = ["ts3_functions.h", "public_errors.h", "public_errors_rare.h"]
DEFINES_EXCLUSIONS = []

FUNCTION_EXCLUSIONS = ["ts3plugin_name", "ts3plugin_version", "ts3plugin_apiVersion", "ts3plugin_author",
"ts3plugin_description", "ts3plugin_setFunctionPointers", "ts3plugin_init", "ts3plugin_shutdown",
"ts3plugin_offersConfigure", "ts3plugin_configure", "ts3plugin_registerPluginID", "ts3plugin_commandKeyword",
"ts3plugin_infoTitle", "ts3plugin_freeMemory", "ts3plugin_requestAutoload", "ts3plugin_infoData", "ts3plugin_initMenus",
"ts3plugin_initHotkeys", "ts3plugin_onMenuItemEvent", "getHotkeyFromKeyword", "requestHotkeyInputDialog", "showHotkeySetup"]

MODULE_EXCLUSIONS = ["freeMemory", "setPluginMenuEnabled"]

PLUGIN_EXCLUSIONS = ["ts3plugin_name", "ts3plugin_version", "ts3plugin_apiVersion", "ts3plugin_author", "ts3plugin_description", "ts3plugin_setFunctionPointers",
"ts3plugin_init", "ts3plugin_shutdown", "ts3plugin_offersConfigure", "ts3plugin_configure", "ts3plugin_registerPluginID", "ts3plugin_commandKeyword", "ts3plugin_processCommand", 
"ts3plugin_infoTitle", "ts3plugin_infoData", "ts3plugin_freeMemory", "ts3plugin_requestAutoload", "ts3plugin_initMenus", "ts3plugin_initHotkeys", "ts3plugin_onEditPlaybackVoiceDataEvent",
"ts3plugin_onEditPostProcessVoiceDataEvent", "ts3plugin_onEditMixedPlaybackVoiceDataEvent", "ts3plugin_onEditCapturedVoiceDataEvent", "ts3plugin_onCustom3dRolloffCalculationClientEvent",
"ts3plugin_onCustom3dRolloffCalculationWaveEvent", "ts3plugin_onMenuItemEvent", "ts3plugin_onHotkeyEvent", "ts3plugin_onHotkeyRecordedEvent"]


def vprint(str):
    if VERBOSE:
        print(str)

def writeFileByTemplate(tplfile, vars, outdir):
    outfname = os.path.join(outdir, os.path.basename(tplfile)[:-4])
    with open(tplfile, "r") as f:
        tpl = f.read()

        for key in vars:
            tpl = tpl.replace("$$%s$$" % key, vars[key])

        with open(outfname, "w") as f:
            f.write(tpl)

def toCoreType(ctype, name):
    ctype = ctype.strip()

    if ctype == "uint64":
        return "(unsigned long long)%s" % name
    elif ctype == "anyID":
        return "(unsigned int)%s" % name
    elif ctype == "float":
        return "(double)%s" % name
    elif ctype.startswith("enum"):
        return "(int)%s" % name
    else:
        return name

def paramFormatString(ctype):
    ctype = ctype.strip()

    if ctype == "uint64":
        return "K"
    elif ctype == "unsigned int":
        return "I"
    elif ctype == "char*" or ctype == "const char*":
        return "s"
    elif ctype == "anyID":
        return "I"
    elif ctype == "int":
        return "i"
    elif ctype == "int*":
        return "ai"
    elif ctype == "float":
        return "d"
    elif ctype == "float*":
        return "af"
    elif ctype == "short*":
        return "ah"
    elif ctype == "unsigned int*" or ctype == "const unsigned int*":
        return "aI"
    elif "enum" in ctype.split(" "):
        return "i"
    else:
        raise Exception("Unknown parameter type %s" % ctype)

def pluginFuncBody(symbol):
    ret = ""
    params = ", ".join(toCoreType(x[0], x[1]) for x in symbol.params)
    format = "".join(paramFormatString(x[0]) for x in symbol.params)
    fname = symbol.name.replace("ts3plugin_", "")

    #add pydocstrings
    ret += "  /*\n    %s\n        \"\"\"\n\n" % symbol.toPython(method = True)
    for p in symbol.params:
      ret += "        @param %s: \n        @type %s: \n" % (simplec.prepareNameForPython(p[1]), simplec.prepareNameForPython(p[1]))
    if symbol.returntype != "void":
        ret += "        @return: \n        @rtype: \n"
    ret += "        \"\"\"\n  */\n"

    if symbol.returntype == "void":
        ret += "  bool callret;\n  QString callerror;\n"
        ret += "  if (!PythonHost::instance()->call(\"%s\", &callret, callerror, \"%s\", %s))\n" % (fname, format, params)
        ret += "    ts3logdispatcher::instance()->add(QObject::tr(\"Calling %s failed with \\\"%%1\\\"\").arg(callerror), false, true, LogLevel_WARNING);\n" % fname
    elif symbol.returntype == "int":
        ret += "  bool callret;\n  QString callerror;\n"
        ret += "  if (!PythonHost::instance()->call(\"%s\", &callret, callerror, \"%s\", %s)) {\n" % (fname, format, params)
        ret += "    ts3logdispatcher::instance()->add(QObject::tr(\"Calling %s failed with \\\"%%1\\\"\").arg(callerror), false, true, LogLevel_WARNING);\n" % fname
        ret += "    return 0;\n  }\n"
        ret += "  else return callret ? 1 : 0;\n"

    return ret

def generatePluginSource(indir, outdir, tpldir):
    date = time.strftime("%Y-%m-%d_%H-%M-%S")
    exfuncs = {}

    #generate auto_plugin.h auto_plugin.cpp
    filenameh = os.path.join(outdir, "auto_plugin.h")
    filenamecpp = os.path.join(outdir, "auto_plugin.cpp")
    bacnameh = os.path.join(outdir, "auto_plugin-%s.h" % date)
    bacnamecpp = os.path.join(outdir, "auto_plugin-%s.cpp" % date)

    if os.path.exists(filenameh):
        os.rename(filenameh, bacnameh)
        vprint("Existing header moved to %s" % bacnameh)

    if os.path.exists(filenamecpp):
        os.rename(filenamecpp, bacnamecpp)
        vprint("Existing source moved to %s" % bacnamecpp)

    vprint("Loading existing auto_plugin.cpp")
    if os.path.exists(filenamecpp):
        exfuncs = {x.name: x for x in simplec.getSymbols([filenamecpp]) if type(x) == simplec.function}
        vprint("Done.")
    else:
        vprint("None found.")

    vprint("Generating export plugin functions ...")
    symbs = simplec.getSymbols([os.path.join(indir, "src", "plugin.h")])

    header = ""
    source = ""

    for s in symbs:
        if type(s) == simplec.function and not s.name in PLUGIN_EXCLUSIONS:
            header += s.declaration() + "\n"

            #remove decorator from returntype
            s.returntype = s.returntype.replace("PLUGINS_EXPORTDLL", "").strip()

            #if function already existed, and its body is not empty
            if s.name in exfuncs and exfuncs[s.name].body.strip() != "":
                #if the declaration is still the same, use the old function
                if s == exfuncs[s.name]:
                    source += exfuncs[s.name].definition() + "\n\n"
                    continue
                else:
                    #print the old function as comment first
                    source += "/*\n%s\n*/\n" % exfuncs[s.name].definition()

            source += s.definition(pluginFuncBody(s)) + "\n\n"

    writeFileByTemplate(os.path.join(tpldir, "auto_plugin.h.tpl"), {"FUNCTIONS": header}, outdir)
    vprint("Header written.")

    writeFileByTemplate(os.path.join(tpldir, "auto_plugin.cpp.tpl"), {"FUNCTIONS": source}, outdir)
    vprint("Source written.")
    
def generateModule(indir, outdir, tpldir):
    date = time.strftime("%Y-%m-%d_%H-%M-%S")
    filenamecpp = os.path.join(outdir, "ts3module.cpp")
    filenameh = os.path.join(outdir, "ts3module.h")
    bacnamecpp = os.path.join(outdir, "ts3module-%s.cpp" % date)
    bacnameh = os.path.join(outdir, "ts3module-%s.h" % date)
    exfuncs = {}

    vprint("Loading existing ts3 cpp module ...")
    if os.path.exists(filenamecpp):
        exfuncs = {x.name: x for x in simplec.getSymbols([filenamecpp]) if type(x) == simplec.function}
        vprint("Done.")

        os.rename(filenamecpp, bacnamecpp)
        vprint("Existing source file moved to %s" % bacnamecpp)
    else:
        vprint("None found.")

    if os.path.exists(filenameh):
        os.rename(filenameh, bacnameh)
        vprint("Existsing header file moved to %s" % bacnameh)

    vprint("Generating ts3 cpp module")
    symbs = simplec.getSymbols([os.path.join(indir, "include", "ts3_functions.h")])

    if len(symbs) != 1:
        print("Error, ts3_functions.h should only return the TS3Functions-struct symbol, got %d symbols" % len(symbs))
        sys.exit(1)

    if type(symbs[0]) != simplec.struct or symbs[0].name != "TS3Functions":
        print("Error, ts3_functions.h should only return the TS3Functions-struct symbol, got type %s with name %s" % (type(symbs[0]), symbs[0].name))
        sys.exit(1)

    decls = ""
    methtable = "static PyMethodDef ts3modfuncs[] = {\n"
    defs = ""

    for s in symbs[0].childs:
        if type(s) == simplec.functionpointer and not s.name in MODULE_EXCLUSIONS:
            #function declaration
            decls += "PyObject* %s(PyObject* self, PyObject* args);\n" % s.name

            #methodtable
            methtable += "  {\"%s\", %s, METH_VARARGS, \"Call %s of the client plugin sdk\"},\n" % (s.name, s.name, s.name)

            if s.name in exfuncs and exfuncs[s.name].body.strip() != "":
                if exfuncs[s.name].body.strip().split("\n")[0].strip() == "//%s" % s.declaration():
                    defs += exfuncs[s.name].definition() + "\n\n"
                    exfuncs.pop(s.name)
                    continue
                else:
                    defs += "/*\n%s\n*/\n" % exfuncs[s.name].definition()

            if s.name in exfuncs:
                exfuncs.pop(s.name)
            defs += "PyObject* %s(PyObject* self, PyObject* args) {\n" % s.name
            defs += "  //%s\n" % s.declaration()
            defs += "  /*\n    @staticmethod\n    %s\n        \"\"\"\n\n" % s.toPython(method = False)
            for p in s.params:
                defs += "        @param %s: \n        @type %s: \n" % (simplec.prepareNameForPython(p[1]), simplec.prepareNameForPython(p[1]))
            defs += "        @return: \n        @rtype: \n"
            defs += "    \"\"\"\n  */\n"
            defs += "\n}\n\n"

    for key in exfuncs:
        s = exfuncs[key]

        decls += s.declaration()

        if s.returntype == "PyObject*" and len(s.params) == 2 and s.params[0][0] == "PyObject*" and s.params[1][0] == "PyObject*":
            methtable += "  {\"%s\", %s, METH_VARARGS, \"Call %s\"},\n" % (s.name, s.name, s.name)
            defs += "%s\n\n" % s.definition()

    methtable += "  {NULL, NULL, 0, NULL}\n};\n"

    writeFileByTemplate(os.path.join(tpldir, "ts3module.h.tpl"), {"FUNCTIONS": decls}, outdir)
    vprint("Header written.")

    writeFileByTemplate(os.path.join(tpldir, "ts3module.cpp.tpl"), {"FUNC_ARRAY": methtable, "FUNCTIONS": defs}, outdir)
    vprint("Source written.")

def generateCpp(indir, outdir, tpldir):
    vprint("Generating CPP code ...")
    generatePluginSource(indir, outdir, tpldir)
    generateModule(indir, outdir, tpldir)
    
def generateDefines(incldir, outdir, tpldir):
    #generate ts3defines.py
    vprint("Generating ts3defines.py ...")
    symbs = []

    symbs = simplec.getSymbols([os.path.join(incldir, file) for file in os.listdir(incldir) if file[-2:] == ".h" and not os.path.basename(file) in DEFINES_FILE_EXCLUSIONS])

    defs = ""
    for s in symbs:
        if not type(s) in [simplec.function, simplec.struct, simplec.functionpointer] and not s.name in DEFINES_EXCLUSIONS:
            defs += s.toPython() + "\n"

    writeFileByTemplate(os.path.join(tpldir, "ts3defines.py.tpl"), {"DEFINES": defs}, outdir)
    vprint("Done.")
    
def generateErrors(incldir, outdir):
    #generate ts3errors.py
    vprint("Generating ts3errors.py ...")
    symbs = simplec.getSymbols([os.path.join(incldir, file) for file in ["public_errors.h", "public_errors_rare.h"]])
    #symbs = filter(lambda x: type(x) == simplec.const, symbs)

    with open(os.path.join(outdir, "ts3errors.py"), "w") as of:
        for s in symbs:
            if type(s) == simplec.const:
                of.write("%s\n" % s.toPython())

    vprint("Done.")

def generatePlugin(indir, outdir, tpldir):
    vprint("Generating plugin.py ...")
    symbs = simplec.getSymbols([os.path.join(indir, "src", "plugin.h")])

    host = ""
    plugin = ""

    for f in symbs:
        if type(f) == simplec.function and not f.name in FUNCTION_EXCLUSIONS:
            fname = f.name.replace("ts3plugin_", "")

            host += "    @staticmethod\n"
            host += "    %s\n" % f.toPython(method = False).replace("ts3plugin_", "")
            host += "        for p in PluginHost.running:\n"
            host += "            p.%s(%s)\n\n" % (fname, ", ".join(simplec.prepareNameForPython(x[1]) for x in f.params))

            plugin += "    %s\n        pass\n" % f.toPython(method = True).replace("ts3plugin_", "")

    writeFileByTemplate(os.path.join(tpldir, "ts3plugin.py.tpl"), {"HOST_FUNCTIONS": host, "PLUGIN_FUNCTIONS": plugin}, outdir)

def generatePython(indir, outdir, tpldir):
    vprint("Generating python code ...")
    incldir = os.path.join(indir, "include")

    generateDefines(incldir, outdir, tpldir)
    generateErrors(incldir, outdir)
    generatePlugin(indir, outdir, tpldir)

def generate(indir, outdir, tpldir):
    generatePython(indir, outdir, tpldir)
    generateCpp(indir, outdir, tpldir)
    
if __name__ == "__main__":
    optparser = OptionParser("genpythonmodule.py [Options] <ts3_plugin_sdk_dir>")
    optparser.add_option("-o", "--output", dest="outputdir", default="./", help="The directory where outfiles should be placed in")
    optparser.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False, help="verbosely message output")

    (options, args) = optparser.parse_args()

    if len(args) != 1 or not os.path.isdir(args[0]):
        print("You need to pass the directory of the desired ts3 plugin sdk")
        optparser.print_help()
        sys.exit(1)
    
    #check for template files
    curdir = os.path.abspath(os.path.dirname(sys.argv[0]))
    for f in ["auto_plugin.cpp.tpl", "auto_plugin.h.tpl", "ts3module.cpp.tpl", "ts3module.h.tpl", "ts3plugin.py.tpl"]:
        if not os.path.isfile(os.path.join(curdir, f)):
            print("Template %s does not exist, aborting." % f)
            sys.exit(1)

    VERBOSE = options.verbose
    
    if not os.path.isdir(options.outputdir):
        vprint("Creating output directory")
        os.mkdir(options.outputdir)
    
    generate(args[0], options.outputdir, curdir)

