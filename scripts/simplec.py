"""
This python module reads C or C++ source files and represents the symbols in simple python classes.
This module has currently more restrictions than features, but it covers my current needs.
"""

import abc
import re
import tempfile
import os

class ValidationError(Exception):
    """
    Exception that will be raised, if the constructor of a c-type fails
    """
    pass

class IncompleteError(Exception):
    """
    Exception that will be raised, if an output method of a c-type is called, but the initiation is incomplete
    """

class UnknownSymbolError(Exception):
    """
    Exception that will be raised, if some symbol is tried to resolve in a class, which does not match
    """

def is_int(i):
    """
    @param i:
    @type i:
    @return:
    @rtype:
    """
    try:
        int(i)
        return True
    except:
        return False

def functionParamsToList(svars):
    """
    Returns the string containing the parameters to a list
    @param svars: comma separated list of parameters
    @type svars: string
    @return: The parameter list as list of tuples (type, name)
    @rtype: list of tuples (string, string)
    """
    if svars.strip() == "":
        return []
    else:
        return [(" ".join(x.split(" ")[:-1]), x.split(" ")[-1]) for x in svars.split(",") if x != ""]

def functionParamsToString(lvars):
    """
    Returns the string containing the list of parameters
    @param lvars: The parameter list as list of tuples (type, name)
    @type lvars: list of tuples (string, string)
    @return: comma separated list of parameters
    @rtype: string
    """
    return ",".join(["%s %s" % (x[0], x[1]) for x in lvars])

def prepareNameForPython(name):
    """
    
    """
    if name.strip() in ["type", "class", "object", "def", "self", "id", "pass"]:
        return "p%s" % name
    else:
        return name

class ctype(object):
    """
    Baseclass of all c-types.
    """
    __metaclass__ = abc.ABCMeta
    
    keyword = ""
    """Every subclass needs to define a static attribute keyword, which needs to be contained in the first line of the specific c-type to make the validation faster,
    leave empty, if not possible"""
    regex = [r""]
    """Every subclass needs to define a static attribute regex listing patterns matching the specific c-type"""

    @abc.abstractmethod
    def __init__(self, line):
        """
        @param line: The (first) line of code of the c-type
        @type line: string
        @raise ValidationError: If the constructor can't validate the line
        """
        self.resolveSymbols = []
    
    def needsResolving(self):
        return len(self.resolveSymbols) != 0
    
    @abc.abstractmethod
    def wantMore(self):
        """
        @return: Returns true, if the c-type needs more lines of source code
        @rtype: Boolean
        """
    
    @abc.abstractmethod
    def doMore(self, line):
        """
        Add next line of source code to the object.
        @param line: next line of code of the c-type
        @type line: string
        @return: returns true, if line was skipped, false otherwise
        @rtype: Boolean
        """
    
    @abc.abstractmethod
    def definition(self, body = ""):
        """
        @param body:
        @type body: string
        @return: Returns a string representing the c-definition
        @rtype: string
        """
        if not self.completed:
            raise IncompleteError("%s is incomplete: %s( %s )" % (self.__class__, self, self.__dict__))
    
    @abc.abstractmethod
    def declaration(self):
        """
        @return: Returns a string representing the c-declaration
        @rtype: Boolean
        """
        if not self.completed:
            raise IncompleteError("%s is incomplete: %s( %s )" % (self.__class__, self, self.__dict__))
    
    @abc.abstractmethod
    def toPython(self, method = True, forcenative = False):
        """
        @param method: If true, imported function will have a self parameter, only important in class function
        @type method: Boolean
        @param forcenative:
        @type forcenative: Boolean
        @return: Returns a string representing a python-representation
        @rtype: Boolean
        """
        if not self.completed:
            raise IncompleteError("%s is incomplete: %s( %s )" % (self.__class__, self, self.__dict__))
    
    @abc.abstractmethod
    def __eq__(self, t2):
        """
        @return: Returns true if self represents the same ctype like t2
        @rtype: Boolean
        """
        if not self.completed:
            raise IncompleteError("%s is incomplete: %s( %s )" % (self.__class__, self, self.__dict__))
    
    @abc.abstractmethod
    def resolve(self, symbol, value):
        """
        @param symbol: 
        @type symbol: string
        @param value:
        @type value:
        """
    
    @abc.abstractmethod
    def knowSymbol(self, symbol):
        """
        @param symbol:
        @type symbol: string
        @return:
        @rtype: Boolean
        """
    
    @abc.abstractmethod
    def getSymbolValue(self, symbol):
        """
        @param symbol:
        @type symbol: string
        @return:
        @rtype:
        """
    

class const(ctype):
    """
    Class representing a C constant.
    """
    
    keyword = ""
    
    regex = [
    r"^\s*const\s+((?:[A-Za-z]+\s*)+\*?)\s+(\w+)\s*;.*$",
    r"^\s*const\s+((?:[A-Za-z]+\s*)+\*?)\s+(\w+)\s+=\s*(\w+)\s*;.*$"
    ]
    
    def __init__(self, line):
        ctype.__init__(self, line)
        
        m = re.match(self.regex[0], line)
        
        if m:
            self.type = m.group(1).strip()
            self.name = m.group(2).strip()
            self.value = 0
        else:
            m = re.match(self.regex[1], line)
            
            if m:
                self.type = m.group(1).strip()
                self.name = m.group(2).strip()
                self.value = m.group(3).strip()
            else:
                raise ValidationError("No valid const string")
    
    def wantMore(self):
        return False
    
    def doMore(self, line):
        raise ValidationError("No extra lines needed")
    
    def definition(self, body = ""):
        return "const %s %s = %s;" % (self.type, self.name, self.value)
    
    def declaration(self):
        return "const %s %s;" % (self.type, self.name)
    
    def toPython(self, method = True, forcenative = False):
        return "%s = %s" % (self.name, self.value)
    
    def __eq__(self, c2):
        return (self.name == c2.name) and (self.type == c2.type) and (self.value == c2.value)
    
    def resolve(self, symbol, value):
        if not self.needsResolving():
            raise UnknownSymbol("No symbol resolving needed")
        else:
            self.value = self.value.replace(symbol, value)
            self.resolveSymbols.remove(symbol)
            
            if not self.needsResolving():
                self.value = eval(self.value)
    
    def knowSymbol(self, symbol):
        return self.name == symbol
    
    def getSymbolValue(self, symbol):
        if self.name != symbol:
            raise UnknownSymbolError("Unknown symbol")
        else:
            return self.value

class enum(ctype):
    """
    Class representing a C enum.
    """
    
    #can't pass 'enum', what if an enum is passed as function parameter?
    keyword = ""
    
    regex = [
    r"^\s*enum\s+(\w+)\s*;$",
    r"^\s*typedef\s+enum\s+(\w+)\s*;$",
    r"^\s*enum\s+(\w+)\s*{\s*$",
    r"^\s*typedef\s+enum\s*{\s*$",
    r"^\s*enum\s*{\s*$"
    ]
    
    def __init__(self, line):
        ctype.__init__(self, line)
        
        self.completed = False
        self.enums = []
        
        m = re.match(self.regex[0], line)
        if m:
            self.name = m.group(1)
            self.completed = True
            self.typedef = False
            return
        
        m = re.match(self.regex[1], line)
        if m:
            self.name = m.group(1)
            self.completed = True
            self.typedef = True
            return
        
        m = re.match(self.regex[2], line)
        if m:
            self.name = m.group(1).strip()
            self.typedef = False
            return
        
        if re.match(self.regex[3], line) != None:
            self.typedef = True
            self.name = ""
        elif re.match(self.regex[4], line) != None:
            self.name = ""            
        else:
            raise ValidationError("No valid enum")
    
    def wantMore(self):
        return not self.completed
    
    def doMore(self, line):
        #skip empty lines
        if line.strip() == "":
            return True
            
        if re.match("^\s*}\s*;\s*$", line) != None:
            self.completed = True
            return False
        
        m = re.match("^\s*}\s*((?:\w+\s*));\s*$", line)
        if m:
            if self.name == "":
                #take the first variable as name
                self.name = m.group(1).strip().split(",")[0].strip()
            
            self.completed = True
            return False
        
        #ignore #ifdef, #ifndef, #else and #endif
        m = re.match(r"^\s*(\w*\s*=?\s*\w*,?)\s*#(?:ifn?def|endif|else).*$", line)
        if m:
            if m.group(1).strip() != "":
                return self.doMore(m.group(1).strip())
            else:
               return True
        
        #is it a comment?
        m = re.match(r"^\s*(\w*\s*=?\s*\w*,?)\s*//.*$", line)
        if m:
            if m.group(1).strip() != "":
                return self.doMore(m.group(1).strip())
            else:
                return True
        
        m = re.match(r"^\s*(\w*\s*=?\s*\w*,?)\s*/\*.*\*/\s*$", line)
        if m:
            if m.group(1).strip() != "":
                return self.doMore(m.group(1).strip())
            else:
                return True
        
        m = re.match(r"^\s*}\s*(\w+)\s*;\s*$", line)
        if m:
            self.name = m.group(1)
            self.completed = True
            return False
        
        m = re.match(r"^\s*(\w+)\s*=\s*(\w+)\s*,?\s*$", line)
        if m:
            if is_int(m.group(2)):
                self.enums.append((m.group(1).strip(), int(m.group(2))))
            else:
                self.enums.append((m.group(1).strip(), m.group(2).strip()))
                
                #try to eval it
                try:
                    rvar = int(eval(self.enums[-1][1]))
                    self.enums[-1] = (self.enums[-1][0], rvar)
                except NameError:
                    self.resolveSymbols.append(m.group(2).strip())
                    
            return False
        
        m = re.match(r"^\s*(\w+)\s*,?\s*$", line)
        if m:
            if len(self.enums) == 0:
                self.enums.append((m.group(1).strip(), 0))
            else:
                if is_int(self.enums[-1][1]):
                    self.enums.append((m.group(1), int(self.enums[-1][1]) +1))
                else:
                    self.enums.append((m.group(1), "%s +1" % self.enums[-1][1]))
            
            return False
        else:
            raise ValidationError("No valid enum in %s" % line)
        
        return True
    
    def definition(self, body = ""):
        ctype.definition(self)
        
        if self.name == "":
            self.createName()
        
        if self.typedef:
            pref = "typedef enum {"
            suff = "} %s;" % self.name
        else:
            pref = "enum %s {" % self.name
            suff = "};"
        
        str = ""
        for e in self.enums:
            str += "%s = %s,\n" % (e[0], e[1])
        
        return "%s\n%s\n%s" % (pref, str.strip(), suff)
    
    def declaration(self):
        ctype.declaration(self)
        
        if self.name == "":
            self.createName()
        
        if self.typedef:
            return "typedef enum %s;" % self.name
        else:
            return "enum %s;" % self.name
    
    def toPython(self, method = True, forcenative = False):
        ctype.toPython(self, method, forcenative)
        
        if len(self.enums) == 1 and not forcenative:
            return "%s = %s" % (self.enums[0][0], self.enums[0][1])
        
        if self.name == "":
            self.createName()
        
        str = "class %s:\n" % self.name
        if len(self.enums) == 0:
            str += "    pass\n"
        else:
            for e in self.enums:
                str += "    %s = %s\n" % (e[0], e[1])
        
        return str
    
    def __eq__(self, e2):
        ctype.__eq__(self, e2)
        
        return (self.name == e2.name) and (self.enums == e2.enums)
    
    def createName(self):
        if len(self.enums) == 1:
            self.name = "%s_enum" % self.enums[0][0]                
        else:
            #substr = longest_common_substring(self.enums[0][0], self.enums[1][0])
            substr = os.path.commonprefix([e[0] for e in self.enums])
                
            #remove special characters at the end
            stop = False
            name = ""
            for c in substr[::-1]:
                if not c in "_-%!,.:;" or stop:
                    name += c
                else:
                    stop = True

            if len(name) > 5:
                self.name = name[::-1]
            else:
                self.name = "%s_enum" % self.enums[0][0]
    
    def resolve(self, symbol, value):
        if not symbol in self.resolveSymbols:
            raise UnknownSymbolError("Don't need to resolve this symbol")
        else:        
            for i, e in enumerate(self.enums):
                if symbol in e[1]:
                    self.enums[i] = (e[0], e[1].replace(symbol, str(value)))
            
            self.resolveSymbols.remove(symbol)
            
            if not self.needsResolving():
                for i, e in enumerate(self.enums):
                    if not is_int(e[1]):
                        self.enums[i] = (e[0], int(eval(e[1])))
    
    def knowSymbol(self, symbol):
        for e in self.enums:
            if e[0] == symbol:
                return True
        
        return False
    
    def getSymbolValue(self, symbol):
        for e in self.enums:
            if e[0] == symbol:
                return e[1]
        
        raise UnknownSymbolError("Unknown symbol")

class function(ctype):
    """
    Class representing a C function.
    """
    
    keyword = ""
    
    regex = [
    r"^\s*([\w\s]+\*?)\s+(\w+)\s*\((.*)\)\s*;\s*$",
    r"^\s*([\w\s]+\*?)\s+(\w+)\s*\((.*)\)\s*{\s*$"
    ]

    def __init__(self, line):
        ctype.__init__(self, line)
        
        self.params = []
        self.body = ""
        
        m = re.match(self.regex[0], line)
        if m:
            self.returntype = m.group(1).strip()
            self.name = m.group(2).strip()
            self.params = functionParamsToList(m.group(3))
            self.completed = True
            
            return
        
        m = re.match(self.regex[1], line)
        if m:
            self.returntype = m.group(1).strip()
            self.name = m.group(2).strip()
            self.params = functionParamsToList(m.group(3))
            self.completed = False
            self.balance = 0
        else:
            raise ValidationError("No valid function")
    
    def wantMore(self):
        return not self.completed
    
    def doMore(self, line):
        if re.match(r"^\s*}\s*$", line) != None:
            if self.balance == 0:
                self.completed = True
                
                return False
        
        self.body += "%s\n" % line
        
        if "{" in line:
            self.balance += 1
        if "}" in line:
            self.balance -= 1
        
        return False
    
    def definition(self, body = ""):
        ctype.definition(self)
        
        pref = "%s %s(%s) {" % (self.returntype, self.name, functionParamsToString(self.params))
        if body != "":
            return "%s\n%s}" % (pref, body)
        else:
            return "%s\n%s}" % (pref, self.body)
    
    def declaration(self):
        ctype.declaration(self)
        
        return "%s %s(%s);" % (self.returntype, self.name, functionParamsToString(self.params))
    
    def toPython(self, method = True, forcenative = False):
        ctype.toPython(self, method, forcenative)
        
        if method:
            if len(self.params) == 0:
                return "def %s(self):" % self.name
            else:
                return "def %s(self, %s):" % (self.name, ", ".join([prepareNameForPython(x[1]) for x in self.params]))
        else:
            if len(self.params) == 0:
                return "def %s():" % self.name
            else:
                return "def %s(%s):" % (self.name, ", ".join([prepareNameForPython(x[1]) for x in self.params]))
    
    def __eq__(self, f2):
        """
        Compares self to a second function object f2. Only name, returntype and parameters will be compared, NOT the body
        @param f2: A second function object
        @type f2: function
        @return: Returns True, if equal
        @rtype: Boolean
        """
        ctype.__eq__(self, f2)
        
        return (self.name == f2.name) and (self.returntype == f2.returntype) and (self.params == f2.params)
    
    def knowSymbol(self, symbol):
        return False
    
    def getSymbolValue(self, symbol):
        raise UnknownSymbolError("Don't know this symbol")
    
    def resolve(self, symbol, value):
        raise UnknownSymbolError("Don't know this symbol")

class macro(ctype):
    """
    class representing a preprocessor macro (#define)
    """
    
    keyword = ""
    
    regex = [
    r"^\s*#define\s+(\w+)\s+([\w\*]+)\s*$", #asterisk to match constructs like 1024*8
    r"^\s*#define\s+(\w+)\s+\"(\w+)\"\s*$"
    ]
    
    def __init__(self, line):
        ctype.__init__(self, line)
        
        m = re.match(self.regex[0], line)
        if m:
            self.name = m.group(1).strip()
            self.value = m.group(2).strip()
            
            if not is_int(self.value):
                #try to eval it
                try:
                    self.value = int(eval(self.value))
                except NameError:                
                    #could be a string
                    if re.match(r"^\".*\"$", self.value) == None:
                        self.resolveSymbols.append(self.value)
        else:
            raise ValidationError("No valid macro")
    
    def wantMore(self):
        return False
    
    def doMore(self, line):
        raise ValidationError("No extra lines needed")
    
    def definition(self, body = ""):
        return "#define %s %s" % (self.name, self.value)
    
    def declaration(self):
        return self.definition()
    
    def toPython(self, method = True, forcenative = False):
        return "%s = %s" % (self.name, self.value)
    
    def __eq__(self, t2):
        return (self.name == t2.name) and (self.value == t2.value) 

    def resolve(self, symbol, value):
        if not self.needsResolving():
            raise UnknownSymbol("No symbol resolving needed")
        else:
            self.value = self.value.replace(symbol, value)
            self.resolveSymbols.remove(symbol)
            
            if not self.needsResolving():
                self.value = eval(self.value)
    
    def knowSymbol(self, symbol):
        return self.name == symbol
    
    def getSymbolValue(self, symbol):
        if self.name != symbol:
            raise UnknownSymbolError("Unknown symbol")
        else:
            return self.value

class struct(ctype):
    """
    
    """
    
    #can't pass 'struct', what if some struct is a function parameter?
    keyword = ""
    
    regex = [
    r"^\s*struct\s+(\w+)\s*{\s*.*$",
    r"^\s*typedef\s+struct\s*{\s*.*$"
    ]
    
    def __init__(self, line):
        ctype.__init__(self, line)
        
        self.completed = False
        self.balance = 0
        #self.inunion = False
        self.childs = []
        
        m = re.match(self.regex[0], line)
        if m:
            self.name = m.group(1).strip()
            self.typedef = False
            return
        
        m = re.match(self.regex[1], line)
        if m:
            self.name = ""
            self.typedef = True
        else:
            raise ValidationError("No valid struct")
        
    
    def wantMore(self):        
        return not self.completed
    
    def doMore(self, line):
        line = line.strip()
        
        if line == "":
            return True

        if len(self.childs) > 0:
            if self.childs[-1].wantMore():
                return self.childs[-1].doMore(line)

        if re.match(r"^}\s*;$", line) != None:
            #if self.inunion:
            #    self.inunion = False
            #else:
            if self.balance == 0:
                self.completed = True
                return False
            else:
                self.balance -= 1
                return True
        
        m = re.match(r"^}\s*(\w+)\s*;$", line)
        if m:
            if self.balance == 0:
                if self.typedef:
                    self.name = m.group(1).strip()
                self.completed = True
                return False
            else:
                return True
        
        if "}" in line:
            self.balance -= 1
        if "{" in line:
            self.balance += 1
        
        #is it a comment?
        m = re.match(r"^(\w*\s*=?\s*\w*,?)\s*//.*$", line)
        if m:
            if m.group(1).strip() != "":
                return self.doMore(m.group(1).strip())
            else:
                return True

        try:
            fp = functionpointer(line)
        except:
            return True

        self.childs.append(fp)
        return False
        
        #if we want to match more structs than TS3Functions, keep on working here ;)
        """
        m = re.match(r"^\s*}\s*(\w+)\s*;\s*$", line)
        if m:
            if self.inunion:
                raise ValidationError("")
            elif self.typedef or self.name == "":
                self.name = m.group(1).strip()
            
            self.completed = True
            return False
                
        return True
        """
        
    def definition(self, body = ""):
        ctype.definition(self)
        
        d = "struct %s {\n" % self.name
        for c in self.childs:
            d += "%s\n" % c.declaration()
        d += "};\n"
        return d
    
    def declaration(self):
        ctype.declaration(self)
        
        if self.typedef:
            return "typedef struct %s;" % self.name
        else:
            return "struct %s;" % self.name
    
    def toPython(self, method = True, forcenative = False):
        ctype.toPython(self, method, forcenative)
        
        s = "class %s:\n" % self.name
        for c in self.childs:
            if type(c) == functionpointer:
                s += "\t%s\n\t\tpass\n" % c.tyPython
        
        return s
    
    def __eq__(self, s2):
        ctyp.__eq__(self, s2)
    
    def resolve(self, symbol, value):
        raise UnknownSymbolError("Don't know this symbol")
    
    def knowSymbol(self, symbol):
        return False
    
    def getSymbolValue(self, symbol):
        raise UnknownSymbolError("Don't know this symbol")
        
class functionpointer(ctype):
    """
    Class representing a C function pointer.
    """
    
    keyword = ""
    
    regex = [
    r"^\s*([\w\s]+\*?)\s+\(\s*\*\s*(\w+)\s*\)\s*\((.*)\)\s*;\s*.*$",
    r"^\s*([\w\s]+\*?)\s+\(\s*\*\s*(\w+)\s*\)\s*\((.*)\s*$"
    ]
    
    def __init__(self, line):
        ctype.__init__(self, line)
        
        self.params = []
        self.name = ""
        
        m = re.match(self.regex[0], line)
        if m:
            self.returntype = m.group(1).strip()
            self.name = m.group(2).strip()
            self.params = functionParamsToList(m.group(3).strip())
            self.completed = True
            return
        
        m= re.match(self.regex[1], line)
        if m:
            self.returntype = m.group(1).strip()
            self.name = m.group(2).strip()
            self.params = functionParamsToList(m.group(3).strip())
            self.completed = False
        else:
            raise ValidationError("No valid function pointer")
    
    def wantMore(self):
        return not self.completed
    
    def doMore(self, line):
        m = re.match("^\s*([\w\s\*,]+)\s*\)\s*;\s*$", line)
        if m:
            self.params += functionParamsToList(m.group(1))
            self.completed = True
            return False
        else:
            return True
        
    def definition(self, body = ""):
        return self.declaration()
    
    def declaration(self):
        return "%s (*%s)(%s);" % (self.returntype, self.name, functionParamsToString(self.params))
    
    def toPython(self, method = True, forcenative = False):
        if method:
            if len(self.params) == 0:
                return "def %s(self):" % self.name
            else:
                return "def %s(self, %s):" % (self.name, ", ".join([prepareNameForPython(x[1]) for x in self.params]))
        else:
            if len(self.params) == 0:
                return "def %s():" % self.name
            else:
                return "def %s(%s):" % (self.name, ", ".join([prepareNameForPython(x[1]) for x in self.params]))
    
    def __eq__(self, f2):
        return (self.name == f2.name) and (self.returntype == f2.returntype) and (self.params == f2.params)
    
    def resolve(self, symbol, value):
        raise UnknownSymbolError("Don't know this symbol")
    
    def knowSymbol(self, symbol):
        return True
    
    def getSymbolValue(self, symbol):
        raise UnknownSymbolError("Don't know this symbol")

def cclasses():
    """
    Returns all ctype-subclasses
    @return: the list of ctype-subclasses
    @rtype: list
    """
    g = globals().copy()
    l = [g[key] for key in g if isinstance(g[key], type) and issubclass(g[key], ctype) and g[key] != ctype]
    #return sorted by keyword, to make search faster; reversed, to move classes without keyword to the tail
    return sorted(l, key=lambda cls: cls.keyword, reverse=True)

def _skipLines(lines):
    """
    Check, if the first line(s) can be skipped by the parser.
    @return: number of lines that can be skipped
    @rtype: int
    """
    i = 0
    if lines[i].strip() == "":
        return 1
    
    if lines[i].split(" ")[0] in ["#ifdef", "#ifndef", "#endif", "#include", "extern"]:
        return 1
    
    """
     #skip structure declarations
    if re.match(r"^\s*struct\s+\w+\s*;$", lines[i]) != None:
        return 1
    
    #skip structure definition till the end
    if re.match(r"^\s*struct\s+\w+\s*{\s*$", lines[i]) != None:
        count = 1

        i += 1
        balance = 0
        while True:
            if re.match(r"^\s*}\s*;$", lines[i]) != None:
                if balance == 0:
                    count += 1
                    break
                else:
                    balance -= 1
            else:
                if "{" in lines[i]:
                    balance += 1
                if "}" in lines[i]:
                    balance -= 1

            i += 1
            count += 1
            
        return count
    """
        
    return 0

def _matchingClass(line):
    """
    Search a matching ctype class for a line of code
    @param line: a line of source code
    @type line: string
    @return: The matching class or None
    @rtype: class
    """
    for cls in cclasses():
        if cls.keyword != "":
            if cls.keyword in line:
                return cls
        else:
            for p in cls.regex:
                if re.match(p, line) != None:
                    return cls
    
    return None

def _resolveH(sym, all):
    """
    @param sym:
    @type sym:
    @param all:
    @type all:
    @return:
    @rtype:
    """
    for a in all:
        if a.knowSymbol(sym):
            ret = a.getSymbolValue(sym)
            
            if is_int(ret):
                return ret
            else:
                raise Exception("Can't resolve symbol %s. Maybe multiple resolving needed?" % symbol)
                #FIXME
                """
                recret = _resolveH(ret, all)
                exec("%s = %d" % (symbol, recret))
                return _resolveH(ret, all)
                """
    raise UnknownSymbolError("Symbol %s can't be resolved" % sym)

def _resolve(res, all):
    """
    
    @param res:
    @type res:
    @param all:
    @type all:
    """
    for r in res:
        for s in r.resolveSymbols:
            r.resolve(s, _resolveH(s, all))

def getSymbols(filepathes):
    """
    Process C source files to get all symbols.
    @param filepathes: pathes to the C source files
    @type filepathes: list of strings
    @return: a list of symbols in the source file
    @rtype: list of ctype-subclasses
    """
    ret = []
    ressym = []
    skipped = {}
    
    for fp in filepathes:
        lines = []
        with open(fp) as f:
            lines = f.read().splitlines()
    
        #skip first two lines (#ifndef header, #define header)
        skipped[os.path.basename(fp)] = lines[0:2]
    
        i = 2
    
        while i < len(lines):
            skip = _skipLines(lines[i:])
        
            if skip != 0:
                skipped[os.path.basename(fp)] += lines[i:i+skip]
                i += skip
                continue

            fcls = _matchingClass(lines[i])

            if fcls == None:
                skipped[os.path.basename(fp)].append(lines[i])
                i += 1
            else:
                try:
                    cobj = fcls(lines[i])
                except:
                    skipped[os.path.basename(fp)].append(lines[i])
                    i += 1
                    continue
            
                i += 1
            
                while cobj.wantMore():
                    if cobj.doMore(lines[i]):
                        skipped[os.path.basename(fp)].append(lines[i])
                    i += 1
                    if i >= len(lines):
                        break
            
                ret.append(cobj)
                if cobj.needsResolving():
                    ressym.append(cobj)
    
    _resolve(ressym, ret)
    
    tmp = tempfile.NamedTemporaryFile(delete=False)
    
    for key in skipped:
        tmp.write(str("Skipped in %s:\n" % key).encode('utf-8'))
        
        for line in skipped[key]:
            tmp.write(str("%s\n" % line).encode('utf-8'))
        
        tmp.write("\n".encode('utf-8'))
    
    tmp.close()
    
    print("Skipped lines are written to %s" % tmp.name)
    
    return ret
