import ts3plugin

import socket, platform, os
from threading import Thread

UNIX_SOCK_FILE = "/tmp/ts3_python_ipcplugin"

class ipcplugin(ts3plugin.ts3Plugin):
    name = "ipcplugin"
    apiVersion = 20
    
    def __init__(self):
        if platform.system() == "Linux" or platform.system() == "Mac":
            if os.path.exists(UNIX_SOCK_FILE):
                os.remove(UNIX_SOCK_FILE)
        
            self.server = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
            self.server.bind(UNIX_SOCK_FILE)
        elif platform.system() == "Windows":
            pass
        else:
            raise Exception("Undefined platform %s" % platform.system())
            
        self.t = Thread(target=serverLoop, args=(self))
        self.t.start()
    
    def __del__(self):
        pass
        
    def serverLoop(self):
        while True:
            data = server.recv(1024)
            if not data:
                break
            else:
                pass
