#include "auto_plugin.h"

#include "pythonhost.h"
#include "ts3logdispatcher.h"

/*
class ts3Plugin:
    """
    Baseclass for ts3 python plugins.
    Inherit methods to subscribe to events.

    The C-Plugin will create an object of every activated plugin.
    """

    def __init__(self):
        """

        """

    def __del__(self):
        """

        """
*/

void ts3plugin_currentServerConnectionChanged(uint64 serverConnectionHandlerID) {
  /*
    def currentServerConnectionChanged(self, serverConnectionHandlerID):
        """
        Triggers when the current active serverconnection changes.
        @param serverConnectionHandlerID: the ID of the new active serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("currentServerConnectionChanged", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling currentServerConnectionChanged failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onConnectStatusChangeEvent(uint64 serverConnectionHandlerID, int newStatus, unsigned int errorNumber) {
  /*
    def onConnectStatusChangeEvent(self, serverConnectionHandlerID, newStatus, errorNumber):
        """
        Triggers when the connect status changes.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param newStatus: the new status, see ts3defines.ConnectStatus
        @type newStatus: int
        @param errorNumber: gives the errorcode if something went wrong
        @type errorNumber: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onConnectStatusChangeEvent", &callret, callerror, "KiI", (unsigned long long)serverConnectionHandlerID, newStatus, errorNumber))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onConnectStatusChangeEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onNewChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID) {
  /*
    def onNewChannelEvent(self, serverConnectionHandlerID, channelID, channelParentID):
        """
        Triggers after a new connection has established for each channel on the server to announce every channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param channelParentID: the ID of the parent channel
        @type channelParentID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onNewChannelEvent", &callret, callerror, "KKK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned long long)channelParentID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onNewChannelEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onNewChannelCreatedEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
  /*
    def onNewChannelCreatedEvent(self, serverConnectionHandlerID, channelID, channelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a new channel was created.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the new channel
        @type channelID: int
        @param channelParentID: the ID of the parent channel
        @type channelParentID: int
        @param invokerID: the ID of the client who created the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onNewChannelCreatedEvent", &callret, callerror, "KKKIss", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned long long)channelParentID, (unsigned int)invokerID, invokerName, invokerUniqueIdentifier))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onNewChannelCreatedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onDelChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
  /*
    def onDelChannelEvent(self, serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a channel was deleted.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the deleted channel
        @type channelID: int
        @param invokerID: the ID of the client who deleted the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onDelChannelEvent", &callret, callerror, "KKIss", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned int)invokerID, invokerName, invokerUniqueIdentifier))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onDelChannelEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelMoveEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 newChannelParentID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
  /*
    def onChannelMoveEvent(self, serverConnectionHandlerID, channelID, newChannelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a channel was moved to a new parent or the order was changed.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param newChannelParentID: the ID of the new parent channel
        @type newChannelParentID: int
        @param invokerID: the ID of the client who moved the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelMoveEvent", &callret, callerror, "KKKIss", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned long long)newChannelParentID, (unsigned int)invokerID, invokerName, invokerUniqueIdentifier))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelMoveEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onUpdateChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
  /*
    def onUpdateChannelEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when a channel was modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onUpdateChannelEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onUpdateChannelEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onUpdateChannelEditedEvent(uint64 serverConnectionHandlerID, uint64 channelID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
  /*
    def onUpdateChannelEditedEvent(self, serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a channel was modified by a client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param invokerID: the ID of the client who edited the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onUpdateChannelEditedEvent", &callret, callerror, "KKIss", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned int)invokerID, invokerName, invokerUniqueIdentifier))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onUpdateChannelEditedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onUpdateClientEvent(uint64 serverConnectionHandlerID, anyID clientID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
  /*
    def onUpdateClientEvent(self, serverConnectionHandlerID, clientID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers after a client variable changed or after requestClientVariables was called.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the updated client
        @type clientID: int
        @param invokerID: the ID of the client who edited the variable
        @type invokerID: int
        @param invokerName: the current nickname of the client who edited the variable
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client who edited the variable
        @type invokerUniqueIdentifier: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onUpdateClientEvent", &callret, callerror, "KIIss", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned int)invokerID, invokerName, invokerUniqueIdentifier))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onUpdateClientEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientMoveEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, const char* moveMessage) {
  /*
    def onClientMoveEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moveMessage):
        """
        Triggers when a client moves to another channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param oldChannelID: the ID of the old channel, if set to 0, the client has just connected to the server
        @type oldChannelID: int
        @param newChannelID: the ID of the new channel, if set to 0, the client disconnected from the server
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        @param moveMessage: the quitmessage, if the client left the server
        @type moveMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientMoveEvent", &callret, callerror, "KIKKis", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility, moveMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientMoveEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientMoveSubscriptionEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility) {
  /*
    def onClientMoveSubscriptionEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility):
        """
        Triggers for each client in a channel after subscribing or unsubscribing to resp. from that channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param oldChannelID: the ID of the subscribed channel where the client left visibility, set to 0 if subscribing to the channel
        @type oldChannelID: int
        @param newChannelID: ID of the subscribed channel where the client entered visibility //FIXME: what if visibility in [LEAVE, RETAIN]?
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientMoveSubscriptionEvent", &callret, callerror, "KIKKi", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientMoveSubscriptionEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientMoveTimeoutEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, const char* timeoutMessage) {
  /*
    def onClientMoveTimeoutEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, timeoutMessage):
        """
        Triggers when a client drops his connection.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param oldChannelID: the ID of the channel the client was in last
        @type oldChannelID: int
        @param newChannelID: always set to 0
        @type newChannelID: int
        @param visibility: Always set to ts3dfines.Visbility.LEAVE_VISIBILITY.
        @type visibility: int
        @param timeoutMessage: message giving the reason for the timeout //wäh?
        @type timeoutMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientMoveTimeoutEvent", &callret, callerror, "KIKKis", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility, timeoutMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientMoveTimeoutEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientMoveMovedEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID moverID, const char* moverName, const char* moverUniqueIdentifier, const char* moveMessage) {
  /*
    def onClientMoveMovedEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moverID, moverName, moverUniqueIdentifier, moveMessage):
        """
        Triggers when a client is moved to another channel by another client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the moved client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: the ID of the new channel
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        @param moverID: the ID of the moving client
        @type moverID: int
        @param moverName: the current nickname of the moving client
        @type moverName: string
        @param moverUniqueIdentifier: the UID of the moving client
        @type moverUniqueIdentifier: string
        @param moveMessage: message giving the reason for the move
        @type moveMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientMoveMovedEvent", &callret, callerror, "KIKKiIsss", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility, (unsigned int)moverID, moverName, moverUniqueIdentifier, moveMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientMoveMovedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientKickFromChannelEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, const char* kickMessage) {
  /*
    def onClientKickFromChannelEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        """
        Triggers when a client is kicked from his current channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the kicked client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: the ID of the default channel, the client was kicked to
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        @param kickerID: the ID of the kicking client
        @type kickerID: int
        @param kickerName: the current name of the kicking client
        @type kickerName: string
        @param kickerUniqueIdentifier: the UID of the kicking client
        @type kickerUniqueIdentifier: string
        @param kickMessage: message giving the reason for the kick
        @type kickMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientKickFromChannelEvent", &callret, callerror, "KIKKiIsss", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility, (unsigned int)kickerID, kickerName, kickerUniqueIdentifier, kickMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientKickFromChannelEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientKickFromServerEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, const char* kickMessage) {
  /*
    def onClientKickFromServerEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        """
        Triggers when a client is kicked from the server.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the kicked client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: always set to 0
        @type newChannelID: int
        @param visibility: always set to ts3defines.Visibility.LEAVE_VISBILITY
        @type visibility: int
        @param kickerID: the ID of the kicking client
        @type kickerID: int
        @param kickerName: the current nickname of the kicking client
        @type kickerName: string
        @param kickerUniqueIdentifier: the UID of the kicking client
        @type kickerUniqueIdentifier: string
        @param kickMessage: message giving the reason for the kick
        @type kickMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientKickFromServerEvent", &callret, callerror, "KIKKiIsss", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility, (unsigned int)kickerID, kickerName, kickerUniqueIdentifier, kickMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientKickFromServerEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientIDsEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, anyID clientID, const char* clientName) {
  /*
    def onClientIDsEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientID, clientName):
        """
        Triggers after requesting a users's client IDs with requestClientIDs.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the client
        @type uniqueClientIdentifier: string
        @param clientID: the ID of the client
        @type clientID: int
        @param clientName: the current nickname of the client
        @type clientName: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientIDsEvent", &callret, callerror, "KsIs", (unsigned long long)serverConnectionHandlerID, uniqueClientIdentifier, (unsigned int)clientID, clientName))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientIDsEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientIDsFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onClientIDsFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all requested client IDs are announced by onClientIDsEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientIDsFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientIDsFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerEditedEvent(uint64 serverConnectionHandlerID, anyID editerID, const char* editerName, const char* editerUniqueIdentifier) {
  /*
    def onServerEditedEvent(self, serverConnectionHandlerID, editerID, editerName, editerUniqueIdentifier):
        """
        Triggers when a server variable was modified by a client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param editerID: the ID of the client
        @type editerID: int
        @param editerName: the current nickname of the client
        @type editerName: string
        @param editerUniqueIdentifier: the UID of the client
        @type editerUniqueIdentifier: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerEditedEvent", &callret, callerror, "KIss", (unsigned long long)serverConnectionHandlerID, (unsigned int)editerID, editerName, editerUniqueIdentifier))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerEditedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerUpdatedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onServerUpdatedEvent(self, serverConnectionHandlerID):
        """
        Triggers when the server variables are available after requesting with requestServerVariables.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerUpdatedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerUpdatedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

int ts3plugin_onServerErrorEvent(uint64 serverConnectionHandlerID, const char* errorMessage, unsigned int error, const char* returnCode, const char* extraMessage) {
  /*
    def onServerErrorEvent(self, serverConnectionHandlerID, errorMessage, error, returnCode, extraMessage):
        """
        Triggers when a server error occured.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param errorMessage: a description of the error
        @type errorMessage: string
        @param error: the errorcode
        @type error: int
        @param returnCode: the returnCode created with createReturnCode and passed to the function which raised the error, empty string if no returnCode was passed
        @type returnCode: string
        @param extraMessage: additional information to the error or an empty string
        @type extraMessage: string
        @return: returns 1 (or True) to let the client ignore the error
        @rtype: int or bool
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerErrorEvent", &callret, callerror, "KsIss", (unsigned long long)serverConnectionHandlerID, errorMessage, error, returnCode, extraMessage)) {
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerErrorEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
    return 0;
  }
  else return callret ? 1 : 0;
}

void ts3plugin_onServerStopEvent(uint64 serverConnectionHandlerID, const char* shutdownMessage) {
  /*
    def onServerStopEvent(self, serverConnectionHandlerID, shutdownMessage):
        """
        Triggers when a server shuts down.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param shutdownMessage: message giving the reason for the shutdown
        @type shutdownMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerStopEvent", &callret, callerror, "Ks", (unsigned long long)serverConnectionHandlerID, shutdownMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerStopEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

int ts3plugin_onTextMessageEvent(uint64 serverConnectionHandlerID, anyID targetMode, anyID toID, anyID fromID, const char* fromName, const char* fromUniqueIdentifier, const char* message, int ffIgnored) {
  /*
    def onTextMessageEvent(self, serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored):
        """
        Triggers when a text message is received.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param targetMode: defines the target of the text message, see ts3defines.TextMessageTargetMode
        @type targetMode: int
        @param toID: the ID of the target //FIXME: muss das nicht unit64 sein?
        @type toID: int
        @param fromID: the ID of the sending client
        @type fromID: int
        @param fromName: the current nickname of the sending client
        @type fromName: string
        @param fromUniqueIdentifier: the UID of the sending client
        @type fromUniqueIdentifier: string
        @param message: the text message
        @type message: string
        @param ffIgnored: if set to 1 (or True) the friend/foe manager will ignore this message
        @type ffIgnored: int or bool
        @return: returns 1 (or True) to let the client ignore this message, 0 (or False) otherwise
        @rtype: int or bool
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onTextMessageEvent", &callret, callerror, "KIIIsssi", (unsigned long long)serverConnectionHandlerID, (unsigned int)targetMode, (unsigned int)toID, (unsigned int)fromID, fromName, fromUniqueIdentifier, message, ffIgnored)) {
    ts3logdispatcher::instance()->add(QObject::tr("Calling onTextMessageEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
    return 0;
  }
  else return callret ? 1 : 0;
}

void ts3plugin_onTalkStatusChangeEvent(uint64 serverConnectionHandlerID, int status, int isReceivedWhisper, anyID clientID) {
  /*
    def onTalkStatusChangeEvent(self, serverConnectionHandlerID, status, isReceivedWhisper, clientID):
        """
        Triggers when a client starts or stops talking.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param status: specifies the talk status, see ts3defines.TalkStatus
        @type status: int
        @param isReceivedWhisper: is set to 1 (or True) if the event is caused by whispering, otherwise 0 (or False)
        @type isReceivedWhisper: int or bool
        @param clientID: the ID of the client
        @type clientID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onTalkStatusChangeEvent", &callret, callerror, "KiiI", (unsigned long long)serverConnectionHandlerID, status, isReceivedWhisper, (unsigned int)clientID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onTalkStatusChangeEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onConnectionInfoEvent(uint64 serverConnectionHandlerID, anyID clientID) {
  /*
    def onConnectionInfoEvent(self, serverConnectionHandlerID, clientID):
        """
        Triggers when the connection info is available, requested with requestConnectionInfo.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onConnectionInfoEvent", &callret, callerror, "KI", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onConnectionInfoEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerConnectionInfoEvent(uint64 serverConnectionHandlerID) {
  /*
    def onServerConnectionInfoEvent(self, serverConnectionHandlerID):
        """
        //FIXME: wäh?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerConnectionInfoEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerConnectionInfoEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelSubscribeEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
  /*
    def onChannelSubscribeEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers for each channel subscribed to with requestChannelSubscribeAll.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelSubscribeEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelSubscribeEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelSubscribeFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onChannelSubscribeFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when subscription to all channels with requestChannelSubscribeAll ended.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelSubscribeFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelSubscribeFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelUnsubscribeEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
  /*
    def onChannelUnsubscribeEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers for each channel unsubscribed from with requestChannelUnsubscribeAll.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: 
        @type channelID: 
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelUnsubscribeEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelUnsubscribeEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelUnsubscribeFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onChannelUnsubscribeFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when unsubscription from all channels with requestChannelUnsubscribeAll ended.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelUnsubscribeFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelUnsubscribeFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelDescriptionUpdateEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
  /*
    def onChannelDescriptionUpdateEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when the description of a channel is edited.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelDescriptionUpdateEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelDescriptionUpdateEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelPasswordChangedEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
  /*
    def onChannelPasswordChangedEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when the password to a channel is modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelPasswordChangedEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelPasswordChangedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onPlaybackShutdownCompleteEvent(uint64 serverConnectionHandlerID) {
  /*
    def onPlaybackShutdownCompleteEvent(self, serverConnectionHandlerID):
        """
        Triggers when it's safe to close the playback device.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPlaybackShutdownCompleteEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPlaybackShutdownCompleteEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onSoundDeviceListChangedEvent(const char* modeID, int playOrCap) {
  /*
    def onSoundDeviceListChangedEvent(self, modeID, playOrCap):
        """
        //FIXME: wäh?
        @param modeID: 
        @type modeID: string
        @param playOrCap: 
        @type playOrCap: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onSoundDeviceListChangedEvent", &callret, callerror, "si", modeID, playOrCap))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onSoundDeviceListChangedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onUserLoggingMessageEvent(const char* logMessage, int logLevel, const char* logChannel, uint64 logID, const char* logTime, const char* completeLogString) {
  /*
    def onUserLoggingMessageEvent(self, logMessage, logLevel, logChannel, logID, logTime, completeLogString):
        """
        Triggers when a message is logged.
        @param logMessage: the message
        @type logMessage: string
        @param logLevel: level of the message, see ts3defines.LogLevel
        @type logLevel: int
        @param logChannel: the channel of the message
        @type logChannel: string
        @param logID: the ID of the serverconnection of the message or 0 if none given
        @type logID: int
        @param logTime: the formated date and time of the message
        @type logTime: string
        @param completeLogString: all log parameters combined for convinience
        @type completeLogString: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onUserLoggingMessageEvent", &callret, callerror, "sisKss", logMessage, logLevel, logChannel, (unsigned long long)logID, logTime, completeLogString))
    printf("%s\n", QObject::tr("Calling onUserLoggingMessageEvent failed with \"%1\"").arg(callerror).toUtf8().data());
}

void ts3plugin_onClientBanFromServerEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, uint64 time, const char* kickMessage) {
  /*
    def onClientBanFromServerEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, time, kickMessage):
        """
        Triggers when a client is banned from the server.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the banned client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: always set to 0
        @type newChannelID: int
        @param visibility: always set to ts3defines.Visbility.LEAVE_VISIBILITY
        @type visibility: int
        @param kickerID: the ID of the banning client
        @type kickerID: int
        @param kickerName: the current nickname of the banning client
        @type kickerName: string
        @param kickerUniqueIdentifier: the UID of the banning client
        @type kickerUniqueIdentifier: string
        @param time: the duration of the ban in seconds, 0 if infinite
        @type time: int
        @param kickMessage: the reason for the ban
        @type kickMessage: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientBanFromServerEvent", &callret, callerror, "KIKKiIssKs", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, (unsigned long long)oldChannelID, (unsigned long long)newChannelID, visibility, (unsigned int)kickerID, kickerName, kickerUniqueIdentifier, (unsigned long long)time, kickMessage))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientBanFromServerEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

int ts3plugin_onClientPokeEvent(uint64 serverConnectionHandlerID, anyID fromClientID, const char* pokerName, const char* pokerUniqueIdentity, const char* message, int ffIgnored) {
  /*
    def onClientPokeEvent(self, serverConnectionHandlerID, fromClientID, pokerName, pokerUniqueIdentity, message, ffIgnored):
        """
        Triggers when the client is poked by another client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param fromClientID: the ID of the poking client
        @type fromClientID: int
        @param pokerName: the current nickname of the poking client
        @type pokerName: string
        @param pokerUniqueIdentity: the UID of the poking client
        @type pokerUniqueIdentity: string
        @param message: the message of the poke
        @type message: string
        @param ffIgnored: if set to 1 (or True) the friend/foe manager will ignore the poke
        @type ffIgnored: int or bool
        @return: returns 1 (or True) to let the client ignore this poke, 0 (or False) otherwise
        @rtype: int or bool
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientPokeEvent", &callret, callerror, "KIsssi", (unsigned long long)serverConnectionHandlerID, (unsigned int)fromClientID, pokerName, pokerUniqueIdentity, message, ffIgnored)) {
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientPokeEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
    return 0;
  }
  else return callret ? 1 : 0;
}

void ts3plugin_onClientSelfVariableUpdateEvent(uint64 serverConnectionHandlerID, int flag, const char* oldValue, const char* newValue) {
  /*
    def onClientSelfVariableUpdateEvent(self, serverConnectionHandlerID, flag, oldValue, newValue):
        """
        Triggers when a client variable of the own client was modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param flag: the flag that has changed
        @type flag: int
        @param oldValue: the old value
        @type oldValue: string
        @param newValue: the new value
        @type newValue: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientSelfVariableUpdateEvent", &callret, callerror, "Kiss", (unsigned long long)serverConnectionHandlerID, flag, oldValue, newValue))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientSelfVariableUpdateEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onFileListEvent(uint64 serverConnectionHandlerID, uint64 channelID, const char* path, const char* name, uint64 size, uint64 datetime, int type, uint64 incompletesize, const char* returnCode) {
  /*
    def onFileListEvent(self, serverConnectionHandlerID, channelID, path, name, size, datetime, ptype, incompletesize, returnCode):
        """
        Triggers for each file and directory in a channel's path requested with requestFileList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param path: the path that was requested
        @type path: string
        @param name: the name of the file or directory
        @type name: string
        @param size: the size of the file on the server in Bytes, 0 if directory
        @type size: int
        @param datetime: time of last modification as unix timestamp
        @type datetime: int
        @param ptype: specifies, if the current entry is a file or directory, see ts3defines.FileListType
        @type ptype: int
        @param incompletesize: the total size of the file, if not completely uploaded, otherwise set to 0
        @type incompletesize: int
        @param returnCode: the returnCode passed to the request, or an empty string if none passed
        @type returnCode: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onFileListEvent", &callret, callerror, "KKssKKiKs", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, path, name, (unsigned long long)size, (unsigned long long)datetime, type, (unsigned long long)incompletesize, returnCode))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onFileListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onFileListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelID, const char* path) {
  /*
    def onFileListFinishedEvent(self, serverConnectionHandlerID, channelID, path):
        """
        Triggers when all files and directories of a path in a channel are announced by onFileListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param path: the path that was requested
        @type path: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onFileListFinishedEvent", &callret, callerror, "KKs", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, path))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onFileListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onFileInfoEvent(uint64 serverConnectionHandlerID, uint64 channelID, const char* name, uint64 size, uint64 datetime) {
  /*
    def onFileInfoEvent(self, serverConnectionHandlerID, channelID, name, size, datetime):
        """
        Triggers when the info on a file or directory in a channel is requested with requestFileInfo.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param name: name of the file or directory
        @type name: string
        @param size: size of the file in Bytes, or 0 if it's a directory
        @type size: int
        @param datetime: time of last modification as unix timestamp
        @type datetime: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onFileInfoEvent", &callret, callerror, "KKsKK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, name, (unsigned long long)size, (unsigned long long)datetime))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onFileInfoEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupListEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID, const char* name, int type, int iconID, int saveDB) {
  /*
    def onServerGroupListEvent(self, serverConnectionHandlerID, serverGroupID, name, ptype, iconID, saveDB):
        """
        Triggers for each servergroup requested with requestServerGroupList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param name: the name of the servergroup
        @type name: string
        @param type: type of the servergroup, see ts3defines.GroupType
        @type type: int
        @param iconID: the ID of the icon
        @type iconID: int
        @param saveDB: set to 1 (or True), if the servergroup is permanent, 0 (or False) otherwise
        @type saveDB: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupListEvent", &callret, callerror, "KKsiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)serverGroupID, name, type, iconID, saveDB))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupListFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onServerGroupListFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all servergroup are announced in onServerGroupListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupListFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupByClientIDEvent(uint64 serverConnectionHandlerID, const char* name, uint64 serverGroupList, uint64 clientDatabaseID) {
  /*
    def onServerGroupByClientIDEvent(self, serverConnectionHandlerID, name, serverGroupList, clientDatabaseID):
        """
        Triggers for each servergroup of a client requested with requestServerGroupsByClientID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param name: the name of the servergroup
        @type name: string
        @param serverGroupList: //FIXME: serverGroupID?
        @type serverGroupList: int
        @param clientDatabaseID: the database ID of the client
        @type clientDatabaseID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupByClientIDEvent", &callret, callerror, "KsKK", (unsigned long long)serverConnectionHandlerID, name, (unsigned long long)serverGroupList, (unsigned long long)clientDatabaseID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupByClientIDEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupPermListEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
  /*
    def onServerGroupPermListEvent(self, serverConnectionHandlerID, serverGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a servergroup requested by requestServerGroupPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupPermListEvent", &callret, callerror, "KKIiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)serverGroupID, permissionID, permissionValue, permissionNegated, permissionSkip))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupPermListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID) {
  /*
    def onServerGroupPermListFinishedEvent(self, serverConnectionHandlerID, serverGroupID):
        """
        Triggers when all permissions of a servergroup are announced by onServerGroupPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupPermListFinishedEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)serverGroupID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupPermListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupClientListEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID, uint64 clientDatabaseID, const char* clientNameIdentifier, const char* clientUniqueID) {
  /*
    def onServerGroupClientListEvent(self, serverConnectionHandlerID, serverGroupID, clientDatabaseID, clientNameIdentifier, clientUniqueID):
        """
        Triggers for each user in a servergroup requested by requestServerGroupClientList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param clientNameIdentifier: //wäh? last nickname on the server?
        @type clientNameIdentifier: string
        @param clientUniqueID: the UID of the user
        @type clientUniqueID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupClientListEvent", &callret, callerror, "KKKss", (unsigned long long)serverConnectionHandlerID, (unsigned long long)serverGroupID, (unsigned long long)clientDatabaseID, clientNameIdentifier, clientUniqueID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupClientListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelGroupListEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID, const char* name, int type, int iconID, int saveDB) {
  /*
    def onChannelGroupListEvent(self, serverConnectionHandlerID, channelGroupID, name, ptype, iconID, saveDB):
        """
        Triggers for each channelgroup requested by requestChannelGroupList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the channelgroup
        @type channelGroupID: int
        @param name: the name of the channelgroup
        @type name: string
        @param type: type of the servergroup, see ts3defines.GroupType
        @type type: int
        @param iconID: the ID of the icon
        @type iconID: int
        @param saveDB: set to 1 (or True), if the channelGroupID is permanent, 0 (or False) otherwise
        @type saveDB: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelGroupListEvent", &callret, callerror, "KKsiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelGroupID, name, type, iconID, saveDB))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelGroupListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelGroupListFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onChannelGroupListFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all channelgroups are announced by onChannelGroupListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelGroupListFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelGroupListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelGroupPermListEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
  /*
    def onChannelGroupPermListEvent(self, serverConnectionHandlerID, channelGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a channelgroup requested by requestChannelGroupPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the channelgroup
        @type channelGroupID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelGroupPermListEvent", &callret, callerror, "KKIiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelGroupID, permissionID, permissionValue, permissionNegated, permissionSkip))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelGroupPermListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelGroupPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID) {
  /*
    def onChannelGroupPermListFinishedEvent(self, serverConnectionHandlerID, channelGroupID):
        """
        Triggers when all permissions assigned to a channelgroup are announced by onChannelGroupPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the channelgroup
        @type channelGroupID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelGroupPermListFinishedEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelGroupID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelGroupPermListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelPermListEvent(uint64 serverConnectionHandlerID, uint64 channelID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
  /*
    def onChannelPermListEvent(self, serverConnectionHandlerID, channelID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a channel requested by requestChannelPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelPermListEvent", &callret, callerror, "KKIiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, permissionID, permissionValue, permissionNegated, permissionSkip))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelPermListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
  /*
    def onChannelPermListFinishedEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when all permissions assigned to a channel are announced by onChannelPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelPermListFinishedEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelPermListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientPermListEvent(uint64 serverConnectionHandlerID, uint64 clientDatabaseID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
  /*
    def onClientPermListEvent(self, serverConnectionHandlerID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a user requested by requestClientPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientPermListEvent", &callret, callerror, "KKIiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientPermListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 clientDatabaseID) {
  /*
    def onClientPermListFinishedEvent(self, serverConnectionHandlerID, clientDatabaseID):
        """
        Triggers when all permissions assigned to a user are announced by onClientPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientPermListFinishedEvent", &callret, callerror, "KK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)clientDatabaseID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientPermListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelClientPermListEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 clientDatabaseID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
  /*
    def onChannelClientPermListEvent(self, serverConnectionHandlerID, channelID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a user in a channel requested by requestChannelClientPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelClientPermListEvent", &callret, callerror, "KKKIiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned long long)clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelClientPermListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onChannelClientPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 clientDatabaseID) {
  /*
    def onChannelClientPermListFinishedEvent(self, serverConnectionHandlerID, channelID, clientDatabaseID):
        """
        Triggers when all permissions assigned to a user in a channel are announced by onChannelClientPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param clientDatabaseID: the database ID of the client
        @type clientDatabaseID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onChannelClientPermListFinishedEvent", &callret, callerror, "KKK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelID, (unsigned long long)clientDatabaseID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onChannelClientPermListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientChannelGroupChangedEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID, uint64 channelID, anyID clientID, anyID invokerClientID, const char* invokerName, const char* invokerUniqueIdentity) {
  /*
    def onClientChannelGroupChangedEvent(self, serverConnectionHandlerID, channelGroupID, channelID, clientID, invokerClientID, invokerName, invokerUniqueIdentity):
        """
        Triggers when the channelgroup of a client is modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the new channelgroup
        @type channelGroupID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param invokerClientID: the ID of the client, who assigned the new channelgroup to the client
        @type invokerClientID: int
        @param invokerName: the current nickname of the invoking client
        @type invokerName: string
        @param invokerUniqueIdentity: the UID of the invoking client
        @type invokerUniqueIdentity: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientChannelGroupChangedEvent", &callret, callerror, "KKKIIss", (unsigned long long)serverConnectionHandlerID, (unsigned long long)channelGroupID, (unsigned long long)channelID, (unsigned int)clientID, (unsigned int)invokerClientID, invokerName, invokerUniqueIdentity))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientChannelGroupChangedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

int ts3plugin_onServerPermissionErrorEvent(uint64 serverConnectionHandlerID, const char* errorMessage, unsigned int error, const char* returnCode, unsigned int failedPermissionID) {
  /*
    def onServerPermissionErrorEvent(self, serverConnectionHandlerID, errorMessage, error, returnCode, failedPermissionID):
        """
        Triggers when a requested action fails because of insufficient permission rights.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param errorMessage: the message of the error
        @type errorMessage: string
        @param error: the errorcode
        @type error: int
        @@param returnCode: the returnCode created with createReturnCode and passed to the function which raised the error, empty string if no returnCode was passed
        @type returnCode: string
        @param failedPermissionID: the ID of the insuficient permission
        @type failedPermissionID: int
        @return: returns 1 (or True) to let the client ignore the error
        @rtype: int or bool
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerPermissionErrorEvent", &callret, callerror, "KsIsI", (unsigned long long)serverConnectionHandlerID, errorMessage, error, returnCode, failedPermissionID)) {
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerPermissionErrorEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
    return 0;
  }
  else return callret ? 1 : 0;
}

void ts3plugin_onPermissionListGroupEndIDEvent(uint64 serverConnectionHandlerID, unsigned int groupEndID) {
  /*
    def onPermissionListGroupEndIDEvent(self, serverConnectionHandlerID, groupEndID):
        """
        //FIXME: wäh?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param groupEndID: 
        @type groupEndID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPermissionListGroupEndIDEvent", &callret, callerror, "KI", (unsigned long long)serverConnectionHandlerID, groupEndID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPermissionListGroupEndIDEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onPermissionListEvent(uint64 serverConnectionHandlerID, unsigned int permissionID, const char* permissionName, const char* permissionDescription) {
  /*
    def onPermissionListEvent(self, serverConnectionHandlerID, permissionID, permissionName, permissionDescription):
        """
        Triggers for each permission on the server requested by requestPermissionList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionName: the name of the permission
        @type permissionName: string
        @param permissionDescription: the description of the permission
        @type permissionDescription: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPermissionListEvent", &callret, callerror, "KIss", (unsigned long long)serverConnectionHandlerID, permissionID, permissionName, permissionDescription))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPermissionListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onPermissionListFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onPermissionListFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all permissions of the server are announced by onPermissionListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPermissionListFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPermissionListFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onPermissionOverviewEvent(uint64 serverConnectionHandlerID, uint64 clientDatabaseID, uint64 channelID, int overviewType, uint64 overviewID1, uint64 overviewID2, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
  /*
    def onPermissionOverviewEvent(self, serverConnectionHandlerID, clientDatabaseID, channelID, overviewType, overviewID1, overviewID2, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission requested by requestPermissionOverview.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param overviewType: //FIXME: wäh?
        @type overviewType: 
        @param overviewID1: 
        @type overviewID1: 
        @param overviewID2: 
        @type overviewID2: 
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPermissionOverviewEvent", &callret, callerror, "KKKiKKIiii", (unsigned long long)serverConnectionHandlerID, (unsigned long long)clientDatabaseID, (unsigned long long)channelID, overviewType, (unsigned long long)overviewID1, (unsigned long long)overviewID2, permissionID, permissionValue, permissionNegated, permissionSkip))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPermissionOverviewEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onPermissionOverviewFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onPermissionOverviewFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all permissions of the overview are announced by onPermissionOverviewEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPermissionOverviewFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPermissionOverviewFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupClientAddedEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientName, const char* clientUniqueIdentity, uint64 serverGroupID, anyID invokerClientID, const char* invokerName, const char* invokerUniqueIdentity) {
  /*
    def onServerGroupClientAddedEvent(self, serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        """
        Triggers when a client is added to a servergroup.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param clientName: the current nickname of the client
        @type clientName: string
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: string
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param invokerClientID: the ID of the invoking client
        @type invokerClientID: int
        @param invokerName: the current nickname of the invoking client
        @type invokerName: string
        @param invokerUniqueIdentity: the UID of the invoking client
        @type invokerUniqueIdentity: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupClientAddedEvent", &callret, callerror, "KIssKIss", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, clientName, clientUniqueIdentity, (unsigned long long)serverGroupID, (unsigned int)invokerClientID, invokerName, invokerUniqueIdentity))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupClientAddedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerGroupClientDeletedEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientName, const char* clientUniqueIdentity, uint64 serverGroupID, anyID invokerClientID, const char* invokerName, const char* invokerUniqueIdentity) {
  /*
    def onServerGroupClientDeletedEvent(self, serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        """
        Triggers when a client is deleted from a servergroup.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client.
        @type clientID: int
        @param clientName: the current nickname of the client
        @type clientName: string
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: string
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param invokerClientID: the ID of the invoking client
        @type invokerClientID: int
        @param invokerName: the current nickname of the invoking client
        @type invokerName: string
        @param invokerUniqueIdentity: the UID of the invoking client
        @type invokerUniqueIdentity: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerGroupClientDeletedEvent", &callret, callerror, "KIssKIss", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, clientName, clientUniqueIdentity, (unsigned long long)serverGroupID, (unsigned int)invokerClientID, invokerName, invokerUniqueIdentity))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerGroupClientDeletedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientNeededPermissionsEvent(uint64 serverConnectionHandlerID, unsigned int permissionID, int permissionValue) {
  /*
    def onClientNeededPermissionsEvent(self, serverConnectionHandlerID, permissionID, permissionValue):
        """
        //FIXME: wäh? fehlt da ein Request?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientNeededPermissionsEvent", &callret, callerror, "KIi", (unsigned long long)serverConnectionHandlerID, permissionID, permissionValue))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientNeededPermissionsEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientNeededPermissionsFinishedEvent(uint64 serverConnectionHandlerID) {
  /*
    def onClientNeededPermissionsFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all permissions are announced by onClientNeededPermissionsEvent. //FIXME: siehe oben
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientNeededPermissionsFinishedEvent", &callret, callerror, "K", (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientNeededPermissionsFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onFileTransferStatusEvent(anyID transferID, unsigned int status, const char* statusMessage, uint64 remotefileSize, uint64 serverConnectionHandlerID) {
  /*
    def onFileTransferStatusEvent(self, transferID, status, statusMessage, remotefileSize, serverConnectionHandlerID):
        """
        //FIXME: wäh?
        @param transferID: the ID of the filetransfer
        @type transferID: int
        @param status: 
        @type status: int
        @param statusMessage: 
        @type statusMessage: string
        @param remotefileSize: 
        @type remotefileSize: int
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onFileTransferStatusEvent", &callret, callerror, "IIsKK", (unsigned int)transferID, status, statusMessage, (unsigned long long)remotefileSize, (unsigned long long)serverConnectionHandlerID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onFileTransferStatusEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientChatClosedEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientUniqueIdentity) {
  /*
    def onClientChatClosedEvent(self, serverConnectionHandlerID, clientID, clientUniqueIdentity):
        """
        Triggers when a client closes the private chat (by closeing the private chat tab).
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientChatClosedEvent", &callret, callerror, "KIs", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, clientUniqueIdentity))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientChatClosedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientChatComposingEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientUniqueIdentity) {
  /*
    def onClientChatComposingEvent(self, serverConnectionHandlerID, clientID, clientUniqueIdentity):
        """
        Triggers when a client is currently composing a private text message.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientChatComposingEvent", &callret, callerror, "KIs", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, clientUniqueIdentity))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientChatComposingEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerLogEvent(uint64 serverConnectionHandlerID, const char* logMsg) {
  /*
    def onServerLogEvent(self, serverConnectionHandlerID, logMsg):
        """
        //FIXME: requestServerLog fehlt?
        @param serverConnectionHandlerID: 
        @type serverConnectionHandlerID: 
        @param logMsg: 
        @type logMsg: 
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerLogEvent", &callret, callerror, "Ks", (unsigned long long)serverConnectionHandlerID, logMsg))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerLogEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerLogFinishedEvent(uint64 serverConnectionHandlerID, uint64 lastPos, uint64 fileSize) {
  /*
    def onServerLogFinishedEvent(self, serverConnectionHandlerID, lastPos, fileSize):
        """
        Triggers when all requested entries from the serverlog are announced by onServerLogEvent
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param lastPos: position of the last entry announced by onServerLogEvent //FIXME: right?
        @type lastPos: int
        @param fileSize: total amount of entries in serverlog //FIXME: right?
        @type fileSize: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerLogFinishedEvent", &callret, callerror, "KKK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)lastPos, (unsigned long long)fileSize))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerLogFinishedEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onMessageListEvent(uint64 serverConnectionHandlerID, uint64 messageID, const char* fromClientUniqueIdentity, const char* subject, uint64 timestamp, int flagRead) {
  /*
    def onMessageListEvent(self, serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, timestamp, flagRead):
        """
        Triggers for each offline message requested by requestMessageList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param messageID: the ID of the message
        @type messageID: int
        @param fromClientUniqueIdentity: the UID of the author
        @type fromClientUniqueIdentity: string
        @param subject: the subject of the message
        @type subject: string
        @param timestamp: the time of creation as unix timestamp
        @type timestamp: int
        @param flagRead: if set to 1 (or True) the message was already read, 0 (or False) otherwise
        @type flagRead: int or bool
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onMessageListEvent", &callret, callerror, "KKssKi", (unsigned long long)serverConnectionHandlerID, (unsigned long long)messageID, fromClientUniqueIdentity, subject, (unsigned long long)timestamp, flagRead))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onMessageListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onMessageGetEvent(uint64 serverConnectionHandlerID, uint64 messageID, const char* fromClientUniqueIdentity, const char* subject, const char* message, uint64 timestamp) {
  /*
    def onMessageGetEvent(self, serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, message, timestamp):
        """
        Triggers with the information of an offline message requested by requestMessageGet.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param messageID: the ID of the message
        @type messageID: int
        @param fromClientUniqueIdentity: the UID of the author
        @type fromClientUniqueIdentity: string
        @param subject: the subject of the message
        @type subject: string
        @param message: the message
        @type message: string
        @param timestamp: the time of creation as unix timestamp
        @type timestamp: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onMessageGetEvent", &callret, callerror, "KKsssK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)messageID, fromClientUniqueIdentity, subject, message, (unsigned long long)timestamp))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onMessageGetEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientDBIDfromUIDEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, uint64 clientDatabaseID) {
  /*
    def onClientDBIDfromUIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID):
        """
        Triggers with the information requested by requestClientDBIDfromUID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the client
        @type uniqueClientIdentifier: string
        @param clientDatabaseID: the database ID of the client
        @type clientDatabaseID: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientDBIDfromUIDEvent", &callret, callerror, "KsK", (unsigned long long)serverConnectionHandlerID, uniqueClientIdentifier, (unsigned long long)clientDatabaseID))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientDBIDfromUIDEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientNamefromUIDEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, uint64 clientDatabaseID, const char* clientNickName) {
  /*
    def onClientNamefromUIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        """
        Triggers with the information requested by requestClientNamefromUID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the user
        @type uniqueClientIdentifier: string
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param clientNickName: the last used nickname of the user
        @type clientNickName: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientNamefromUIDEvent", &callret, callerror, "KsKs", (unsigned long long)serverConnectionHandlerID, uniqueClientIdentifier, (unsigned long long)clientDatabaseID, clientNickName))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientNamefromUIDEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientNamefromDBIDEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, uint64 clientDatabaseID, const char* clientNickName) {
  /*
    def onClientNamefromDBIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        """
        Triggers with the information requested by requestClientNamefromDBID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the user
        @type uniqueClientIdentifier: string
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param clientNickName: the last used nickname of the user
        @type clientNickName: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientNamefromDBIDEvent", &callret, callerror, "KsKs", (unsigned long long)serverConnectionHandlerID, uniqueClientIdentifier, (unsigned long long)clientDatabaseID, clientNickName))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientNamefromDBIDEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onComplainListEvent(uint64 serverConnectionHandlerID, uint64 targetClientDatabaseID, const char* targetClientNickName, uint64 fromClientDatabaseID, const char* fromClientNickName, const char* complainReason, uint64 timestamp) {
  /*
    def onComplainListEvent(self, serverConnectionHandlerID, targetClientDatabaseID, targetClientNickName, fromClientDatabaseID, fromClientNickName, complainReason, timestamp):
        """
        Triggers for each complain of a user requested by requestComplainList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param targetClientDatabaseID: the database ID of the user
        @type targetClientDatabaseID: int
        @param targetClientNickName: the last nickname of the user
        @type targetClientNickName: string
        @param fromClientDatabaseID: the database ID of the complaining user
        @type fromClientDatabaseID: int
        @param fromClientNickName: the last nickname of the complaining user
        @type fromClientNickName: string
        @param complainReason: the reason for the complain
        @type complainReason: string
        @param timestamp: time of creation as unix timestamp
        @type timestamp: int
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onComplainListEvent", &callret, callerror, "KKsKssK", (unsigned long long)serverConnectionHandlerID, (unsigned long long)targetClientDatabaseID, targetClientNickName, (unsigned long long)fromClientDatabaseID, fromClientNickName, complainReason, (unsigned long long)timestamp))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onComplainListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onBanListEvent(uint64 serverConnectionHandlerID, uint64 banid, const char* ip, const char* name, const char* uid, uint64 creationTime, uint64 durationTime, const char* invokerName, uint64 invokercldbid, const char* invokeruid, const char* reason, int numberOfEnforcements, const char* lastNickName) {
  /*
    def onBanListEvent(self, serverConnectionHandlerID, banid, ip, name, uid, creationTime, durationTime, invokerName, invokercldbid, invokeruid, reason, numberOfEnforcements, lastNickName):
        """
        Triggers for each ban of the server requested by requestBanList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param banid: the ID of the ban
        @type banid: int
        @param ip: regular expression to match IPs, empty string if ignored
        @type ip: string
        @param name: regular expression to match nicknames, empty string if ignored
        @type name: string
        @param uid: the UID which was banned, empty string if ignored
        @type uid: string
        @param creationTime: time of creation as unix timestamp
        @type creationTime: int
        @param durationTime: duration of the ban in seconds, 0 is unlimited
        @type durationTime: int
        @param invokerName: the nickname of the invoking user at time of creation
        @type invokerName: string
        @param invokercldbid: the database ID of the invoking user
        @type invokercldbid: int
        @param invokeruid: the UID of the invoking user
        @type invokeruid: string
        @param reason: the reason for the ban
        @type reason: string
        @param numberOfEnforcements: number clients, whose connection was prevented of the ban
        @type numberOfEnforcements: int
        @param lastNickName: last nickname of the banned user (only if a UID was banned)
        @type lastNickName: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onBanListEvent", &callret, callerror, "KKsssKKsKssis", (unsigned long long)serverConnectionHandlerID, (unsigned long long)banid, ip, name, uid, (unsigned long long)creationTime, (unsigned long long)durationTime, invokerName, (unsigned long long)invokercldbid, invokeruid, reason, numberOfEnforcements, lastNickName))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onBanListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onClientServerQueryLoginPasswordEvent(uint64 serverConnectionHandlerID, const char* loginPassword) {
  /*
    def onClientServerQueryLoginPasswordEvent(self, serverConnectionHandlerID, loginPassword):
        """
        //FIXME: entweder fehlt hier der loginname oder der command requestClientServerQueryLoginPassword(schid, loginName) fehlt
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param loginPassword: the password to the serverquery login
        @type loginPassword: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onClientServerQueryLoginPasswordEvent", &callret, callerror, "Ks", (unsigned long long)serverConnectionHandlerID, loginPassword))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onClientServerQueryLoginPasswordEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onPluginCommandEvent(uint64 serverConnectionHandlerID, const char* pluginName, const char* pluginCommand) {
  /*
    def onPluginCommandEvent(self, serverConnectionHandlerID, pluginName, pluginCommand):
        """
        Triggers for each plugin command send by another client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param pluginName: the name identifier to the remote plugin
        @type pluginName: string
        @param pluginCommand: the command
        @type pluginCommand: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onPluginCommandEvent", &callret, callerror, "Kss", (unsigned long long)serverConnectionHandlerID, pluginName, pluginCommand))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onPluginCommandEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onIncomingClientQueryEvent(uint64 serverConnectionHandlerID, const char* commandText) {
  /*
    def onIncomingClientQueryEvent(self, serverConnectionHandlerID, commandText):
        """
        //FIXME: wäh?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param commandText: 
        @type commandText: 
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onIncomingClientQueryEvent", &callret, callerror, "Ks", (unsigned long long)serverConnectionHandlerID, commandText))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onIncomingClientQueryEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onServerTemporaryPasswordListEvent(uint64 serverConnectionHandlerID, const char* clientNickname, const char* uniqueClientIdentifier, const char* description, const char* password, uint64 timestampStart, uint64 timestampEnd, uint64 targetChannelID, const char* targetChannelPW) {
  /*
    def onServerTemporaryPasswordListEvent(self, serverConnectionHandlerID, clientNickname, uniqueClientIdentifier, description, password, timestampStart, timestampEnd, targetChannelID, targetChannelPW):
        """
        Triggers for each temporary password on the server requested by requestServerTemporaryPasswordList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientNickname: the nickname of the creator
        @type clientNickname: string
        @param uniqueClientIdentifier: the UID of the creator
        @type uniqueClientIdentifier: string
        @param description: the description of the temporary password
        @type description: string
        @param password: the password
        @type password: string
        @param timestampStart: time of start as unix timestamp
        @type timestampStart: int
        @param timestampEnd: time of end as unix timestamp
        @type timestampEnd: int
        @param targetChannelID: the ID of the default channel for clients connecting with the password
        @type targetChannelID: int
        @param targetChannelPW: the password of the target channel
        @type targetChannelPW: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onServerTemporaryPasswordListEvent", &callret, callerror, "KssssKKKs", (unsigned long long)serverConnectionHandlerID, clientNickname, uniqueClientIdentifier, description, password, (unsigned long long)timestampStart, (unsigned long long)timestampEnd, (unsigned long long)targetChannelID, targetChannelPW))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onServerTemporaryPasswordListEvent failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}

void ts3plugin_onAvatarUpdated(uint64 serverConnectionHandlerID, anyID clientID, const char* avatarPath) {
  /*
    def onAvatarUpdated(self, serverConnectionHandlerID, clientID, avatarPath):
        """
        Triggers when a client in view modified his avatar.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param avatarPath: the path to the avatar on the system
        @type avatarPath: string
        """
  */
  bool callret;
  QString callerror;
  if (!PythonHost::instance()->call("onAvatarUpdated", &callret, callerror, "KIs", (unsigned long long)serverConnectionHandlerID, (unsigned int)clientID, avatarPath))
    ts3logdispatcher::instance()->add(QObject::tr("Calling onAvatarUpdated failed with \"%1\"").arg(callerror), false, true, LogLevel_WARNING);
}
