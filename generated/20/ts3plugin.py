class PluginMetaClass(type):
  def __init__(cls, name, bases, nmspc):
    super(PluginMetaClass, cls).__init__(name, bases, nmspc)
    if not hasattr(cls, 'registry'):
      cls.registry = set()
    cls.registry.add(cls)
    cls.registry -= set(bases) # Remove base classes
    # Metamethods, called on class objects:
  def __iter__(cls):
    return iter(cls.registry)
  def __str__(cls):
    if cls in cls.registry:
      return cls.__name__
    return cls.__name__ + ": " + ", ".join([sc.__name__ for sc in cls])

class PluginHost:
    running = []
    @staticmethod
    def start(apiver, names, forced):
        lowapi = []

        for p in ts3Plugin:
            if p is not ts3Plugin and p.name in activated:
                if p.apiVersion != apiver and not p.name in forced:
                    lowapi.append(p.name)
                else:
                    PluginHost.running.append(p())

        return lowapi

    @staticmethod
    def stop():
        PluginHost.running = []

    @staticmethod
    def startPlugin(name):
        for p in ts3Plugin:
            if p is not ts3Plugin and p.name == name:
                PluginHost.running.append(p())
                return

    @staticmethod
    def stopPlugin(name):
        for p in PluginHost.running:
            if p.name  == name:
                PluginHost.running.remove(p)
                return

    @staticmethod
    def isRunning(name):
        for p in PluginHost.running:
            if p.name == name:
                return true

        return false

    @staticmethod
    def instance(name):
        for p in PluginHost.running:
            if p.name == name:
                return p

        return None

        @staticmethod
    def processCommand(serverConnectionHandlerID, command):
        for p in PluginHost.running:
            p.processCommand(serverConnectionHandlerID, command)

    @staticmethod
    def currentServerConnectionChanged(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.currentServerConnectionChanged(serverConnectionHandlerID)

    @staticmethod
    def onConnectStatusChangeEvent(serverConnectionHandlerID, newStatus, errorNumber):
        for p in PluginHost.running:
            p.onConnectStatusChangeEvent(serverConnectionHandlerID, newStatus, errorNumber)

    @staticmethod
    def onNewChannelEvent(serverConnectionHandlerID, channelID, channelParentID):
        for p in PluginHost.running:
            p.onNewChannelEvent(serverConnectionHandlerID, channelID, channelParentID)

    @staticmethod
    def onNewChannelCreatedEvent(serverConnectionHandlerID, channelID, channelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        for p in PluginHost.running:
            p.onNewChannelCreatedEvent(serverConnectionHandlerID, channelID, channelParentID, invokerID, invokerName, invokerUniqueIdentifier)

    @staticmethod
    def onDelChannelEvent(serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        for p in PluginHost.running:
            p.onDelChannelEvent(serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier)

    @staticmethod
    def onChannelMoveEvent(serverConnectionHandlerID, channelID, newChannelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        for p in PluginHost.running:
            p.onChannelMoveEvent(serverConnectionHandlerID, channelID, newChannelParentID, invokerID, invokerName, invokerUniqueIdentifier)

    @staticmethod
    def onUpdateChannelEvent(serverConnectionHandlerID, channelID):
        for p in PluginHost.running:
            p.onUpdateChannelEvent(serverConnectionHandlerID, channelID)

    @staticmethod
    def onUpdateChannelEditedEvent(serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        for p in PluginHost.running:
            p.onUpdateChannelEditedEvent(serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier)

    @staticmethod
    def onUpdateClientEvent(serverConnectionHandlerID, clientID, invokerID, invokerName, invokerUniqueIdentifier):
        for p in PluginHost.running:
            p.onUpdateClientEvent(serverConnectionHandlerID, clientID, invokerID, invokerName, invokerUniqueIdentifier)

    @staticmethod
    def onClientMoveEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moveMessage):
        for p in PluginHost.running:
            p.onClientMoveEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moveMessage)

    @staticmethod
    def onClientMoveSubscriptionEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility):
        for p in PluginHost.running:
            p.onClientMoveSubscriptionEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility)

    @staticmethod
    def onClientMoveTimeoutEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, timeoutMessage):
        for p in PluginHost.running:
            p.onClientMoveTimeoutEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, timeoutMessage)

    @staticmethod
    def onClientMoveMovedEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moverID, moverName, moverUniqueIdentifier, moveMessage):
        for p in PluginHost.running:
            p.onClientMoveMovedEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moverID, moverName, moverUniqueIdentifier, moveMessage)

    @staticmethod
    def onClientKickFromChannelEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        for p in PluginHost.running:
            p.onClientKickFromChannelEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage)

    @staticmethod
    def onClientKickFromServerEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        for p in PluginHost.running:
            p.onClientKickFromServerEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage)

    @staticmethod
    def onClientIDsEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientID, clientName):
        for p in PluginHost.running:
            p.onClientIDsEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientID, clientName)

    @staticmethod
    def onClientIDsFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onClientIDsFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onServerEditedEvent(serverConnectionHandlerID, editerID, editerName, editerUniqueIdentifier):
        for p in PluginHost.running:
            p.onServerEditedEvent(serverConnectionHandlerID, editerID, editerName, editerUniqueIdentifier)

    @staticmethod
    def onServerUpdatedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onServerUpdatedEvent(serverConnectionHandlerID)

    @staticmethod
    def onServerErrorEvent(serverConnectionHandlerID, errorMessage, error, returnCode, extraMessage):
        for p in PluginHost.running:
            p.onServerErrorEvent(serverConnectionHandlerID, errorMessage, error, returnCode, extraMessage)

    @staticmethod
    def onServerStopEvent(serverConnectionHandlerID, shutdownMessage):
        for p in PluginHost.running:
            p.onServerStopEvent(serverConnectionHandlerID, shutdownMessage)

    @staticmethod
    def onTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored):
        for p in PluginHost.running:
            p.onTextMessageEvent(serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored)

    @staticmethod
    def onTalkStatusChangeEvent(serverConnectionHandlerID, status, isReceivedWhisper, clientID):
        for p in PluginHost.running:
            p.onTalkStatusChangeEvent(serverConnectionHandlerID, status, isReceivedWhisper, clientID)

    @staticmethod
    def onConnectionInfoEvent(serverConnectionHandlerID, clientID):
        for p in PluginHost.running:
            p.onConnectionInfoEvent(serverConnectionHandlerID, clientID)

    @staticmethod
    def onServerConnectionInfoEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onServerConnectionInfoEvent(serverConnectionHandlerID)

    @staticmethod
    def onChannelSubscribeEvent(serverConnectionHandlerID, channelID):
        for p in PluginHost.running:
            p.onChannelSubscribeEvent(serverConnectionHandlerID, channelID)

    @staticmethod
    def onChannelSubscribeFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onChannelSubscribeFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onChannelUnsubscribeEvent(serverConnectionHandlerID, channelID):
        for p in PluginHost.running:
            p.onChannelUnsubscribeEvent(serverConnectionHandlerID, channelID)

    @staticmethod
    def onChannelUnsubscribeFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onChannelUnsubscribeFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onChannelDescriptionUpdateEvent(serverConnectionHandlerID, channelID):
        for p in PluginHost.running:
            p.onChannelDescriptionUpdateEvent(serverConnectionHandlerID, channelID)

    @staticmethod
    def onChannelPasswordChangedEvent(serverConnectionHandlerID, channelID):
        for p in PluginHost.running:
            p.onChannelPasswordChangedEvent(serverConnectionHandlerID, channelID)

    @staticmethod
    def onPlaybackShutdownCompleteEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onPlaybackShutdownCompleteEvent(serverConnectionHandlerID)

    @staticmethod
    def onSoundDeviceListChangedEvent(modeID, playOrCap):
        for p in PluginHost.running:
            p.onSoundDeviceListChangedEvent(modeID, playOrCap)

    @staticmethod
    def onEditPlaybackVoiceDataEvent(serverConnectionHandlerID, clientID, samples, sampleCount, channels):
        for p in PluginHost.running:
            p.onEditPlaybackVoiceDataEvent(serverConnectionHandlerID, clientID, samples, sampleCount, channels)

    @staticmethod
    def onEditPostProcessVoiceDataEvent(serverConnectionHandlerID, clientID, samples, sampleCount, channels, channelSpeakerArray, channelFillMask):
        for p in PluginHost.running:
            p.onEditPostProcessVoiceDataEvent(serverConnectionHandlerID, clientID, samples, sampleCount, channels, channelSpeakerArray, channelFillMask)

    @staticmethod
    def onEditMixedPlaybackVoiceDataEvent(serverConnectionHandlerID, samples, sampleCount, channels, channelSpeakerArray, channelFillMask):
        for p in PluginHost.running:
            p.onEditMixedPlaybackVoiceDataEvent(serverConnectionHandlerID, samples, sampleCount, channels, channelSpeakerArray, channelFillMask)

    @staticmethod
    def onEditCapturedVoiceDataEvent(serverConnectionHandlerID, samples, sampleCount, channels, edited):
        for p in PluginHost.running:
            p.onEditCapturedVoiceDataEvent(serverConnectionHandlerID, samples, sampleCount, channels, edited)

    @staticmethod
    def onCustom3dRolloffCalculationClientEvent(serverConnectionHandlerID, clientID, distance, volume):
        for p in PluginHost.running:
            p.onCustom3dRolloffCalculationClientEvent(serverConnectionHandlerID, clientID, distance, volume)

    @staticmethod
    def onCustom3dRolloffCalculationWaveEvent(serverConnectionHandlerID, waveHandle, distance, volume):
        for p in PluginHost.running:
            p.onCustom3dRolloffCalculationWaveEvent(serverConnectionHandlerID, waveHandle, distance, volume)

    @staticmethod
    def onUserLoggingMessageEvent(logMessage, logLevel, logChannel, logID, logTime, completeLogString):
        for p in PluginHost.running:
            p.onUserLoggingMessageEvent(logMessage, logLevel, logChannel, logID, logTime, completeLogString)

    @staticmethod
    def onClientBanFromServerEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, time, kickMessage):
        for p in PluginHost.running:
            p.onClientBanFromServerEvent(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, time, kickMessage)

    @staticmethod
    def onClientPokeEvent(serverConnectionHandlerID, fromClientID, pokerName, pokerUniqueIdentity, message, ffIgnored):
        for p in PluginHost.running:
            p.onClientPokeEvent(serverConnectionHandlerID, fromClientID, pokerName, pokerUniqueIdentity, message, ffIgnored)

    @staticmethod
    def onClientSelfVariableUpdateEvent(serverConnectionHandlerID, flag, oldValue, newValue):
        for p in PluginHost.running:
            p.onClientSelfVariableUpdateEvent(serverConnectionHandlerID, flag, oldValue, newValue)

    @staticmethod
    def onFileListEvent(serverConnectionHandlerID, channelID, path, name, size, datetime, ptype, incompletesize, returnCode):
        for p in PluginHost.running:
            p.onFileListEvent(serverConnectionHandlerID, channelID, path, name, size, datetime, ptype, incompletesize, returnCode)

    @staticmethod
    def onFileListFinishedEvent(serverConnectionHandlerID, channelID, path):
        for p in PluginHost.running:
            p.onFileListFinishedEvent(serverConnectionHandlerID, channelID, path)

    @staticmethod
    def onFileInfoEvent(serverConnectionHandlerID, channelID, name, size, datetime):
        for p in PluginHost.running:
            p.onFileInfoEvent(serverConnectionHandlerID, channelID, name, size, datetime)

    @staticmethod
    def onServerGroupListEvent(serverConnectionHandlerID, serverGroupID, name, ptype, iconID, saveDB):
        for p in PluginHost.running:
            p.onServerGroupListEvent(serverConnectionHandlerID, serverGroupID, name, ptype, iconID, saveDB)

    @staticmethod
    def onServerGroupListFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onServerGroupListFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onServerGroupByClientIDEvent(serverConnectionHandlerID, name, serverGroupList, clientDatabaseID):
        for p in PluginHost.running:
            p.onServerGroupByClientIDEvent(serverConnectionHandlerID, name, serverGroupList, clientDatabaseID)

    @staticmethod
    def onServerGroupPermListEvent(serverConnectionHandlerID, serverGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        for p in PluginHost.running:
            p.onServerGroupPermListEvent(serverConnectionHandlerID, serverGroupID, permissionID, permissionValue, permissionNegated, permissionSkip)

    @staticmethod
    def onServerGroupPermListFinishedEvent(serverConnectionHandlerID, serverGroupID):
        for p in PluginHost.running:
            p.onServerGroupPermListFinishedEvent(serverConnectionHandlerID, serverGroupID)

    @staticmethod
    def onServerGroupClientListEvent(serverConnectionHandlerID, serverGroupID, clientDatabaseID, clientNameIdentifier, clientUniqueID):
        for p in PluginHost.running:
            p.onServerGroupClientListEvent(serverConnectionHandlerID, serverGroupID, clientDatabaseID, clientNameIdentifier, clientUniqueID)

    @staticmethod
    def onChannelGroupListEvent(serverConnectionHandlerID, channelGroupID, name, ptype, iconID, saveDB):
        for p in PluginHost.running:
            p.onChannelGroupListEvent(serverConnectionHandlerID, channelGroupID, name, ptype, iconID, saveDB)

    @staticmethod
    def onChannelGroupListFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onChannelGroupListFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onChannelGroupPermListEvent(serverConnectionHandlerID, channelGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        for p in PluginHost.running:
            p.onChannelGroupPermListEvent(serverConnectionHandlerID, channelGroupID, permissionID, permissionValue, permissionNegated, permissionSkip)

    @staticmethod
    def onChannelGroupPermListFinishedEvent(serverConnectionHandlerID, channelGroupID):
        for p in PluginHost.running:
            p.onChannelGroupPermListFinishedEvent(serverConnectionHandlerID, channelGroupID)

    @staticmethod
    def onChannelPermListEvent(serverConnectionHandlerID, channelID, permissionID, permissionValue, permissionNegated, permissionSkip):
        for p in PluginHost.running:
            p.onChannelPermListEvent(serverConnectionHandlerID, channelID, permissionID, permissionValue, permissionNegated, permissionSkip)

    @staticmethod
    def onChannelPermListFinishedEvent(serverConnectionHandlerID, channelID):
        for p in PluginHost.running:
            p.onChannelPermListFinishedEvent(serverConnectionHandlerID, channelID)

    @staticmethod
    def onClientPermListEvent(serverConnectionHandlerID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        for p in PluginHost.running:
            p.onClientPermListEvent(serverConnectionHandlerID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip)

    @staticmethod
    def onClientPermListFinishedEvent(serverConnectionHandlerID, clientDatabaseID):
        for p in PluginHost.running:
            p.onClientPermListFinishedEvent(serverConnectionHandlerID, clientDatabaseID)

    @staticmethod
    def onChannelClientPermListEvent(serverConnectionHandlerID, channelID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        for p in PluginHost.running:
            p.onChannelClientPermListEvent(serverConnectionHandlerID, channelID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip)

    @staticmethod
    def onChannelClientPermListFinishedEvent(serverConnectionHandlerID, channelID, clientDatabaseID):
        for p in PluginHost.running:
            p.onChannelClientPermListFinishedEvent(serverConnectionHandlerID, channelID, clientDatabaseID)

    @staticmethod
    def onClientChannelGroupChangedEvent(serverConnectionHandlerID, channelGroupID, channelID, clientID, invokerClientID, invokerName, invokerUniqueIdentity):
        for p in PluginHost.running:
            p.onClientChannelGroupChangedEvent(serverConnectionHandlerID, channelGroupID, channelID, clientID, invokerClientID, invokerName, invokerUniqueIdentity)

    @staticmethod
    def onServerPermissionErrorEvent(serverConnectionHandlerID, errorMessage, error, returnCode, failedPermissionID):
        for p in PluginHost.running:
            p.onServerPermissionErrorEvent(serverConnectionHandlerID, errorMessage, error, returnCode, failedPermissionID)

    @staticmethod
    def onPermissionListGroupEndIDEvent(serverConnectionHandlerID, groupEndID):
        for p in PluginHost.running:
            p.onPermissionListGroupEndIDEvent(serverConnectionHandlerID, groupEndID)

    @staticmethod
    def onPermissionListEvent(serverConnectionHandlerID, permissionID, permissionName, permissionDescription):
        for p in PluginHost.running:
            p.onPermissionListEvent(serverConnectionHandlerID, permissionID, permissionName, permissionDescription)

    @staticmethod
    def onPermissionListFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onPermissionListFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onPermissionOverviewEvent(serverConnectionHandlerID, clientDatabaseID, channelID, overviewType, overviewID1, overviewID2, permissionID, permissionValue, permissionNegated, permissionSkip):
        for p in PluginHost.running:
            p.onPermissionOverviewEvent(serverConnectionHandlerID, clientDatabaseID, channelID, overviewType, overviewID1, overviewID2, permissionID, permissionValue, permissionNegated, permissionSkip)

    @staticmethod
    def onPermissionOverviewFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onPermissionOverviewFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onServerGroupClientAddedEvent(serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        for p in PluginHost.running:
            p.onServerGroupClientAddedEvent(serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity)

    @staticmethod
    def onServerGroupClientDeletedEvent(serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        for p in PluginHost.running:
            p.onServerGroupClientDeletedEvent(serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity)

    @staticmethod
    def onClientNeededPermissionsEvent(serverConnectionHandlerID, permissionID, permissionValue):
        for p in PluginHost.running:
            p.onClientNeededPermissionsEvent(serverConnectionHandlerID, permissionID, permissionValue)

    @staticmethod
    def onClientNeededPermissionsFinishedEvent(serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onClientNeededPermissionsFinishedEvent(serverConnectionHandlerID)

    @staticmethod
    def onFileTransferStatusEvent(transferID, status, statusMessage, remotefileSize, serverConnectionHandlerID):
        for p in PluginHost.running:
            p.onFileTransferStatusEvent(transferID, status, statusMessage, remotefileSize, serverConnectionHandlerID)

    @staticmethod
    def onClientChatClosedEvent(serverConnectionHandlerID, clientID, clientUniqueIdentity):
        for p in PluginHost.running:
            p.onClientChatClosedEvent(serverConnectionHandlerID, clientID, clientUniqueIdentity)

    @staticmethod
    def onClientChatComposingEvent(serverConnectionHandlerID, clientID, clientUniqueIdentity):
        for p in PluginHost.running:
            p.onClientChatComposingEvent(serverConnectionHandlerID, clientID, clientUniqueIdentity)

    @staticmethod
    def onServerLogEvent(serverConnectionHandlerID, logMsg):
        for p in PluginHost.running:
            p.onServerLogEvent(serverConnectionHandlerID, logMsg)

    @staticmethod
    def onServerLogFinishedEvent(serverConnectionHandlerID, lastPos, fileSize):
        for p in PluginHost.running:
            p.onServerLogFinishedEvent(serverConnectionHandlerID, lastPos, fileSize)

    @staticmethod
    def onMessageListEvent(serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, timestamp, flagRead):
        for p in PluginHost.running:
            p.onMessageListEvent(serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, timestamp, flagRead)

    @staticmethod
    def onMessageGetEvent(serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, message, timestamp):
        for p in PluginHost.running:
            p.onMessageGetEvent(serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, message, timestamp)

    @staticmethod
    def onClientDBIDfromUIDEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID):
        for p in PluginHost.running:
            p.onClientDBIDfromUIDEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID)

    @staticmethod
    def onClientNamefromUIDEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        for p in PluginHost.running:
            p.onClientNamefromUIDEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName)

    @staticmethod
    def onClientNamefromDBIDEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        for p in PluginHost.running:
            p.onClientNamefromDBIDEvent(serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName)

    @staticmethod
    def onComplainListEvent(serverConnectionHandlerID, targetClientDatabaseID, targetClientNickName, fromClientDatabaseID, fromClientNickName, complainReason, timestamp):
        for p in PluginHost.running:
            p.onComplainListEvent(serverConnectionHandlerID, targetClientDatabaseID, targetClientNickName, fromClientDatabaseID, fromClientNickName, complainReason, timestamp)

    @staticmethod
    def onBanListEvent(serverConnectionHandlerID, banid, ip, name, uid, creationTime, durationTime, invokerName, invokercldbid, invokeruid, reason, numberOfEnforcements, lastNickName):
        for p in PluginHost.running:
            p.onBanListEvent(serverConnectionHandlerID, banid, ip, name, uid, creationTime, durationTime, invokerName, invokercldbid, invokeruid, reason, numberOfEnforcements, lastNickName)

    @staticmethod
    def onClientServerQueryLoginPasswordEvent(serverConnectionHandlerID, loginPassword):
        for p in PluginHost.running:
            p.onClientServerQueryLoginPasswordEvent(serverConnectionHandlerID, loginPassword)

    @staticmethod
    def onPluginCommandEvent(serverConnectionHandlerID, pluginName, pluginCommand):
        for p in PluginHost.running:
            p.onPluginCommandEvent(serverConnectionHandlerID, pluginName, pluginCommand)

    @staticmethod
    def onIncomingClientQueryEvent(serverConnectionHandlerID, commandText):
        for p in PluginHost.running:
            p.onIncomingClientQueryEvent(serverConnectionHandlerID, commandText)

    @staticmethod
    def onServerTemporaryPasswordListEvent(serverConnectionHandlerID, clientNickname, uniqueClientIdentifier, description, password, timestampStart, timestampEnd, targetChannelID, targetChannelPW):
        for p in PluginHost.running:
            p.onServerTemporaryPasswordListEvent(serverConnectionHandlerID, clientNickname, uniqueClientIdentifier, description, password, timestampStart, timestampEnd, targetChannelID, targetChannelPW)

    @staticmethod
    def onAvatarUpdated(serverConnectionHandlerID, clientID, avatarPath):
        for p in PluginHost.running:
            p.onAvatarUpdated(serverConnectionHandlerID, clientID, avatarPath)

    @staticmethod
    def onHotkeyEvent(keyword):
        for p in PluginHost.running:
            p.onHotkeyEvent(keyword)

    @staticmethod
    def onHotkeyRecordedEvent(keyword, key):
        for p in PluginHost.running:
            p.onHotkeyRecordedEvent(keyword, key)



class ts3Plugin(object, metaclass = PluginMetaClass):
    name = "pluginname"
    version = "1.0"
    description = ""
    author = ""
    apiVersion = 20
    
    def processCommand(self, serverConnectionHandlerID, command):
        pass
    def currentServerConnectionChanged(self, serverConnectionHandlerID):
        pass
    def onConnectStatusChangeEvent(self, serverConnectionHandlerID, newStatus, errorNumber):
        pass
    def onNewChannelEvent(self, serverConnectionHandlerID, channelID, channelParentID):
        pass
    def onNewChannelCreatedEvent(self, serverConnectionHandlerID, channelID, channelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        pass
    def onDelChannelEvent(self, serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        pass
    def onChannelMoveEvent(self, serverConnectionHandlerID, channelID, newChannelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        pass
    def onUpdateChannelEvent(self, serverConnectionHandlerID, channelID):
        pass
    def onUpdateChannelEditedEvent(self, serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        pass
    def onUpdateClientEvent(self, serverConnectionHandlerID, clientID, invokerID, invokerName, invokerUniqueIdentifier):
        pass
    def onClientMoveEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moveMessage):
        pass
    def onClientMoveSubscriptionEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility):
        pass
    def onClientMoveTimeoutEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, timeoutMessage):
        pass
    def onClientMoveMovedEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moverID, moverName, moverUniqueIdentifier, moveMessage):
        pass
    def onClientKickFromChannelEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        pass
    def onClientKickFromServerEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        pass
    def onClientIDsEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientID, clientName):
        pass
    def onClientIDsFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onServerEditedEvent(self, serverConnectionHandlerID, editerID, editerName, editerUniqueIdentifier):
        pass
    def onServerUpdatedEvent(self, serverConnectionHandlerID):
        pass
    def onServerErrorEvent(self, serverConnectionHandlerID, errorMessage, error, returnCode, extraMessage):
        pass
    def onServerStopEvent(self, serverConnectionHandlerID, shutdownMessage):
        pass
    def onTextMessageEvent(self, serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored):
        pass
    def onTalkStatusChangeEvent(self, serverConnectionHandlerID, status, isReceivedWhisper, clientID):
        pass
    def onConnectionInfoEvent(self, serverConnectionHandlerID, clientID):
        pass
    def onServerConnectionInfoEvent(self, serverConnectionHandlerID):
        pass
    def onChannelSubscribeEvent(self, serverConnectionHandlerID, channelID):
        pass
    def onChannelSubscribeFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onChannelUnsubscribeEvent(self, serverConnectionHandlerID, channelID):
        pass
    def onChannelUnsubscribeFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onChannelDescriptionUpdateEvent(self, serverConnectionHandlerID, channelID):
        pass
    def onChannelPasswordChangedEvent(self, serverConnectionHandlerID, channelID):
        pass
    def onPlaybackShutdownCompleteEvent(self, serverConnectionHandlerID):
        pass
    def onSoundDeviceListChangedEvent(self, modeID, playOrCap):
        pass
    def onEditPlaybackVoiceDataEvent(self, serverConnectionHandlerID, clientID, samples, sampleCount, channels):
        pass
    def onEditPostProcessVoiceDataEvent(self, serverConnectionHandlerID, clientID, samples, sampleCount, channels, channelSpeakerArray, channelFillMask):
        pass
    def onEditMixedPlaybackVoiceDataEvent(self, serverConnectionHandlerID, samples, sampleCount, channels, channelSpeakerArray, channelFillMask):
        pass
    def onEditCapturedVoiceDataEvent(self, serverConnectionHandlerID, samples, sampleCount, channels, edited):
        pass
    def onCustom3dRolloffCalculationClientEvent(self, serverConnectionHandlerID, clientID, distance, volume):
        pass
    def onCustom3dRolloffCalculationWaveEvent(self, serverConnectionHandlerID, waveHandle, distance, volume):
        pass
    def onUserLoggingMessageEvent(self, logMessage, logLevel, logChannel, logID, logTime, completeLogString):
        pass
    def onClientBanFromServerEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, time, kickMessage):
        pass
    def onClientPokeEvent(self, serverConnectionHandlerID, fromClientID, pokerName, pokerUniqueIdentity, message, ffIgnored):
        pass
    def onClientSelfVariableUpdateEvent(self, serverConnectionHandlerID, flag, oldValue, newValue):
        pass
    def onFileListEvent(self, serverConnectionHandlerID, channelID, path, name, size, datetime, ptype, incompletesize, returnCode):
        pass
    def onFileListFinishedEvent(self, serverConnectionHandlerID, channelID, path):
        pass
    def onFileInfoEvent(self, serverConnectionHandlerID, channelID, name, size, datetime):
        pass
    def onServerGroupListEvent(self, serverConnectionHandlerID, serverGroupID, name, ptype, iconID, saveDB):
        pass
    def onServerGroupListFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onServerGroupByClientIDEvent(self, serverConnectionHandlerID, name, serverGroupList, clientDatabaseID):
        pass
    def onServerGroupPermListEvent(self, serverConnectionHandlerID, serverGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        pass
    def onServerGroupPermListFinishedEvent(self, serverConnectionHandlerID, serverGroupID):
        pass
    def onServerGroupClientListEvent(self, serverConnectionHandlerID, serverGroupID, clientDatabaseID, clientNameIdentifier, clientUniqueID):
        pass
    def onChannelGroupListEvent(self, serverConnectionHandlerID, channelGroupID, name, ptype, iconID, saveDB):
        pass
    def onChannelGroupListFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onChannelGroupPermListEvent(self, serverConnectionHandlerID, channelGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        pass
    def onChannelGroupPermListFinishedEvent(self, serverConnectionHandlerID, channelGroupID):
        pass
    def onChannelPermListEvent(self, serverConnectionHandlerID, channelID, permissionID, permissionValue, permissionNegated, permissionSkip):
        pass
    def onChannelPermListFinishedEvent(self, serverConnectionHandlerID, channelID):
        pass
    def onClientPermListEvent(self, serverConnectionHandlerID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        pass
    def onClientPermListFinishedEvent(self, serverConnectionHandlerID, clientDatabaseID):
        pass
    def onChannelClientPermListEvent(self, serverConnectionHandlerID, channelID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        pass
    def onChannelClientPermListFinishedEvent(self, serverConnectionHandlerID, channelID, clientDatabaseID):
        pass
    def onClientChannelGroupChangedEvent(self, serverConnectionHandlerID, channelGroupID, channelID, clientID, invokerClientID, invokerName, invokerUniqueIdentity):
        pass
    def onServerPermissionErrorEvent(self, serverConnectionHandlerID, errorMessage, error, returnCode, failedPermissionID):
        pass
    def onPermissionListGroupEndIDEvent(self, serverConnectionHandlerID, groupEndID):
        pass
    def onPermissionListEvent(self, serverConnectionHandlerID, permissionID, permissionName, permissionDescription):
        pass
    def onPermissionListFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onPermissionOverviewEvent(self, serverConnectionHandlerID, clientDatabaseID, channelID, overviewType, overviewID1, overviewID2, permissionID, permissionValue, permissionNegated, permissionSkip):
        pass
    def onPermissionOverviewFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onServerGroupClientAddedEvent(self, serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        pass
    def onServerGroupClientDeletedEvent(self, serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        pass
    def onClientNeededPermissionsEvent(self, serverConnectionHandlerID, permissionID, permissionValue):
        pass
    def onClientNeededPermissionsFinishedEvent(self, serverConnectionHandlerID):
        pass
    def onFileTransferStatusEvent(self, transferID, status, statusMessage, remotefileSize, serverConnectionHandlerID):
        pass
    def onClientChatClosedEvent(self, serverConnectionHandlerID, clientID, clientUniqueIdentity):
        pass
    def onClientChatComposingEvent(self, serverConnectionHandlerID, clientID, clientUniqueIdentity):
        pass
    def onServerLogEvent(self, serverConnectionHandlerID, logMsg):
        pass
    def onServerLogFinishedEvent(self, serverConnectionHandlerID, lastPos, fileSize):
        pass
    def onMessageListEvent(self, serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, timestamp, flagRead):
        pass
    def onMessageGetEvent(self, serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, message, timestamp):
        pass
    def onClientDBIDfromUIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID):
        pass
    def onClientNamefromUIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        pass
    def onClientNamefromDBIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        pass
    def onComplainListEvent(self, serverConnectionHandlerID, targetClientDatabaseID, targetClientNickName, fromClientDatabaseID, fromClientNickName, complainReason, timestamp):
        pass
    def onBanListEvent(self, serverConnectionHandlerID, banid, ip, name, uid, creationTime, durationTime, invokerName, invokercldbid, invokeruid, reason, numberOfEnforcements, lastNickName):
        pass
    def onClientServerQueryLoginPasswordEvent(self, serverConnectionHandlerID, loginPassword):
        pass
    def onPluginCommandEvent(self, serverConnectionHandlerID, pluginName, pluginCommand):
        pass
    def onIncomingClientQueryEvent(self, serverConnectionHandlerID, commandText):
        pass
    def onServerTemporaryPasswordListEvent(self, serverConnectionHandlerID, clientNickname, uniqueClientIdentifier, description, password, timestampStart, timestampEnd, targetChannelID, targetChannelPW):
        pass
    def onAvatarUpdated(self, serverConnectionHandlerID, clientID, avatarPath):
        pass
    def onHotkeyEvent(self, keyword):
        pass
    def onHotkeyRecordedEvent(self, keyword, key):
        pass

