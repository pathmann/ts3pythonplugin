class ts3helpers:
    """

    """
    @staticmethod
    def getSettingsTables():
        """
        Returns the list of available tables in the client's settings database.
        """
    @staticmethod
    def getSettingsKeys(table):
        """
        Returns the list of available keys in a table in the client's settings database.
        @param table: the table of the database
        @type table: string
        """
    @staticmethod
    def getSettingsVariable(table, key):
        """
        Returns a variable out the client's settings.
        @param table:
        @type table: string
        @param key: the key of the variable
        @type key: string
        @return: the value of the key in the database's table
        @rtype: string
        """
    @staticmethod
    def setSettingsVariable(table, key):
        """
        Sets a variable in the client's settings database.
        @param table:
        @type table: string
        @param key: the key of the variable
        @type key: string
        """
    @staticmethod
    def saveScript(modulename):
        """
        Stores the module build with the manually executed commands to a file in the scripts directory.
        @param modulename: the name of the module (without filename extension)
        @type modulename: string
        """
    @staticmethod
    def clearScript():
        """
        Clears the module build with the manually executed commands.
        """
