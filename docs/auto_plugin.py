class ts3Plugin:
    """
    Baseclass for ts3 python plugins.
    Inherit methods to subscribe to events.

    The C-Plugin will create an object of every activated plugin.
    """

    def __init__(self):
        """

        """

    def __del__(self):
        """

        """
    def currentServerConnectionChanged(self, serverConnectionHandlerID):
        """
        Triggers when the current active serverconnection changes.
        @param serverConnectionHandlerID: the ID of the new active serverconnection
        @type serverConnectionHandlerID: int
        """
    def onConnectStatusChangeEvent(self, serverConnectionHandlerID, newStatus, errorNumber):
        """
        Triggers when the connect status changes.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param newStatus: the new status, see ts3defines.ConnectStatus
        @type newStatus: int
        @param errorNumber: gives the errorcode if something went wrong
        @type errorNumber: int
        """
    def onNewChannelEvent(self, serverConnectionHandlerID, channelID, channelParentID):
        """
        Triggers after a new connection has established for each channel on the server to announce every channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param channelParentID: the ID of the parent channel
        @type channelParentID: int
        """
    def onNewChannelCreatedEvent(self, serverConnectionHandlerID, channelID, channelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a new channel was created.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the new channel
        @type channelID: int
        @param channelParentID: the ID of the parent channel
        @type channelParentID: int
        @param invokerID: the ID of the client who created the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
    def onDelChannelEvent(self, serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a channel was deleted.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the deleted channel
        @type channelID: int
        @param invokerID: the ID of the client who deleted the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
    def onChannelMoveEvent(self, serverConnectionHandlerID, channelID, newChannelParentID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a channel was moved to a new parent or the order was changed.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param newChannelParentID: the ID of the new parent channel
        @type newChannelParentID: int
        @param invokerID: the ID of the client who moved the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
    def onUpdateChannelEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when a channel was modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
    def onUpdateChannelEditedEvent(self, serverConnectionHandlerID, channelID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers when a channel was modified by a client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param invokerID: the ID of the client who edited the channel
        @type invokerID: int
        @param invokerName: the current nickname of the client
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client
        @type invokerUniqueIdentifier: string
        """
    def onUpdateClientEvent(self, serverConnectionHandlerID, clientID, invokerID, invokerName, invokerUniqueIdentifier):
        """
        Triggers after a client variable changed or after requestClientVariables was called.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the updated client
        @type clientID: int
        @param invokerID: the ID of the client who edited the variable
        @type invokerID: int
        @param invokerName: the current nickname of the client who edited the variable
        @type invokerName: string
        @param invokerUniqueIdentifier: the UID of the client who edited the variable
        @type invokerUniqueIdentifier: string
        """
    def onClientMoveEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moveMessage):
        """
        Triggers when a client moves to another channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param oldChannelID: the ID of the old channel, if set to 0, the client has just connected to the server
        @type oldChannelID: int
        @param newChannelID: the ID of the new channel, if set to 0, the client disconnected from the server
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        @param moveMessage: the quitmessage, if the client left the server
        @type moveMessage: string
        """
    def onClientMoveSubscriptionEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility):
        """
        Triggers for each client in a channel after subscribing or unsubscribing to resp. from that channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param oldChannelID: the ID of the subscribed channel where the client left visibility //TODO: set to 0 if subscribing to the channel?
        @type oldChannelID: int
        @param newChannelID: ID of the subscribed channel where the client entered visibility //TODO: what if visibility in [LEAVE, RETAIN]?
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        """
    def onClientMoveTimeoutEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, timeoutMessage):
        """
        Triggers when a client drops his connection.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param oldChannelID: the ID of the channel the client was in last
        @type oldChannelID: int
        @param newChannelID: always set to 0
        @type newChannelID: int
        @param visibility: Always set to ts3dfines.Visbility.LEAVE_VISIBILITY.
        @type visibility: int
        @param timeoutMessage: message giving the reason for the timeout //wäh?
        @type timeoutMessage: string
        """
    def onClientMoveMovedEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, moverID, moverName, moverUniqueIdentifier, moveMessage):
        """
        Triggers when a client is moved to another channel by another client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the moved client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: the ID of the new channel
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        @param moverID: the ID of the moving client
        @type moverID: int
        @param moverName: the current nickname of the moving client
        @type moverName: string
        @param moverUniqueIdentifier: the UID of the moving client
        @type moverUniqueIdentifier: string
        @param moveMessage: message giving the reason for the move
        @type moveMessage: string
        """
    def onClientKickFromChannelEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        """
        Triggers when a client is kicked from his current channel.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the kicked client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: the ID of the default channel, the client was kicked to
        @type newChannelID: int
        @param visibility: defines if the client entered, left or retained the current view, see ts3defines.Visibility
        @type visibility: int
        @param kickerID: the ID of the kicking client
        @type kickerID: int
        @param kickerName: the current name of the kicking client
        @type kickerName: string
        @param kickerUniqueIdentifier: the UID of the kicking client
        @type kickerUniqueIdentifier: string
        @param kickMessage: message giving the reason for the kick
        @type kickMessage: string
        """
    def onClientKickFromServerEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, kickMessage):
        """
        Triggers when a client is kicked from the server.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the kicked client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: always set to 0
        @type newChannelID: int
        @param visibility: always set to ts3defines.Visibility.LEAVE_VISBILITY
        @type visibility: int
        @param kickerID: the ID of the kicking client
        @type kickerID: int
        @param kickerName: the current nickname of the kicking client
        @type kickerName: string
        @param kickerUniqueIdentifier: the UID of the kicking client
        @type kickerUniqueIdentifier: string
        @param kickMessage: message giving the reason for the kick
        @type kickMessage: string
        """
    def onClientIDsEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientID, clientName):
        """
        Triggers after requesting a users's client IDs with requestClientIDs.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the client
        @type uniqueClientIdentifier: string
        @param clientID: the ID of the client
        @type clientID: int
        @param clientName: the current nickname of the client
        @type clientName: string
        """
    def onClientIDsFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all requested client IDs are announced by onClientIDsEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onServerEditedEvent(self, serverConnectionHandlerID, editerID, editerName, editerUniqueIdentifier):
        """
        Triggers when a server variable was modified by a client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param editerID: the ID of the client
        @type editerID: int
        @param editerName: the current nickname of the client
        @type editerName: string
        @param editerUniqueIdentifier: the UID of the client
        @type editerUniqueIdentifier: string
        """
    def onServerUpdatedEvent(self, serverConnectionHandlerID):
        """
        Triggers when the server variables are available after requesting with requestServerVariables.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onServerErrorEvent(self, serverConnectionHandlerID, errorMessage, error, returnCode, extraMessage):
        """
        Triggers when a server error occured.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param errorMessage: a description of the error
        @type errorMessage: string
        @param error: the errorcode
        @type error: int
        @param returnCode: the returnCode created with createReturnCode and passed to the function which raised the error, empty string if no returnCode was passed
        @type returnCode: string
        @param extraMessage: additional information to the error or an empty string
        @type extraMessage: string
        @return: returns 1 (or True) to let the client ignore the error
        @rtype: int or bool
        """
    def onServerStopEvent(self, serverConnectionHandlerID, shutdownMessage):
        """
        Triggers when a server shuts down.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param shutdownMessage: message giving the reason for the shutdown
        @type shutdownMessage: string
        """
    def onTextMessageEvent(self, serverConnectionHandlerID, targetMode, toID, fromID, fromName, fromUniqueIdentifier, message, ffIgnored):
        """
        Triggers when a text message is received.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param targetMode: defines the target of the text message, see ts3defines.TextMessageTargetMode
        @type targetMode: int
        @param toID: the ID of the target //TODO: muss das nicht unit64 sein?
        @type toID: int
        @param fromID: the ID of the sending client
        @type fromID: int
        @param fromName: the current nickname of the sending client
        @type fromName: string
        @param fromUniqueIdentifier: the UID of the sending client
        @type fromUniqueIdentifier: string
        @param message: the text message
        @type message: string
        @param ffIgnored: if set to 1 (or True) the friend/foe manager will ignore this message
        @type ffIgnored: int or bool
        @return: returns 1 (or True) to let the client ignore this message, 0 (or False) otherwise
        @rtype: int or bool
        """
    def onTalkStatusChangeEvent(self, serverConnectionHandlerID, status, isReceivedWhisper, clientID):
        """
        Triggers when a client starts or stops talking.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param status: specifies the talk status, see ts3defines.TalkStatus
        @type status: int
        @param isReceivedWhisper: is set to 1 (or True) if the event is caused by whispering, otherwise 0 (or False)
        @type isReceivedWhisper: int or bool
        @param clientID: the ID of the client
        @type clientID: int
        """
    def onConnectionInfoEvent(self, serverConnectionHandlerID, clientID):
        """
        Triggers when the connection info is available, requested with requestConnectionInfo.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        """
    def onServerConnectionInfoEvent(self, serverConnectionHandlerID):
        """
        //TODO: wäh?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onChannelSubscribeEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers for each channel subscribed to with requestChannelSubscribeAll.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
    def onChannelSubscribeFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when subscription to all channels with requestChannelSubscribeAll ended.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onChannelUnsubscribeEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers for each channel unsubscribed from with requestChannelUnsubscribeAll.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: 
        @type channelID: 
        """
    def onChannelUnsubscribeFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when unsubscription from all channels with requestChannelUnsubscribeAll ended.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onChannelDescriptionUpdateEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when the description of a channel is edited.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
    def onChannelPasswordChangedEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when the password to a channel is modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
    def onPlaybackShutdownCompleteEvent(self, serverConnectionHandlerID):
        """
        Triggers when it's safe to close the playback device.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onSoundDeviceListChangedEvent(self, modeID, playOrCap):
        """
        //TODO: wäh?
        @param modeID: 
        @type modeID: string
        @param playOrCap: 
        @type playOrCap: int
        """
    def onUserLoggingMessageEvent(self, logMessage, logLevel, logChannel, logID, logTime, completeLogString):
        """
        Triggers when a message is logged.
        @param logMessage: the message
        @type logMessage: string
        @param logLevel: level of the message, see ts3defines.LogLevel
        @type logLevel: int
        @param logChannel: the channel of the message
        @type logChannel: string
        @param logID: the ID of the serverconnection of the message or 0 if none given
        @type logID: int
        @param logTime: the formated date and time of the message
        @type logTime: string
        @param completeLogString: all log parameters combined for convinience
        @type completeLogString: string
        """
    def onClientBanFromServerEvent(self, serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility, kickerID, kickerName, kickerUniqueIdentifier, time, kickMessage):
        """
        Triggers when a client is banned from the server.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the banned client
        @type clientID: int
        @param oldChannelID: the ID of the old channel
        @type oldChannelID: int
        @param newChannelID: always set to 0
        @type newChannelID: int
        @param visibility: always set to ts3defines.Visbility.LEAVE_VISIBILITY
        @type visibility: int
        @param kickerID: the ID of the banning client
        @type kickerID: int
        @param kickerName: the current nickname of the banning client
        @type kickerName: string
        @param kickerUniqueIdentifier: the UID of the banning client
        @type kickerUniqueIdentifier: string
        @param time: the time of the ban //TODO: wäh? UTC?
        @type time: int
        @param kickMessage: the reason for the ban
        @type kickMessage: string
        """
    def onClientPokeEvent(self, serverConnectionHandlerID, fromClientID, pokerName, pokerUniqueIdentity, message, ffIgnored):
        """
        Triggers when the client is poked by another client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param fromClientID: the ID of the poking client
        @type fromClientID: int
        @param pokerName: the current nickname of the poking client
        @type pokerName: string
        @param pokerUniqueIdentity: the UID of the poking client
        @type pokerUniqueIdentity: string
        @param message: the message of the poke
        @type message: string
        @param ffIgnored: if set to 1 (or True) the friend/foe manager will ignore the poke
        @type ffIgnored: int or bool
        @return: returns 1 (or True) to let the client ignore this poke, 0 (or False) otherwise
        @rtype: int or bool
        """
    def onClientSelfVariableUpdateEvent(self, serverConnectionHandlerID, flag, oldValue, newValue):
        """
        Triggers when a client variable of the own client was modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param flag: the flag that has changed
        @type flag: int
        @param oldValue: the old value
        @type oldValue: string
        @param newValue: the new value
        @type newValue: string
        """
    def onFileListEvent(self, serverConnectionHandlerID, channelID, path, name, size, datetime, ptype, incompletesize, returnCode):
        """
        Triggers for each file and directory in a channel's path requested with requestFileList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param path: the path that was requested
        @type path: string
        @param name: the name of the file or directory
        @type name: string
        @param size: the total size of the file in Bytes, 0 if directory
        @type size: int
        @param datetime: time of last modification //TODO: UTC?
        @type datetime: int
        @param ptype: specifies, if the current entry is a file or directory, see ts3defines.FileListType
        @type ptype: int
        @param incompletesize: the currently uploaded size, if not completely uploaded, otherwise equals parameter size //TODO: or 0 if complete? or defininig Bytes missing?
        @type incompletesize: int
        @param returnCode: the returnCode passed to the request, or an empty string if none passed
        @type returnCode: string
        """
    def onFileListFinishedEvent(self, serverConnectionHandlerID, channelID, path):
        """
        Triggers when all files and directories of a path in a channel are announced by onFileListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param path: the path that was requested
        @type path: string
        """
    def onFileInfoEvent(self, serverConnectionHandlerID, channelID, name, size, datetime):
        """
        Triggers when the info on a file or directory in a channel is requested with requestFileInfo.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param name: name of the file or directory
        @type name: string
        @param size: size of the file in Bytes, or 0 if it's a directory
        @type size: int
        @param datetime: time of last modification //TODO: UTC?
        @type datetime: int
        """
    def onServerGroupListEvent(self, serverConnectionHandlerID, serverGroupID, name, ptype, iconID, saveDB):
        """
        Triggers for each servergroup requested with requestServerGroupList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param name: the name of the servergroup
        @type name: string
        @param type: type of the servergroup, see ts3defines.GroupType
        @type type: int
        @param iconID: the ID of the icon
        @type iconID: int
        @param saveDB: //TODO: wäh?
        @type saveDB: int
        """
    def onServerGroupListFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all servergroup are announced in onServerGroupListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onServerGroupByClientIDEvent(self, serverConnectionHandlerID, name, serverGroupList, clientDatabaseID):
        """
        Triggers for each servergroup of a client requested with requestServerGroupsByClientID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param name: the name of the servergroup
        @type name: string
        @param serverGroupList: //TODO: serverGroupID?
        @type serverGroupList: int
        @param clientDatabaseID: the database ID of the client
        @type clientDatabaseID: int
        """
    def onServerGroupPermListEvent(self, serverConnectionHandlerID, serverGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a servergroup requested by requestServerGroupPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
    def onServerGroupPermListFinishedEvent(self, serverConnectionHandlerID, serverGroupID):
        """
        Triggers when all permissions of a servergroup are announced by onServerGroupPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        """
    def onServerGroupClientListEvent(self, serverConnectionHandlerID, serverGroupID, clientDatabaseID, clientNameIdentifier, clientUniqueID):
        """
        Triggers for each user in a servergroup requested by requestServerGroupClientList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param clientNameIdentifier: //wäh? last nickname on the server?
        @type clientNameIdentifier: string
        @param clientUniqueID: the UID of the user
        @type clientUniqueID: int
        """
    def onChannelGroupListEvent(self, serverConnectionHandlerID, channelGroupID, name, ptype, iconID, saveDB):
        """
        Triggers for each channelgroup requested by requestChannelGroupList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the channelgroup
        @type channelGroupID: int
        @param name: the name of the channelgroup
        @type name: string
        @param type: type of the servergroup, see ts3defines.GroupType
        @type type: int
        @param iconID: the ID of the icon
        @type iconID: int
        @param saveDB: //TODO: wäh? siehe onServerGroupListEvent
        @type saveDB: int
        """
    def onChannelGroupListFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all channelgroups are announced by onChannelGroupListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onChannelGroupPermListEvent(self, serverConnectionHandlerID, channelGroupID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a channelgroup requested by requestChannelGroupPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the channelgroup
        @type channelGroupID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
    def onChannelGroupPermListFinishedEvent(self, serverConnectionHandlerID, channelGroupID):
        """
        Triggers when all permissions assigned to a channelgroup are announced by onChannelGroupPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the channelgroup
        @type channelGroupID: int
        """
    def onChannelPermListEvent(self, serverConnectionHandlerID, channelID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a channel requested by requestChannelPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
    def onChannelPermListFinishedEvent(self, serverConnectionHandlerID, channelID):
        """
        Triggers when all permissions assigned to a channel are announced by onChannelPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        """
    def onClientPermListEvent(self, serverConnectionHandlerID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a user requested by requestClientPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
    def onClientPermListFinishedEvent(self, serverConnectionHandlerID, clientDatabaseID):
        """
        Triggers when all permissions assigned to a user are announced by onClientPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        """
    def onChannelClientPermListEvent(self, serverConnectionHandlerID, channelID, clientDatabaseID, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission assigned to a user in a channel requested by requestChannelClientPermList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
    def onChannelClientPermListFinishedEvent(self, serverConnectionHandlerID, channelID, clientDatabaseID):
        """
        Triggers when all permissions assigned to a user in a channel are announced by onChannelClientPermListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param clientDatabaseID: the database ID of the client
        @type clientDatabaseID: int
        """
    def onClientChannelGroupChangedEvent(self, serverConnectionHandlerID, channelGroupID, channelID, clientID, invokerClientID, invokerName, invokerUniqueIdentity):
        """
        Triggers when the channelgroup of a client is modified.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param channelGroupID: the ID of the new channelgroup
        @type channelGroupID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param invokerClientID: the ID of the client, who assigned the new channelgroup to the client
        @type invokerClientID: int
        @param invokerName: the current nickname of the invoking client
        @type invokerName: string
        @param invokerUniqueIdentity: the UID of the invoking client
        @type invokerUniqueIdentity: string
        """
    def onServerPermissionErrorEvent(self, serverConnectionHandlerID, errorMessage, error, returnCode, failedPermissionID):
        """
        Triggers when a requested action fails because of insufficient permission rights.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param errorMessage: the message of the error
        @type errorMessage: string
        @param error: the errorcode
        @type error: int
        @@param returnCode: the returnCode created with createReturnCode and passed to the function which raised the error, empty string if no returnCode was passed
        @type returnCode: string
        @param failedPermissionID: the ID of the insuficient permission
        @type failedPermissionID: int
        @return: returns 1 (or True) to let the client ignore the error
        @rtype: int or bool
        """
    def onPermissionListGroupEndIDEvent(self, serverConnectionHandlerID, groupEndID):
        """
        //TODO: wäh?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param groupEndID: 
        @type groupEndID: int
        """
    def onPermissionListEvent(self, serverConnectionHandlerID, permissionID, permissionName, permissionDescription):
        """
        Triggers for each permission on the server requested by requestPermissionList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionName: the name of the permission
        @type permissionName: string
        @param permissionDescription: the description of the permission
        @type permissionDescription: string
        """
    def onPermissionListFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all permissions of the server are announced by onPermissionListEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onPermissionOverviewEvent(self, serverConnectionHandlerID, clientDatabaseID, channelID, overviewType, overviewID1, overviewID2, permissionID, permissionValue, permissionNegated, permissionSkip):
        """
        Triggers for each permission requested by requestPermissionOverview.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param channelID: the ID of the channel
        @type channelID: int
        @param overviewType: //TODO: wäh?
        @type overviewType: 
        @param overviewID1: 
        @type overviewID1: 
        @param overviewID2: 
        @type overviewID2: 
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        @param permissionNegated: the negated value of the permission
        @type permissionNegated: int
        @param permissionSkip: the skip value of the permission
        @type permissionSkip: int
        """
    def onPermissionOverviewFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all permissions of the overview are announced by onPermissionOverviewEvent.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onServerGroupClientAddedEvent(self, serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        """
        Triggers when a client is added to a servergroup.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param clientName: the current nickname of the client
        @type clientName: string
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: string
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param invokerClientID: the ID of the invoking client
        @type invokerClientID: int
        @param invokerName: the current nickname of the invoking client
        @type invokerName: string
        @param invokerUniqueIdentity: the UID of the invoking client
        @type invokerUniqueIdentity: string
        """
    def onServerGroupClientDeletedEvent(self, serverConnectionHandlerID, clientID, clientName, clientUniqueIdentity, serverGroupID, invokerClientID, invokerName, invokerUniqueIdentity):
        """
        Triggers when a client is deleted from a servergroup.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client.
        @type clientID: int
        @param clientName: the current nickname of the client
        @type clientName: string
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: string
        @param serverGroupID: the ID of the servergroup
        @type serverGroupID: int
        @param invokerClientID: the ID of the invoking client
        @type invokerClientID: int
        @param invokerName: the current nickname of the invoking client
        @type invokerName: string
        @param invokerUniqueIdentity: the UID of the invoking client
        @type invokerUniqueIdentity: string
        """
    def onClientNeededPermissionsEvent(self, serverConnectionHandlerID, permissionID, permissionValue):
        """
        //TODO: wäh? fehlt da ein Request?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param permissionID: the ID of the permission
        @type permissionID: int
        @param permissionValue: the value of the permission
        @type permissionValue: int
        """
    def onClientNeededPermissionsFinishedEvent(self, serverConnectionHandlerID):
        """
        Triggers when all permissions are announced by onClientNeededPermissionsEvent. //TODO: siehe oben
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onFileTransferStatusEvent(self, transferID, status, statusMessage, remotefileSize, serverConnectionHandlerID):
        """
        //TODO: wäh?
        @param transferID: the ID of the filetransfer
        @type transferID: int
        @param status: 
        @type status: int
        @param statusMessage: 
        @type statusMessage: string
        @param remotefileSize: 
        @type remotefileSize: int
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        """
    def onClientChatClosedEvent(self, serverConnectionHandlerID, clientID, clientUniqueIdentity):
        """
        Triggers when a client closes the private chat (by closeing the private chat tab).
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: int
        """
    def onClientChatComposingEvent(self, serverConnectionHandlerID, clientID, clientUniqueIdentity):
        """
        Triggers when a client is currently composing a private text message.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param clientUniqueIdentity: the UID of the client
        @type clientUniqueIdentity: string
        """
    def onServerLogEvent(self, serverConnectionHandlerID, logMsg):
        """
        //TODO: requestServerLog
        @param serverConnectionHandlerID: 
        @type serverConnectionHandlerID: 
        @param logMsg: 
        @type logMsg: 
        """
    def onServerLogFinishedEvent(self, serverConnectionHandlerID, lastPos, fileSize):
        """
        Triggers when all requested entries from the serverlog are announced by onServerLogEvent
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param lastPos: position of the last entry announced by onServerLogEvent //TODO: right?
        @type lastPos: int
        @param fileSize: total amount of entries in serverlog //TODO: right?
        @type fileSize: int
        """
    def onMessageListEvent(self, serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, timestamp, flagRead):
        """
        Triggers for each offline message requested by requestMessageList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param messageID: the ID of the message
        @type messageID: int
        @param fromClientUniqueIdentity: the UID of the author
        @type fromClientUniqueIdentity: string
        @param subject: the subject of the message
        @type subject: string
        @param timestamp: the time of creation //TODO: in UTC?
        @type timestamp: int
        @param flagRead: if set to 1 (or True) the message was already read, 0 (or False) otherwise
        @type flagRead: int or bool
        """
    def onMessageGetEvent(self, serverConnectionHandlerID, messageID, fromClientUniqueIdentity, subject, message, timestamp):
        """
        Triggers with the information of an offline message requested by requestMessageGet.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param messageID: the ID of the message
        @type messageID: int
        @param fromClientUniqueIdentity: the UID of the author
        @type fromClientUniqueIdentity: string
        @param subject: the subject of the message
        @type subject: string
        @param message: the message
        @type message: string
        @param timestamp: the time of creation //TODO: in UTC?
        @type timestamp: int
        """
    def onClientDBIDfromUIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID):
        """
        Triggers with the information requested by requestClientDBIDfromUID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the client
        @type uniqueClientIdentifier: string
        @param clientDatabaseID: the database ID of the client
        @type clientDatabaseID: int
        """
    def onClientNamefromUIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        """
        Triggers with the information requested by requestClientNamefromUID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the user
        @type uniqueClientIdentifier: string
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param clientNickName: the last nickname of the user //TODO: right?
        @type clientNickName: string
        """
    def onClientNamefromDBIDEvent(self, serverConnectionHandlerID, uniqueClientIdentifier, clientDatabaseID, clientNickName):
        """
        Triggers with the information requested by requestClientNamefromDBID.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param uniqueClientIdentifier: the UID of the user
        @type uniqueClientIdentifier: string
        @param clientDatabaseID: the database ID of the user
        @type clientDatabaseID: int
        @param clientNickName: the last nickname of the user //TODO: right?
        @type clientNickName: string
        """
    def onComplainListEvent(self, serverConnectionHandlerID, targetClientDatabaseID, targetClientNickName, fromClientDatabaseID, fromClientNickName, complainReason, timestamp):
        """
        Triggers for each complain of a user requested by requestComplainList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param targetClientDatabaseID: the database ID of the user
        @type targetClientDatabaseID: int
        @param targetClientNickName: the last nickname of the user
        @type targetClientNickName: string
        @param fromClientDatabaseID: the database ID of the complaining user
        @type fromClientDatabaseID: int
        @param fromClientNickName: the last nickname of the complaining user
        @type fromClientNickName: string
        @param complainReason: the reason for the complain
        @type complainReason: string
        @param timestamp: time of creation //TODO: int UTC?
        @type timestamp: int
        """
    def onBanListEvent(self, serverConnectionHandlerID, banid, ip, name, uid, creationTime, durationTime, invokerName, invokercldbid, invokeruid, reason, numberOfEnforcements, lastNickName):
        """
        Triggers for each ban of the server requested by requestBanList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param banid: the ID of the ban
        @type banid: int
        @param ip: regular expression to match IPs, empty string if ignored
        @type ip: string
        @param name: regular expression to match nicknames, empty string if ignored
        @type name: string
        @param uid: the UID which was banned, empty string if ignored
        @type uid: string
        @param creationTime: time of creation //TODO: in UTC?
        @type creationTime: int
        @param durationTime: duration of the ban, 0 is unlimited //TODO: in seconds?
        @type durationTime: int
        @param invokerName: the nickname of the invoking user at time of creation
        @type invokerName: string
        @param invokercldbid: the database ID of the invoking user
        @type invokercldbid: int
        @param invokeruid: the UID of the invoking user
        @type invokeruid: string
        @param reason: the reason for the ban
        @type reason: string
        @param numberOfEnforcements: number clients, whose connection was prevented of the ban
        @type numberOfEnforcements: int
        @param lastNickName: last nickname of the banned user (only if a UID was banned)
        @type lastNickName: string
        """
    def onClientServerQueryLoginPasswordEvent(self, serverConnectionHandlerID, loginPassword):
        """
        //TODO: entweder fehlt hier der loginname oder der command requestClientServerQueryLoginPassword(schid, loginName) fehlt
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param loginPassword: the password to the serverquery login
        @type loginPassword: string
        """
    def onPluginCommandEvent(self, serverConnectionHandlerID, pluginName, pluginCommand):
        """
        Triggers for each plugin command send by another client.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param pluginName: the name identifier to the remote plugin
        @type pluginName: string
        @param pluginCommand: the command
        @type pluginCommand: string
        """
    def onIncomingClientQueryEvent(self, serverConnectionHandlerID, commandText):
        """
        //TODO: wäh?
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param commandText: 
        @type commandText: 
        """
    def onServerTemporaryPasswordListEvent(self, serverConnectionHandlerID, clientNickname, uniqueClientIdentifier, description, password, timestampStart, timestampEnd, targetChannelID, targetChannelPW):
        """
        Triggers for each temporary password on the server requested by requestServerTemporaryPasswordList.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientNickname: the nickname of the creator
        @type clientNickname: string
        @param uniqueClientIdentifier: the UID of the creator
        @type uniqueClientIdentifier: string
        @param description: the description of the temporary password
        @type description: string
        @param password: the password
        @type password: string
        @param timestampStart: time of start //TODO: in UTC?
        @type timestampStart: int
        @param timestampEnd: time of end //TODO: in UTC?
        @type timestampEnd: int
        @param targetChannelID: the ID of the default channel for clients connecting with the password
        @type targetChannelID: int
        @param targetChannelPW: the password of the target channel
        @type targetChannelPW: string
        """
    def onAvatarUpdated(self, serverConnectionHandlerID, clientID, avatarPath):
        """
        Triggers when a client in view modified his avatar.
        @param serverConnectionHandlerID: the ID of the serverconnection
        @type serverConnectionHandlerID: int
        @param clientID: the ID of the client
        @type clientID: int
        @param avatarPath: the path to the avatar on the system
        @type avatarPath: string
        """
