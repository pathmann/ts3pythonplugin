#ifndef AUTO_PLUGIN_H__
#define AUTO_PLUGIN_H__

#include <Python.h>

#include "public_definitions.h"
#include "plugin_definitions.h"

#ifdef WIN32
#define PLUGINS_EXPORTDLL __declspec(dllexport)
#else
#define PLUGINS_EXPORTDLL __attribute__ ((visibility("default")))
#endif

#ifdef __cplusplus
extern "C" {
#endif

$$FUNCTIONS$$

#ifdef __cplusplus
}
#endif

#endif
