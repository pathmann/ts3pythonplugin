#ifndef TS3MODULE_H__
#define TS3MODULE_H__

#include <Python.h>

#include "ts3_functions.h"

$$FUNCTIONS$$

PyMODINIT_FUNC PyInit_ts3(void);

#endif
