#include "ts3module.h"

#include "public_errors.h"

#include "global_shared.h"
#include "python_helpers.h"

$$FUNC_ARRAY$$

static PyModuleDef mdef = {
  PyModuleDef_HEAD_INIT,
  "ts3",
  NULL,
  -1,
  ts3modfuncs,
  NULL,
  NULL,
  NULL,
  NULL
};

PyMODINIT_FUNC PyInit_ts3(void) {
 return PyModule_Create(&mdef);
}

/*
class ts3:
    """
    Class with all available ts3 client lib functions.
    """

*/

$$FUNCTIONS$$

