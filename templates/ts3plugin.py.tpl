class PluginMetaClass(type):
    def __init__(cls, name, bases, nmspc):
        super(PluginMetaClass, cls).__init__(name, bases, nmspc)
        if not hasattr(cls, 'registry'):
            cls.registry = set()
        cls.registry.add(cls)
        cls.registry -= set(bases) # Remove base classes
        # Metamethods, called on class objects:
    def __iter__(cls):
        return iter(cls.registry)
    def __str__(cls):
        if cls in cls.registry:
            return cls.__name__
        return cls.__name__ + ": " + ", ".join([sc.__name__ for sc in cls])

class ts3Plugin(object, metaclass = PluginMetaClass):
    #name = "pluginname"
    #version = "1.0"
    #description = "This python module does fancy things"
    #author = "Thomas Pathmann"
    #apiVersion = 20
    offersConfigure = False

    def __init__(self):
        pass
    def __del__(self):
        pass
    def configure(self):
        pass
    def infoData(self, serverConnectionHandlerID, id, type):
        pass

$$PLUGIN_FUNCTIONS$$


class PluginHost:
    running = []
    @staticmethod
    def start(apiver, names, forced):
        lowapi = []

        for p in ts3Plugin:
            if p is not ts3Plugin:
                if getattr(p, 'name', '') == "":
                    print("Every plugin needs a static string attribute name")
                    continue
                elif p.name in names:
                    if getattr(p, 'apiVersion', '0') != apiver and not p.name in forced:
                        lowapi.append(p.name)
                    else:
                        PluginHost.running.append(p())

        return lowapi

    @staticmethod
    def stop():
        PluginHost.running = []

    @staticmethod
    def allPlugins():
        ret = []
        for p in ts3Plugin:
            if p is not ts3Plugin and getattr(p, 'name', '') != '':
                ret.append(p.name)

        return ret

    @staticmethod
    def startPlugin(name):
        for p in ts3Plugin:
            if p is not ts3Plugin and getattr(p, 'name', '') == name:
                PluginHost.running.append(p())
                return

    @staticmethod
    def stopPlugin(name):
        for p in PluginHost.running:
            if p.name  == name:
                PluginHost.running.remove(p)
                return

    @staticmethod
    def isRunning(name):
        for p in PluginHost.running:
            if p.name == name:
                return True

        return False

    @staticmethod
    def instance(name):
        for p in PluginHost.running:
            if p.name == name:
                return p

        return None

    @staticmethod
    def pluginInfo(name):
        for p in ts3Plugin:
            if p is not ts3Plugin and getattr(p, 'name', '') == name:
                version = getattr(p, 'version', 'Unknown')
                description = getattr(p, 'description', 'Unknown')
                author = getattr(p, 'description', 'Unknown')
                apiVersion = getattr(p, 'apiVersion', 0)
                hasconfigure = getattr(p, 'offersConfigure', False)

                return (name, version, description, author, apiVersion, hasconfigure)

        return None

    @staticmethod
    def configurePlugin(name):
        for p in PluginHost.running:
            if p.name == name:
                p.configure()

    @staticmethod
    def infoData(serverConnectionHandlerID, id, type):
        ret = []

        for p in PluginHost.running:
            tmp = p.infoData(serverConnectionHandlerID, id, type)
            if tmp and tmp.strip() != "":
                ret.append(tmp)

        return ret


$$HOST_FUNCTIONS$$
