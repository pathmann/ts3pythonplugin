import ts3plugin

import socket, platform, os
from threading import Thread

UNIX_SOCK_FILE = "/tmp/ts3_python_ipcplugin"
WIN_SOCK_PORT = 13370

class ipcplugin(ts3plugin.ts3Plugin):
    name = "ipcplugin"
    apiVersion = 20

    def __init__(self):
        self.clients = []
        self.running = True

        if platform.system() == "Linux" or platform.system() == "Mac":
            if os.path.exists(UNIX_SOCK_FILE):
                os.remove(UNIX_SOCK_FILE)

            self.server = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
            self.server.bind(UNIX_SOCK_FILE)
        elif platform.system() == "Windows":
            self.server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.server.bind(("localhost", WIN_SOCK_PORT))
        else:
            raise Exception("Undefined platform %s" % platform.system())

        self.server.listen(5)

        self.t = Thread(target=serverLoop, args=(self))
        self.t.start()

    def __del__(self):
        for cli in self.clients:
            self.clients.remove(cli)
            cli.close()

        self.server.close()

    def serverLoop(self):
        while self.running:
            cli, addr = self.server.accept()

            clithread = Thread(target=clientLoop, args=(self, cli))
            clithread.start()

    def clientLoop(self, cli):
        self.clients.append(cli)

        while cli in self.clients:
            data = cli.recv(1024)
            if not data:
                break
            else:
                pass

    def broadcast(self, data):
        pass
