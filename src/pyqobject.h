#ifndef PYQOBJECT_H
#define PYQOBJECT_H

#include <Python.h>

#include <QObject>

typedef struct {
  PyObject_HEAD
  bool ownership;
  QObject* data;
} pyqobject;

PyObject* pyqobject_new(PyTypeObject* type, PyObject* args, PyObject* kwds);
int pyqobject_init(pyqobject* self, PyObject* args, PyObject* kwds);
void pyqobject_dealloc(pyqobject* self);
PyObject* pyqobject_getProperty(pyqobject* self, PyObject* args);
PyObject* pyqobject_setProperty(pyqobject* self, PyObject* args);
PyObject* pyqobject_getChildren(pyqobject* self, PyObject* args);
PyObject* pyqobject_connect(pyqobject* self, PyObject* args);

PyMODINIT_FUNC PyInit_pyqobject(void);

#endif // PYQOBJECT_H
