#include "plugin.h"

#include "global_shared.h"

#include "ts3logdispatcher.h"
#include "pluginconfig.h"
#include "configdialog.h"

#include "ts3module.h"
#include "ts3helpersmodule.h"
#include "pyqobject.h"

#include "pyredirector.h"
#include "pystddispatcher.h"
#include "pyshell.h"
#include "shelldialog.h"

static QString commandmodule;
static QString curcommand;

static shelldialog* shdlg = NULL;

void showShell() {
  if (shdlg)
    shdlg->setFocus();
  else {
    shdlg = new shelldialog;
    shdlg->setAttribute(Qt::WA_DeleteOnClose, true);
    shdlg->connect(shdlg, &shelldialog::finished, [=]() {
      shdlg = NULL;
    } );
    shdlg->show();
  }
}

const char* ts3plugin_name() {
  return "Python Plugin";
}

const char* ts3plugin_version() {
  return "0.1a";
}

int ts3plugin_apiVersion() {
  return API_VER;
}

const char* ts3plugin_author() {
  return "Thomas Pathmann";
}

const char* ts3plugin_description() {
  return "Enhance the TS3 plugin system with fancy python scripts";
}

void ts3plugin_setFunctionPointers(const struct TS3Functions functions) {
  funcs = functions;
}

int ts3plugin_init() {
  QString error;

  ts3logdispatcher::instance()->init(funcs, "PythonPlugin");

  if (PyImport_AppendInittab("ts3", &PyInit_ts3) == -1)
    return 1;

  if (PyImport_AppendInittab("ts3helpers", &PyInit_ts3helpers) == -1)
    return 1;

  if (PyImport_AppendInittab("redirector", &PyInit_redirector) == -1)
    return 1;

  if (PyImport_AppendInittab("pyqobject", &PyInit_pyqobject) == -1) {
    ts3logdispatcher::instance()->add("error pyqobject", true, true, LogLevel_WARNING);
    return 1;
  }

  Py_Initialize();

  PluginConfig::instance()->load();

  if (!PythonHost::instance()->setup(error)) {
    ts3logdispatcher::instance()->add(error, true, true, LogLevel_ERROR);
    return 1;
  }
  else if (!error.isEmpty()) {
    ts3logdispatcher::instance()->add(error, false, true);
    error.clear();
  }

  if (!PythonHost::instance()->start(error)) {
    ts3logdispatcher::instance()->add(error, true, true, LogLevel_ERROR);
    return 1;
  }
  else if (!error.isEmpty()) {
    ts3logdispatcher::instance()->add(error, false, true);
    error.clear();
  }

  if (!PythonHost::instance()->addUserscriptModule(commandmodule, error)) {
    ts3logdispatcher::instance()->add(error, true, true, LogLevel_ERROR);
    return 1;
  }

  PythonHost::instance()->setupUserscriptModule(commandmodule, error);

  user_scripts.insert(commandmodule, QStringList());
  pystddispatcher::instance()->connect(pystddispatcher::instance(), &pystddispatcher::onMessage, [=](const QString& context, const QString &str) {
      if (context == commandmodule) {
        foreach (QString out, str.split('\n'))
          if (!out.trimmed().isEmpty())
            funcs.printMessageToCurrentTab(out.toUtf8().data());
      }
  } );

  return 0;
}

void ts3plugin_shutdown() {
  if (shdlg)
    shdlg->close();

  QString error;
  if (!PythonHost::instance()->stop(error))
    ts3logdispatcher::instance()->add(error, false, true);
  else {
    if (!error.isEmpty()) {
      ts3logdispatcher::instance()->add(error, false, true);
    }
  }

  PluginConfig::instance()->save();

  PythonHost::instance()->shutdown();

  if (Py_IsInitialized() != 0)
    Py_Finalize();

  if (pluginid) {
    free(pluginid);
    pluginid = NULL;
  }
}

void ts3plugin_initMenus(struct PluginMenuItem*** menuItems, char** menuIcon) {
  *menuItems = (struct PluginMenuItem**)malloc(sizeof(struct PluginMenuItem*) * 2);

  struct PluginMenuItem* item = (struct PluginMenuItem*)malloc(sizeof(struct PluginMenuItem));
  item->type = PLUGIN_MENU_TYPE_GLOBAL;
  item->id = 1;
  qstrncpy(item->text, "PyShell", PLUGIN_MENU_BUFSZ);
  qstrncpy(item->icon, "", PLUGIN_MENU_BUFSZ);

  (*menuItems)[0] = item;
  (*menuItems)[1] = NULL;

  *menuIcon = (char*)malloc(PLUGIN_MENU_BUFSZ * sizeof(char));
  qstrncpy(*menuIcon, "", PLUGIN_MENU_BUFSZ);
}

void ts3plugin_freeMemory(void* data) {
  free(data);
}

void ts3plugin_onMenuItemEvent(uint64 /* serverConnectionHandlerID*/, enum PluginMenuType type, int menuItemID, uint64 /* selectedItemID */) {
  if (type != PLUGIN_MENU_TYPE_GLOBAL || menuItemID != 1)
    return;

  showShell();
}

int ts3plugin_offersConfigure() {
  return PLUGIN_OFFERS_CONFIGURE_QT_THREAD;
}

void ts3plugin_configure(void* handle, void* qParentWidget) {
  Q_UNUSED(handle)

  ConfigDialog* dlg = new ConfigDialog((QWidget*)qParentWidget);
  dlg->setAttribute(Qt::WA_DeleteOnClose, true);
  dlg->show();
}

void ts3plugin_registerPluginID(const char* id) {
  pluginid = (char*)malloc((strlen(id) +1) * sizeof(char));
  strncpy(pluginid, id, (strlen(id) +1) * sizeof(char));
}

const char* ts3plugin_commandKeyword() {
  return "py";
}

int ts3plugin_processCommand(uint64 /* schid */, const char* command) {
  QString error;
  QString cmd = command;

  /* as long as the client strips away spaces and tabs, we need to add the run command */

  if (cmd.startsWith("run"))
    cmd = cmd.mid(4);
  else if (cmd.startsWith("shell")) {
    showShell();

    return 0;
  }
  else return 1;

  if (pyshell::isMultiLine(cmd)) {
    curcommand.append("\n");
    curcommand.append(cmd);
    return 0;
  }

  if (!curcommand.isEmpty())
    curcommand.append("\n");

  curcommand.append(cmd);

  user_scripts[commandmodule] << curcommand;

  if (!PythonHost::instance()->execUserString(curcommand, commandmodule, error)) {
    funcs.printMessageToCurrentTab(error.toUtf8().data());
    user_scripts[commandmodule].pop_back();
  }
  curcommand.clear();

  return 0;
}

const char* ts3plugin_infoTitle() {
  if (PluginConfig::instance()->showInfoTitle())
    return "Python Plugin";
  else return "";
}

void ts3plugin_infoData(uint64 schid, uint64 id, enum PluginItemType type, char** data) {
  QString error;
  QStringList retlist;

  if (!PythonHost::instance()->getInfoData(schid, id, (int)type, retlist, error)) {
    ts3logdispatcher::instance()->add(QObject::tr("Error getting info data: %1").arg(error), true, true);
    return;
  }

  QString oneline = retlist.join("\n");
  *data = (char*)malloc((oneline.count() +1) * sizeof(char));
  qstrcpy(*data, oneline.toUtf8().data());
}
