#ifndef PYSIGNALSLOTCONNECTION_H
#define PYSIGNALSLOTCONNECTION_H

#include <Python.h>

#include <QObject>

class pysignalslotconnection: public QObject {
    Q_OBJECT
  public:
    pysignalslotconnection(QObject* sender, const char* signal, PyObject* slot, QObject* parent = 0);
    ~pysignalslotconnection();

    bool connect();
  public slots:
    void onTriggered();
  private:
    QObject* m_sender;
    QString m_signal;
    PyObject* m_callable;

};

#endif // PYSIGNALSLOTCONNECTION_H
