#ifndef PLUGINCONFIG_H
#define PLUGINCONFIG_H

#include "singleton.h"
#include "global_shared.h"

#include <QString>
#include <QStringList>
#include <QSharedPointer>

class PluginConfig;

class Observable: public QObject {
  Q_OBJECT

    friend class PluginConfig;
  public:
    Observable(QObject* parent = 0): QObject(parent) { }
    ~Observable() { }
  protected:
    void setChanged() { emit changed(); }
  signals:
    void changed();
};

class PluginConfig: public singleton<PluginConfig> {
    friend class singleton<PluginConfig>;
  public:
    Observable* observable() const { return m_observ.data(); }

    void load();
    void save() const;

    /* plugin related settings */
    const QStringList& activatedPlugins() const;
    void setActivatedPlugins(const QStringList& list);
    void activatePlugin(const QString& name, bool set);
    bool isActivated(const QString& name) const;

    const QStringList& forcedPlugins() const;
    void setForcedPlugins(const QStringList& list);
    void forcePlugin(const QString& name, bool set);
    bool isForced(const QString& name) const;

    bool showInfoTitle() const;
    void setShowInfoTitle(bool set);

    /* shell related settings */
    bool tabCompletion() const;
    void setTabCompletion(bool set);
    bool insertSpaces() const;
    void setInsertSpaces(bool set);
    int tabWidth() const;
    void setTabWidth(int val);

    QString fontFamily() const;
    void setFontFamily(const QString& val);
    int fontSize() const;
    void setFontSize(int val);
  protected:
    void setDefaults();
  private:
    PluginConfig();
    PluginConfig(const PluginConfig& copy);
    ~PluginConfig();

    QSharedPointer<Observable> m_observ;

    QStringList m_activeplugins;
    QStringList m_forcedplugins;

    bool m_showinfotitle;

    bool m_tabcompletion;
    bool m_insertspaces;
    int m_tabwidth;

    QString m_fontfamily;
    int m_fontsize;
};

#endif // PLUGINCONFIG_H
