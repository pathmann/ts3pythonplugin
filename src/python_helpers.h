#ifndef PYTHON_HELPERS_H
#define PYTHON_HELPERS_H

#include <Python.h>

#include <QStringList>
#include <QVariant>

#include "public_definitions.h"
#include "plugin_definitions.h"

PyObject* qstringlistToPyList(const QStringList& inlist, QString& error);
bool pyListToQstringlist(PyObject* pylist, QStringList* ret, QString& error);
void listDecref(const QList<PyObject*>& list);
PyObject* tupleFromPyObjectQlist(const QList<PyObject*>& list);
PyObject* floatListToPyList(float* inp);
PyObject* unsignedIntListToPyList(unsigned int* inp);
PyObject* shortListToPyList(short* inp);
PyObject* intListToPyList(int* inp);
bool PyTupleToTS3Vector(PyObject* tuple, TS3_VECTOR* ret);
bool PyListToCharArray(PyObject* list, char*** ret);
bool PyListToUint64Array(PyObject* list, uint64** ret);
bool PyListToAnyIDArray(PyObject* list, anyID** ret);
bool deviceListToPyList(char*** list, PyObject** ret);
bool charArrayToPyList(char** list, PyObject** ret);
bool anyIDArrayToPyList(anyID* list, PyObject** ret);
bool uint64ArrayToPyList(uint64* list, PyObject** ret);
bool PyListToShortArray(PyObject* list, short** ret);
bool shortArrayToPyList(short* list, PyObject** ret, int size);
bool PyListToIntArray(PyObject* list, int** ret, int* size);
bool PyListToUnsignedIntArray(PyObject* list, unsigned int** ret, int* size);
bool PyListToUint64Array(PyObject *list, uint64 **ret, int* size);
bool PyListToCharArray(PyObject* list, char*** ret, int* size);
bool bookMarksToPyList(struct PluginBookmarkList* bm, PyObject** ret);
QString pyObjectToOutputString(PyObject* o, bool incontainer = false);
PyObject* qvariantToPyObject(const QVariant& var);
QVariant pyObjectToQVariant(PyObject* o);

#endif // PYTHON_HELPERS_H
