/********************************************************************************
** Form generated from reading UI file 'configdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIGDIALOG_H
#define UI_CONFIGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ConfigDialog
{
public:
    QVBoxLayout *verticalLayout_4;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QListWidget *pluginlist;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *reloadbutton;
    QPushButton *refreshbutton;
    QPushButton *configurebutton;
    QGridLayout *gridLayout;
    QLabel *namelabel;
    QLineEdit *nameedit;
    QLabel *versionlabel;
    QLineEdit *versionedit;
    QLabel *authorlabel;
    QLineEdit *authoredit;
    QLabel *descriptionlabel;
    QTextBrowser *descriptionbrowser;
    QLabel *apiverlabel;
    QLineEdit *apiveredit;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_5;
    QCheckBox *infotitlecheckbox;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *tabcompletioncheckbox;
    QCheckBox *spacescheckbox;
    QHBoxLayout *horizontalLayout_4;
    QLabel *tabwidthlabel;
    QSpinBox *tabwidthspin;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_2;
    QFontComboBox *fontfamilycombobox;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_3;
    QSpinBox *fontsizespinbox;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *closebutton;

    void setupUi(QDialog *ConfigDialog)
    {
        if (ConfigDialog->objectName().isEmpty())
            ConfigDialog->setObjectName(QStringLiteral("ConfigDialog"));
        ConfigDialog->resize(481, 350);
        verticalLayout_4 = new QVBoxLayout(ConfigDialog);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        tabWidget = new QTabWidget(ConfigDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        horizontalLayout_2 = new QHBoxLayout(tab);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        pluginlist = new QListWidget(tab);
        pluginlist->setObjectName(QStringLiteral("pluginlist"));

        verticalLayout->addWidget(pluginlist);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        reloadbutton = new QPushButton(tab);
        reloadbutton->setObjectName(QStringLiteral("reloadbutton"));

        horizontalLayout->addWidget(reloadbutton);

        refreshbutton = new QPushButton(tab);
        refreshbutton->setObjectName(QStringLiteral("refreshbutton"));

        horizontalLayout->addWidget(refreshbutton);

        configurebutton = new QPushButton(tab);
        configurebutton->setObjectName(QStringLiteral("configurebutton"));

        horizontalLayout->addWidget(configurebutton);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        namelabel = new QLabel(tab);
        namelabel->setObjectName(QStringLiteral("namelabel"));

        gridLayout->addWidget(namelabel, 0, 0, 1, 2);

        nameedit = new QLineEdit(tab);
        nameedit->setObjectName(QStringLiteral("nameedit"));
        nameedit->setReadOnly(true);

        gridLayout->addWidget(nameedit, 0, 2, 1, 1);

        versionlabel = new QLabel(tab);
        versionlabel->setObjectName(QStringLiteral("versionlabel"));

        gridLayout->addWidget(versionlabel, 1, 0, 1, 2);

        versionedit = new QLineEdit(tab);
        versionedit->setObjectName(QStringLiteral("versionedit"));
        versionedit->setReadOnly(true);

        gridLayout->addWidget(versionedit, 1, 2, 1, 1);

        authorlabel = new QLabel(tab);
        authorlabel->setObjectName(QStringLiteral("authorlabel"));

        gridLayout->addWidget(authorlabel, 2, 0, 1, 2);

        authoredit = new QLineEdit(tab);
        authoredit->setObjectName(QStringLiteral("authoredit"));
        authoredit->setReadOnly(true);

        gridLayout->addWidget(authoredit, 2, 2, 1, 1);

        descriptionlabel = new QLabel(tab);
        descriptionlabel->setObjectName(QStringLiteral("descriptionlabel"));

        gridLayout->addWidget(descriptionlabel, 3, 0, 1, 1);

        descriptionbrowser = new QTextBrowser(tab);
        descriptionbrowser->setObjectName(QStringLiteral("descriptionbrowser"));

        gridLayout->addWidget(descriptionbrowser, 3, 1, 1, 2);

        apiverlabel = new QLabel(tab);
        apiverlabel->setObjectName(QStringLiteral("apiverlabel"));

        gridLayout->addWidget(apiverlabel, 4, 0, 1, 1);

        apiveredit = new QLineEdit(tab);
        apiveredit->setObjectName(QStringLiteral("apiveredit"));
        apiveredit->setReadOnly(true);

        gridLayout->addWidget(apiveredit, 4, 2, 1, 1);


        horizontalLayout_2->addLayout(gridLayout);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_5 = new QVBoxLayout(tab_2);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        infotitlecheckbox = new QCheckBox(tab_2);
        infotitlecheckbox->setObjectName(QStringLiteral("infotitlecheckbox"));

        verticalLayout_5->addWidget(infotitlecheckbox);

        groupBox = new QGroupBox(tab_2);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        tabcompletioncheckbox = new QCheckBox(groupBox);
        tabcompletioncheckbox->setObjectName(QStringLiteral("tabcompletioncheckbox"));

        verticalLayout_3->addWidget(tabcompletioncheckbox);

        spacescheckbox = new QCheckBox(groupBox);
        spacescheckbox->setObjectName(QStringLiteral("spacescheckbox"));

        verticalLayout_3->addWidget(spacescheckbox);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        tabwidthlabel = new QLabel(groupBox);
        tabwidthlabel->setObjectName(QStringLiteral("tabwidthlabel"));

        horizontalLayout_4->addWidget(tabwidthlabel);

        tabwidthspin = new QSpinBox(groupBox);
        tabwidthspin->setObjectName(QStringLiteral("tabwidthspin"));
        tabwidthspin->setValue(4);

        horizontalLayout_4->addWidget(tabwidthspin);


        verticalLayout_3->addLayout(horizontalLayout_4);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_2->addWidget(label);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_5->addWidget(label_2);

        fontfamilycombobox = new QFontComboBox(groupBox);
        fontfamilycombobox->setObjectName(QStringLiteral("fontfamilycombobox"));

        horizontalLayout_5->addWidget(fontfamilycombobox);


        horizontalLayout_7->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_6->addWidget(label_3);

        fontsizespinbox = new QSpinBox(groupBox);
        fontsizespinbox->setObjectName(QStringLiteral("fontsizespinbox"));
        fontsizespinbox->setMaximum(99);
        fontsizespinbox->setSingleStep(2);
        fontsizespinbox->setValue(12);

        horizontalLayout_6->addWidget(fontsizespinbox);


        horizontalLayout_7->addLayout(horizontalLayout_6);


        verticalLayout_2->addLayout(horizontalLayout_7);


        verticalLayout_3->addLayout(verticalLayout_2);


        verticalLayout_5->addWidget(groupBox);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_4->addWidget(tabWidget);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        closebutton = new QPushButton(ConfigDialog);
        closebutton->setObjectName(QStringLiteral("closebutton"));

        horizontalLayout_3->addWidget(closebutton);


        verticalLayout_4->addLayout(horizontalLayout_3);


        retranslateUi(ConfigDialog);
        QObject::connect(closebutton, SIGNAL(pressed()), ConfigDialog, SLOT(accept()));
        QObject::connect(tabcompletioncheckbox, SIGNAL(clicked(bool)), spacescheckbox, SLOT(setDisabled(bool)));
        QObject::connect(tabcompletioncheckbox, SIGNAL(clicked(bool)), tabwidthlabel, SLOT(setDisabled(bool)));
        QObject::connect(tabcompletioncheckbox, SIGNAL(clicked(bool)), tabwidthspin, SLOT(setDisabled(bool)));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ConfigDialog);
    } // setupUi

    void retranslateUi(QDialog *ConfigDialog)
    {
        ConfigDialog->setWindowTitle(QApplication::translate("ConfigDialog", "PythonPlugin", 0));
#ifndef QT_NO_TOOLTIP
        reloadbutton->setToolTip(QApplication::translate("ConfigDialog", "Reloads all modules. Activated plugins need to be restarted to gain changes.", 0));
#endif // QT_NO_TOOLTIP
        reloadbutton->setText(QApplication::translate("ConfigDialog", "Reload All", 0));
#ifndef QT_NO_TOOLTIP
        refreshbutton->setToolTip(QApplication::translate("ConfigDialog", "Refresh directory lookup.", 0));
#endif // QT_NO_TOOLTIP
        refreshbutton->setText(QApplication::translate("ConfigDialog", "Refresh", 0));
        configurebutton->setText(QApplication::translate("ConfigDialog", "Configure", 0));
        namelabel->setText(QApplication::translate("ConfigDialog", "Name:", 0));
        versionlabel->setText(QApplication::translate("ConfigDialog", "Version:", 0));
        authorlabel->setText(QApplication::translate("ConfigDialog", "Author:", 0));
        descriptionlabel->setText(QApplication::translate("ConfigDialog", "Description:", 0));
        apiverlabel->setText(QApplication::translate("ConfigDialog", "API Version:", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ConfigDialog", "Plugins", 0));
        infotitlecheckbox->setText(QApplication::translate("ConfigDialog", "Show info title", 0));
        groupBox->setTitle(QApplication::translate("ConfigDialog", "Shell", 0));
        tabcompletioncheckbox->setText(QApplication::translate("ConfigDialog", "Tab completion", 0));
        spacescheckbox->setText(QApplication::translate("ConfigDialog", "Insert spaces", 0));
        tabwidthlabel->setText(QApplication::translate("ConfigDialog", "Tab width", 0));
        label->setText(QApplication::translate("ConfigDialog", "Font", 0));
        label_2->setText(QApplication::translate("ConfigDialog", "Family:", 0));
        label_3->setText(QApplication::translate("ConfigDialog", "Size:", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("ConfigDialog", "General", 0));
        closebutton->setText(QApplication::translate("ConfigDialog", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class ConfigDialog: public Ui_ConfigDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIGDIALOG_H
