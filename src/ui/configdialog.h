#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <Python.h>

#include "ui_configdialog.h"

#include <QList>

class ConfigDialog : public QDialog, private Ui::ConfigDialog {
    Q_OBJECT

  public:
    explicit ConfigDialog(QWidget *parent = 0);
  protected:
    void setupActions();
  protected slots:
    void onPluginlistItemChanged(QListWidgetItem* item);
    void onPluginlistCurrentTextChanged(const QString& text);

    void onConfigureButtonClicked();
    void onReloadButtonClicked();
    void onRefreshButtonClicked();

    void onInfoTitleCheckboxToggled(bool checked);
    void onTabCompletionCheckboxToggled(bool checked);
    void onSpacesCheckboxToggled(bool checked);
    void onTabWidthSpinboxValueChanged(int newval);
    void onFontFamilyComboBoxFontChanged(const QFont& newfont);
    void onFontSizeSpinboxValueChanged(int newval);
  private:
    QList<bool> m_checked;
};

#endif // CONFIGDIALOG_H
