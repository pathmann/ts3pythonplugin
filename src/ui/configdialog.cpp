#include "configdialog.h"

#include "pythonhost.h"
#include "pluginconfig.h"

#include <QMessageBox>

ConfigDialog::ConfigDialog(QWidget *parent): QDialog(parent) {
  setupUi(this);

  QString error;
  QStringList plugins =  PythonHost::instance()->allPlugins(error);
  plugins.sort();

  if (!error.isEmpty()) {
    printf("error getting allPlugins: %s\n", error.toUtf8().data());
    QMessageBox::warning(this, tr("Error, getting all plugins"), error);
  }

  foreach (QString name, plugins) {
    QListWidgetItem* it = new QListWidgetItem(name, pluginlist);
    it->setFlags(it->flags() | Qt::ItemIsUserCheckable);

    if (PluginConfig::instance()->isActivated(name)) {
      it->setCheckState(Qt::Checked);
      m_checked.push_back(true);
    }
    else {
      it->setCheckState(Qt::Unchecked);
      m_checked.push_back(false);
    }
  }

  setupActions();
}

void ConfigDialog::setupActions() {
  connect(pluginlist, &QListWidget::itemChanged, this, &ConfigDialog::onPluginlistItemChanged);
  connect(pluginlist, &QListWidget::currentTextChanged, this, &ConfigDialog::onPluginlistCurrentTextChanged);
  connect(configurebutton, &QPushButton::clicked, this, &ConfigDialog::onConfigureButtonClicked);

  connect(refreshbutton, &QPushButton::clicked, this, &ConfigDialog::onRefreshButtonClicked);
  connect(reloadbutton, &QPushButton::clicked, this, &ConfigDialog::onReloadButtonClicked);

  connect(infotitlecheckbox, &QCheckBox::toggled, this, &ConfigDialog::onInfoTitleCheckboxToggled);
  connect(tabcompletioncheckbox, &QCheckBox::toggled, this, &ConfigDialog::onTabCompletionCheckboxToggled);
  connect(spacescheckbox, &QCheckBox::toggled, this, &ConfigDialog::onSpacesCheckboxToggled);
  connect(tabwidthspin, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &ConfigDialog::onTabWidthSpinboxValueChanged);
  connect(fontfamilycombobox, &QFontComboBox::currentFontChanged, this, &ConfigDialog::onFontFamilyComboBoxFontChanged);
  connect(fontsizespinbox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this, &ConfigDialog::onFontSizeSpinboxValueChanged);
}

void ConfigDialog::onPluginlistItemChanged(QListWidgetItem* item) {
  if ((item->checkState() == Qt::Checked) != m_checked[pluginlist->row(item)]) {
    QString error;
    QString plugname = item->text();

    m_checked[pluginlist->row(item)] = (item->checkState() == Qt::Checked);

    if (item->checkState() == Qt::Checked) {
      //start plugin
      if (!PythonHost::instance()->isRunning(plugname, error)) {
        //check if force is needed
        if (!PluginConfig::instance()->forcedPlugins().contains(plugname)) {
          PythonPluginInfo pluginfo;
          QString infoerr;

          if (!PythonHost::instance()->pluginInfo(plugname, &pluginfo, infoerr) || !infoerr.isEmpty()) {
            QMessageBox::warning(this, tr("Error"), tr("Error getting plugin info %1").arg(infoerr));
            return;
          }

          if (pluginfo.apiVersion != API_VER) {
            if (QMessageBox::question(this, tr("Force plugin"), tr("This plugin was made for another api version. Force to start?"), QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Ok) != QMessageBox::Ok)
              return;
            else PluginConfig::instance()->forcePlugin(plugname, true);
          }
        }

        if (error.isEmpty() && PythonHost::instance()->startPlugin(plugname, error)) {
          //if previously starting fails, reset the item states
          item->setTextColor(Qt::black);
          item->setToolTip("");
          PluginConfig::instance()->activatePlugin(plugname, true);
        }
        else {
          m_checked[pluginlist->row(item)] = false;
          item->setTextColor(Qt::red);
          item->setToolTip(error);
        }
      }
    }
    else {
      //stop plugin
      if (PythonHost::instance()->isRunning(plugname, error)) {
        if (PythonHost::instance()->stopPlugin(plugname, error)) {
          //if previously starting fails, reset the item states
          item->setTextColor(Qt::black);
          item->setToolTip("");
          PluginConfig::instance()->activatePlugin(plugname, false);
        }
        else {
          m_checked[pluginlist->row(item)] = true;
          item->setTextColor(Qt::red);
          item->setToolTip(error);
        }
      }
    }
  }
}

void ConfigDialog::onPluginlistCurrentTextChanged(const QString& text) {
  if (text.isEmpty())
    return;

  QString error;
  PythonPluginInfo pluginfo;

  if (PythonHost::instance()->pluginInfo(text, &pluginfo, error)) {
    nameedit->setText(pluginfo.name);
    versionedit->setText(pluginfo.version);
    authoredit->setText(pluginfo.author);
    descriptionbrowser->setText(pluginfo.description);
    apiveredit->setText(QString::number(pluginfo.apiVersion));
    configurebutton->setEnabled(pluginfo.hasConfigure);

    descriptionbrowser->setTextColor(Qt::black);
  }
  else {
    QString unknown = tr("Unkown");

    nameedit->setText(unknown);
    versionedit->setText(unknown);
    authoredit->setText(unknown);
    apiveredit->setText(unknown);
    configurebutton->setEnabled(false);

    descriptionbrowser->setText(error);

    descriptionbrowser->setTextColor(Qt::red);
  }
}

void ConfigDialog::onConfigureButtonClicked() {
  QString error;

  if (!PythonHost::instance()->configurePlugin(pluginlist->currentItem()->text(), error))
    QMessageBox::warning(this, tr("Error"), error);
}

void ConfigDialog::onReloadButtonClicked() {
  QString error;

  for (int i = 0; i < pluginlist->count(); ++i) {
    if (pluginlist->item(i)->isSelected()) {
      if (!PythonHost::instance()->stopPlugin(pluginlist->item(i)->text(), error))
        QMessageBox::warning(this, tr("Error"), error);

      error.clear();
    }
  }
}

void ConfigDialog::onRefreshButtonClicked() {
  pluginlist->clear();
  m_checked.clear();

  QString error;
  if (!PythonHost::instance()->refresh(error)) {
    QMessageBox::warning(this, tr("Error"), error);
    return;
  }

  if (!error.isEmpty()) {
    QMessageBox::warning(this, tr("Error"), error);
    error.clear();
  }

  QStringList plugins =  PythonHost::instance()->allPlugins(error);
  plugins.sort();

  if (!error.isEmpty())
    QMessageBox::warning(this, tr("Error"), error);

  foreach (QString name, plugins) {
    QListWidgetItem* it = new QListWidgetItem(name, pluginlist);
    it->setFlags(it->flags() | Qt::ItemIsUserCheckable);

    if (PluginConfig::instance()->isActivated(name)) {
      it->setCheckState(Qt::Checked);
      m_checked.push_back(true);
    }
    else {
      it->setCheckState(Qt::Unchecked);
      m_checked.push_back(false);
    }
  }
}

void ConfigDialog::onInfoTitleCheckboxToggled(bool checked) {
  PluginConfig::instance()->setShowInfoTitle(checked);
}

void ConfigDialog::onTabCompletionCheckboxToggled(bool checked) {
  PluginConfig::instance()->setTabCompletion(checked);
}

void ConfigDialog::onSpacesCheckboxToggled(bool checked) {
  PluginConfig::instance()->setInsertSpaces(checked);
}

void ConfigDialog::onTabWidthSpinboxValueChanged(int newval) {
  PluginConfig::instance()->setTabWidth(newval);
}

void ConfigDialog::onFontFamilyComboBoxFontChanged(const QFont& newfont) {
  PluginConfig::instance()->setFontFamily(newfont.family());
}

void ConfigDialog::onFontSizeSpinboxValueChanged(int newval) {
  PluginConfig::instance()->setFontSize(newval);
}
