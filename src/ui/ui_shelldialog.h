/********************************************************************************
** Form generated from reading UI file 'shelldialog.ui'
**
** Created by: Qt User Interface Compiler version 5.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHELLDIALOG_H
#define UI_SHELLDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_shelldialog
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;

    void setupUi(QDialog *shelldialog)
    {
        if (shelldialog->objectName().isEmpty())
            shelldialog->setObjectName(QStringLiteral("shelldialog"));
        shelldialog->resize(400, 300);
        verticalLayout = new QVBoxLayout(shelldialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(shelldialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setContextMenuPolicy(Qt::CustomContextMenu);

        verticalLayout->addWidget(tabWidget);


        retranslateUi(shelldialog);

        QMetaObject::connectSlotsByName(shelldialog);
    } // setupUi

    void retranslateUi(QDialog *shelldialog)
    {
        shelldialog->setWindowTitle(QApplication::translate("shelldialog", "Dialog", 0));
    } // retranslateUi

};

namespace Ui {
    class shelldialog: public Ui_shelldialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHELLDIALOG_H
