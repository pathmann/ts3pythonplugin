#ifndef PYSHELL_H
#define PYSHELL_H

#include <QPlainTextEdit>

class pyshell : public QPlainTextEdit
{
    Q_OBJECT
  public:
    explicit pyshell(const QString& module, QWidget *parent = 0);

    static bool isMultiLine(const QString& cmd);

    QString module() const { return m_module; }
    void setModule(const QString module) { m_module = module; }

    void setFont(const QFont& f);
  signals:
    void onExecuteCommand(QString, QString);
    void contextMenuRequested(QPoint);
    void closeRequested(QString);
  public slots:
    void printResult(const QString& str);

    QString selectedText() const;
    void paste(const QString& str);

    void setTabCompletion(bool set);
    void setInsertSpaces(bool set);
  protected:
    void setDefaults();

    void keyPressEvent(QKeyEvent* e);
    void mousePressEvent(QMouseEvent* e);
    void mouseReleaseEvent(QMouseEvent* e);
    void mouseDoubleClickEvent(QMouseEvent* e);
    void contextMenuEvent(QContextMenuEvent* e);

    void removeCurrentLine();

    void writePrompt(bool newline = false);
    QString prompt() const;
    int promptLength() const;

    void doExecuteCommand();

    void doHistoryUp();
    void doHistoryDown();

    void doKeyboardInterrupt();
    void doEndOfFile();

    void doTab();
    void doUntab();

    QTextCursor promptCursor() const;
  private:
    bool m_multi;
    bool m_locked;

    QString m_module;
    QString m_command;

    QStringList m_history;
    int m_curhistory;

    bool m_tabcompletion;
    bool m_insertspaces;

    int m_comstate;
    QString m_comstr;
    QStringList m_completings;
};

#endif // PYSHELL_H
