#include "pyshell.h"

#include <QTextBlock>

pyshell::pyshell(const QString& module, QWidget *parent): QPlainTextEdit(parent), m_module(module), m_curhistory(0), m_comstate(-1) {
  QPalette p = palette();
  p.setColor(QPalette::Base, Qt::black);
  p.setColor(QPalette::Text, Qt::white);
  setPalette(p);

  setDefaults();

  m_locked = false;
  m_multi = false;

  writePrompt(false);
}

void pyshell::setDefaults() {
  m_tabcompletion = true;
  m_insertspaces = true;
  setTabStopWidth(4);

  QFont f;
  f.setFamily("Monospace");
  setFont(f);
}

void pyshell::setFont(const QFont& f) {
  QPlainTextEdit::setFont(f);

  QFontMetrics fm(f);
  setCursorWidth(fm.width("X"));
}

void pyshell::setTabCompletion(bool set) {
  m_tabcompletion = set;
}

void pyshell::setInsertSpaces(bool set) {
  m_insertspaces = set;
}

void pyshell::writePrompt(bool newline) {
  if (newline)
    textCursor().insertBlock();

  textCursor().insertText(prompt());
}

int pyshell::promptLength() const {
  return prompt().length();
}

QString pyshell::prompt() const {
  if (m_multi)
    return QString("... ");
  else return QString(">>> ");
}

void pyshell::keyPressEvent(QKeyEvent* e) {
  if (m_locked)
    return;

  if (textCursor() < promptCursor())
    moveCursor(QTextCursor::End);

  //buffer strings if locked?

  if (e->key() == Qt::Key_C && e->modifiers() == Qt::ControlModifier)
    doKeyboardInterrupt();
  else if (e->key() == Qt::Key_D && e->modifiers() == Qt::ControlModifier) {
    doEndOfFile();
    return;
  }
  else if (e->key() == Qt::Key_Backspace) {
    if (e->modifiers() == Qt::NoModifier && textCursor() > promptCursor())
      QPlainTextEdit::keyPressEvent(e);
  }
  else if ((e->key() == Qt::Key_Enter || e->key() == Qt::Key_Return) && e->modifiers() == Qt::NoModifier)
    doExecuteCommand();
  else if (e->key() == Qt::Key_Up && e->modifiers() == Qt::NoModifier)
    doHistoryUp();
  else if (e->key() == Qt::Key_Down && e->modifiers() == Qt::NoModifier)
    doHistoryDown();
  else if (e->key() == Qt::Key_Left && textCursor().positionInBlock() > promptLength())
    QPlainTextEdit::keyPressEvent(e);
  else if (e->key() == Qt::Key_Right)
    QPlainTextEdit::keyPressEvent(e);
  else if (e->key() == Qt::Key_Delete && e->modifiers() == Qt::NoModifier)
    QPlainTextEdit::keyPressEvent(e);
  else if (e->key() == Qt::Key_End && e->modifiers() == Qt::NoModifier)
    moveCursor(QTextCursor::EndOfLine);
  else if (e->key() == Qt::Key_Tab && e->modifiers() == Qt::NoModifier)
    doTab();
  else if (e->key() == Qt::Key_Tab && e->modifiers() == Qt::ShiftModifier)
    doUntab();
  else if (e->key() == Qt::Key_Home && e->modifiers() == Qt::NoModifier)
    setTextCursor(promptCursor());
  else QPlainTextEdit::keyPressEvent(e);
  /*if(e->key() >= 0x20 && e->key() <= 0x7e && (e->modifiers() == Qt::NoModifier || e->modifiers() == Qt::ShiftModifier))
    QPlainTextEdit::keyPressEvent(e);
    */
  
  ensureCursorVisible();
}

void pyshell::mousePressEvent(QMouseEvent* e) {
  setFocus();

  if (e->button() == Qt::LeftButton)
    QPlainTextEdit::mousePressEvent(e);
}

QTextCursor pyshell::promptCursor() const {
  QTextCursor retcur(this->textCursor());

  retcur.movePosition(QTextCursor::End);
  retcur.movePosition(QTextCursor::StartOfLine);
  retcur.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, promptLength());

  return retcur;
}

void pyshell::mouseReleaseEvent(QMouseEvent *e) {
  QPlainTextEdit::mouseReleaseEvent(e);

  if (e->button() == Qt::LeftButton) {
    QTextCursor cursor = textCursor();
    if (cursor.hasSelection() == false && cursor < promptCursor()) {
      cursor.movePosition(QTextCursor::End);
      setTextCursor(cursor);
    }
  }
  else if (e->button() == Qt::MidButton)
    paste(selectedText());
}

void pyshell::mouseDoubleClickEvent(QMouseEvent* e) {
  QPlainTextEdit::mouseDoubleClickEvent(e);
}

void pyshell::contextMenuEvent(QContextMenuEvent* e) {
  emit contextMenuRequested(mapToGlobal(e->pos()));
}

QString pyshell::selectedText() const {
  return textCursor().selectedText();
}

void pyshell::paste(const QString& str) {
  if (m_locked)
    return;

  moveCursor(QTextCursor::End);
  textCursor().insertText(str);
}

bool pyshell::isMultiLine(const QString& cmd) {
  if (cmd.trimmed().endsWith(":"))
    return true;
  return false;
}

void pyshell::doExecuteCommand() {
  moveCursor(QTextCursor::End);

  /*
  if (textCursor().positionInBlock() == promptLength() && !m_multi) {
    writePrompt(true);
    return;
  }
  */

  QString cmd = textCursor().block().text().mid(promptLength());

  if (cmd.trimmed().isEmpty()) {
    if (m_multi) {
      m_multi = false;

      writePrompt(true);
      m_locked = true;
      emit onExecuteCommand(m_module, m_command + "\n" + cmd);
      m_locked = false;
      m_command.clear();
    }
    else writePrompt(true);
  }
  else {
    m_history.append(cmd);
    m_curhistory = m_history.count();

    if (m_multi) {
      m_command.append("\n" + cmd);
      writePrompt(true);
    }
    else {
      if (isMultiLine(cmd)) {
        m_multi = true;
        m_command = cmd;

        writePrompt(true);
      }
      else {
        writePrompt(true);
        m_locked = true;
        emit onExecuteCommand(m_module, cmd);
        m_locked = false;
      }
    }
  }
}

void pyshell::removeCurrentLine() {
  moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
  moveCursor(QTextCursor::StartOfLine, QTextCursor::MoveAnchor);
  moveCursor(QTextCursor::End, QTextCursor::KeepAnchor);
  textCursor().removeSelectedText();
}

void pyshell::printResult(const QString& str) {
  if (m_comstate >= 0) {
    if (str.trimmed().isEmpty()) {
      m_comstate = -1;
      m_locked = false;

      if (m_completings.count() == 1) {
        setTextCursor(promptCursor());
        moveCursor(QTextCursor::End, QTextCursor::KeepAnchor);
        QString curtext = textCursor().selectedText();
        textCursor().removeSelectedText();

        //insert found completing without surrounding quotation marks
        textCursor().insertText(curtext.replace(m_comstr.replace("\\\"", "\""), m_completings[0].mid(1, m_completings[0].count() -2)));
      }
      else if (!m_completings.isEmpty()) {
        setTextCursor(promptCursor());
        moveCursor(QTextCursor::End, QTextCursor::KeepAnchor);
        QString curtext = textCursor().selectedText();
        textCursor().insertBlock();

        textCursor().insertText(m_completings.join("  "));
        writePrompt(true);
        textCursor().insertText(curtext);//m_comstr.replace("\\\"", "\""));
      }

      m_completings.clear();
    }
    else {
      m_completings.append(str.trimmed());
      emit onExecuteCommand(m_module, QString("__tabcompleter__.complete(\"%1\", %2)").arg(m_comstr).arg(++m_comstate));
    }

    ensureCursorVisible();
    return;
  }

  if (str == "\n")
    return;

  removeCurrentLine();
  textCursor().insertText(str);

  writePrompt(true);
  ensureCursorVisible();
}

void pyshell::doHistoryUp() {
  if (!m_curhistory)
    return;

  removeCurrentLine();
  writePrompt(false);
  textCursor().insertText(m_history[--m_curhistory]);
}

void pyshell::doHistoryDown() {
  if (m_curhistory == m_history.count())
    return;

  removeCurrentLine();
  writePrompt(false);

  if (m_curhistory++ < m_history.count() -1)
    textCursor().insertText(m_history[m_curhistory]);
}

void pyshell::doKeyboardInterrupt() {
  moveCursor(QTextCursor::End);

  textCursor().insertBlock();
  textCursor().insertText(tr("KeyboardInterrupt"));

  writePrompt(true);
}

void pyshell::doEndOfFile() {
  //delete key to the right if there are
  QTextCursor cur = textCursor();

  moveCursor(QTextCursor::Right);
  if (cur != textCursor())
    textCursor().deletePreviousChar();
  else if (textCursor() == promptCursor())
    emit closeRequested(m_module); //if no chars in cur command, close
}

void pyshell::doTab() {
  if (m_tabcompletion) {
    QTextCursor cur = textCursor();
    moveCursor(QTextCursor::EndOfWord);

    while (textCursor() > promptCursor() && textCursor().block().text()[textCursor().positionInBlock()] != ' ')
      moveCursor(QTextCursor::PreviousCharacter, QTextCursor::KeepAnchor);

    m_comstr = textCursor().selectedText().replace("\"", "\\\"").trimmed();
    setTextCursor(cur);

    m_locked = true;
    m_comstate = 0;
    emit onExecuteCommand(m_module, QString("__tabcompleter__.complete(\"%1\", %2)").arg(m_comstr).arg(m_comstate));
  }
  else {
    if (m_insertspaces)
      textCursor().insertText(QString("").fill(' ', tabStopWidth()));
    else textCursor().insertText("\t");
  }
}

void pyshell::doUntab() {
  if (m_tabcompletion)
    return;

  QTextCursor cur = textCursor();

  if (m_insertspaces) {
    textCursor().movePosition(QTextCursor::Left, QTextCursor::KeepAnchor, tabStopWidth());
    if (textCursor() > promptCursor() && textCursor().selectedText() == QString("").fill(' ', tabStopWidth())) {
      textCursor().removeSelectedText();
      return;
    }
  }
  else {
    textCursor().movePosition(QTextCursor::Left, QTextCursor::KeepAnchor);

    if (textCursor() > promptCursor() && textCursor().selectedText() == "\t") {
      textCursor().removeSelectedText();
      return;
    }
  }

  setTextCursor(cur);
}
