#ifndef SHELLDIALOG_H
#define SHELLDIALOG_H

#include "pythonhost.h"

#include "ui_shelldialog.h"

#include <QMenu>
#include <QHash>
#include <QString>

class pyshell;

class shelldialog : public QDialog, private Ui::shelldialog {
    Q_OBJECT

  public:
    explicit shelldialog(QWidget *parent = 0);
    ~shelldialog();

  public slots:
    void addShell();

  protected:
    void setupActions();

    void focusInEvent(QFocusEvent* e);
  protected slots:    
    void onOutputReceived(const QString& module, const QString& str);
    void onExecuteCommand(const QString& module, const QString& cmd);
    void onContextMenuRequested(const QPoint& pos);
    void onCloseRequested(const QString& module);

    void onConfigChanged();

    void onContextMenuPreferences();
    void onContextMenuCopy();
    void onContextMenuPaste();
    void onContextMenuCloseTab();
  private:
    QHash<QString, pyshell*> m_shells;

    QMenu* m_menu;
};

#endif // SHELLDIALOG_H
