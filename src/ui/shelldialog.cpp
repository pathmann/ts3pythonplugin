#include "shelldialog.h"

#include <QMessageBox>
#include <QClipboard>

#include "global_shared.h"
#include "pyshell.h"
#include "pystddispatcher.h"
#include "pluginconfig.h"
#include "configdialog.h"

shelldialog::shelldialog(QWidget *parent): QDialog(parent) {
  setupUi(this);
  setupActions();

  m_menu = new QMenu(this);
  m_menu->addAction(tr("Preferences"), this, SLOT(onContextMenuPreferences()));
  m_menu->addSeparator();
  m_menu->addAction(tr("Copy"), this, SLOT(onContextMenuCopy()));
  m_menu->addAction(tr("Paste"), this, SLOT(onContextMenuPaste()));
  m_menu->addSeparator();
  m_menu->addAction(tr("Close tab"), this, SLOT(onContextMenuCloseTab()));

  addShell();
}

shelldialog::~shelldialog() {
  foreach (QString key, m_shells.keys()) {
    PythonHost::instance()->deleteUserscriptModule(key);
    delete m_shells[key];
    user_scripts.remove(key);
  }
  m_shells.clear();

  m_menu->clear();
  delete m_menu;
}

void shelldialog::focusInEvent(QFocusEvent* ) {
  tabWidget->currentWidget()->setFocus();
}

void shelldialog::setupActions() {
  connect(pystddispatcher::instance(), &pystddispatcher::onMessage, this, &shelldialog::onOutputReceived);
  connect(tabWidget, &QTabWidget::customContextMenuRequested, this, &shelldialog::onContextMenuRequested);
  connect(PluginConfig::instance()->observable(), &Observable::changed, this, &shelldialog::onConfigChanged);
}

void shelldialog::addShell() {
  QString modname, error;
  if (PythonHost::instance()->addUserscriptModule(modname, error)) {
    user_scripts.insert(modname, QStringList());
    pyshell* sh = new pyshell(modname, this);
    m_shells.insert(modname, sh);
    tabWidget->addTab(sh, QString("Pyshell %1").arg(m_shells.count()));

    PluginConfig* conf = PluginConfig::instance();
    QFont f;
    f.setFamily(conf->fontFamily());
    f.setPointSize(conf->fontSize());
    sh->setFont(f);

    sh->setTabCompletion(conf->tabCompletion());
    sh->setInsertSpaces(conf->insertSpaces());
    sh->setTabStopWidth(conf->tabWidth());

    connect(sh, SIGNAL(onExecuteCommand(QString, QString)), this, SLOT(onExecuteCommand(QString, QString)));
    connect(sh, &pyshell::contextMenuRequested, this, &shelldialog::onContextMenuRequested);
    connect(sh, &pyshell::closeRequested, this, &shelldialog::onCloseRequested);

    PythonHost::instance()->setupUserscriptModule(modname, error);
    if (!error.isEmpty())
      sh->printResult(QString("Bootstrapping usermodule failed: %1").arg(error));

    sh->setFocus();
  }
  else {
    QMessageBox::warning(this, tr("Error adding module"), error);
    close();
  }
}

void shelldialog::onConfigChanged() {
  PluginConfig* conf = PluginConfig::instance();

  QFont f;
  f.setFamily(conf->fontFamily());
  f.setPointSize(conf->fontSize());

  foreach (pyshell* sh, m_shells) {
    sh->setFont(f);
    sh->setTabCompletion(conf->tabCompletion());
    sh->setInsertSpaces(conf->insertSpaces());
    sh->setTabStopWidth(conf->tabWidth());
  }
}

void shelldialog::onOutputReceived(const QString& module, const QString& str) {
  if (m_shells.keys().contains(module))
    m_shells[module]->printResult(str);
}

void shelldialog::onExecuteCommand(const QString& module, const QString& cmd) {
  QString error;

  user_scripts[module] << cmd;
  if (!PythonHost::instance()->execUserString(cmd, module, error)) {
    m_shells[module]->printResult(error);
    user_scripts[module].pop_back();
  }
}

void shelldialog::onContextMenuRequested(const QPoint& pos) {
  if (sender() == tabWidget)
    m_menu->popup(tabWidget->mapToGlobal(pos));
  else m_menu->popup(pos);
}

void shelldialog::onCloseRequested(const QString &module) {
  if (m_shells.contains(module)) {
    PythonHost::instance()->deleteUserscriptModule(module);
    tabWidget->removeTab(tabWidget->indexOf(m_shells[module]));
    delete m_shells[module];
    m_shells.remove(module);

    if (m_shells.isEmpty())
      close();
  }
}

void shelldialog::onContextMenuPreferences() {
  ConfigDialog* dlg = new ConfigDialog(this);
  dlg->setAttribute(Qt::WA_DeleteOnClose, true);
  dlg->show();
}

void shelldialog::onContextMenuCopy() {
  pyshell* sh = (pyshell*)tabWidget->currentWidget();

  if (sh)
    qApp->clipboard()->setText(sh->selectedText());
}

void shelldialog::onContextMenuPaste() {
  pyshell* sh = (pyshell*)tabWidget->currentWidget();

  if (sh)
    sh->paste(qApp->clipboard()->text());
}

void shelldialog::onContextMenuCloseTab() {
  pyshell* sh = (pyshell*)tabWidget->currentWidget();
  if (!sh)
    return;

  PythonHost::instance()->deleteUserscriptModule(sh->module());
  m_shells.remove(sh->module());
  delete sh;

  if (m_shells.isEmpty())
    close();
}
