#include "ts3helpersmodule.h"

#include <QFile>
#include <QUiLoader>
#include <QDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include "global_shared.h"
#include "python_helpers.h"
#include "pyqobject.h"
#include "pysignalslotconnection.h"

/*
class ts3helpers:
    """

    """
*/

static PyMethodDef ts3helpersmodfuncs[] = {
  {"getPluginID", getPluginID, METH_NOARGS, "Get the plugin's pluginID"},
  {"getSettingsTables", getSettingsTables, METH_VARARGS, "Returns the list of available tables in the client's settings database"},
  {"getSettingsKeys", getSettingsKeys, METH_VARARGS, "Returns the list of available keys in a table in the client's settings database"},
  {"getSettingsValue", getSettingsVariable, METH_VARARGS, "Returns a variable out the client's settings database"},
  {"setSettingsVariable", setSettingsVariable, METH_VARARGS, "Sets a variable in the client's settings database"},
  {"saveScript", saveScript, METH_VARARGS, "Stores the module build with the manually executed commands to a file"},
  {"clearScript", clearScript, METH_VARARGS, "Clears the module build with the manually executed commands"},
  {NULL, NULL, 0, NULL}
};

static PyModuleDef helpersdef = {
  PyModuleDef_HEAD_INIT,
  "ts3helpers",
  NULL,
  -1,
  ts3helpersmodfuncs,
  NULL,
  NULL,
  NULL,
  NULL
};


PyMODINIT_FUNC PyInit_ts3helpers(void) {
 return PyModule_Create(&helpersdef);
}

PyObject* getPluginID(PyObject* /* self */, PyObject* args) {
  if (!PyArg_ParseTuple(args, ""))
    return NULL;

  return PyBytes_FromString(pluginid);
}

PyObject* getSettingsTables(PyObject* /* self */, PyObject* args) {
  /*
    @staticmethod
    def getSettingsTables():
        """
        Returns the list of available tables in the client's settings database.
        """
  */
  if (!PyArg_ParseTuple(args, ""))
    return NULL;

	char path[256];
  funcs.getConfigPath(path, 256);

  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "pythonplugin_db_connection");
  db.setDatabaseName(QString("%1%2settings.db").arg(path).arg(QDir::separator()));

  if (!db.open()) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error opening database").toUtf8().data());
    return NULL;
  }

  PyObject* ret = qvariantToPyObject(db.tables());
  db.close();

  if (!ret) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error converting stringlist to python list").toUtf8().data());
    return NULL;
  }
  else return ret;
}

PyObject* getSettingsKeys(PyObject* /* self */, PyObject* args) {
  /*
    @staticmethod
    def getSettingsKeys(table):
        """
        Returns the list of available keys in a table in the client's settings database.
        @param table: the table of the database
        @type table: string
        """
  */
  char* table;

  if (!PyArg_ParseTuple(args, "s", &table))
    return NULL;

  char path[256];
  funcs.getConfigPath(path, 256);

  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "pythonplugin_db_connection");
  db.setDatabaseName(QString("%1%2settings.db").arg(path).arg(QDir::separator()));

  if (!db.open()) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error opening database").toUtf8().data());
    return NULL;
  }

  QSqlQuery q = db.exec(QString("SELECT key FROM %1").arg(table));
  db.close();

  PyObject* retlist = PyList_New(0);
  PyObject* it;

  for (int i = 0; i < q.size(); ++i) {
    it = PyUnicode_FromString(q.value(i).toString().toUtf8().data());

    if (!it) {
      Py_DECREF(retlist);
      PyErr_SetString(PyExc_AttributeError, QObject::tr("Error converting string to python string").toUtf8().data());
      return NULL;
    }

    if (PyList_Append(retlist, it) != 0) {
      Py_DECREF(it);
      Py_DECREF(retlist);
      PyErr_SetString(PyExc_AttributeError, QObject::tr("Error adding string to list").toUtf8().data());
      return NULL;
    }
    Py_DECREF(it);
  }

  return retlist;
}

PyObject* getSettingsVariable(PyObject* /* self */, PyObject* args) {
  /*
    @staticmethod
    def getSettingsVariable(table, key):
        """
        Returns a variable out the client's settings.
        @param table:
        @type table: string
        @param key: the key of the variable
        @type key: string
        @return: the value of the key in the database's table
        @rtype: string
        """
  */
  char* table;
  char* key;

  if (!PyArg_ParseTuple(args, "ss", &table, &key))
    return NULL;

  char path[256];
  funcs.getConfigPath(path, 256);

  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "pythonplugin_db_connection");
  db.setDatabaseName(QString("%1%2settings.db").arg(path).arg(QDir::separator()));

  if (!db.open()) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error opening database").toUtf8().data());
    return NULL;
  }

  QSqlQuery q = db.exec(QString("SELECT value FROM %1 WHERE key='%2'").arg(table).arg(key));
  db.close();

  if (q.size() != 1) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Database empty result set").toUtf8().data());
    return NULL;
  }

  return Py_BuildValue("s", q.value(0).toString().toUtf8().data());
}

PyObject* setSettingsVariable(PyObject* /* self */, PyObject* args) {
  /*
    @staticmethod
    def setSettingsVariable(table, key):
        """
        Sets a variable in the client's settings database.
        @param table:
        @type table: string
        @param key: the key of the variable
        @type key: string
        """
  */
  char* table;
  char* key;
  char* value;

  if (!PyArg_ParseTuple(args, "ss", &table, &key, &value))
    return NULL;

  char path[256];
  funcs.getConfigPath(path, 256);

  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "pythonplugin_db_connection");
  db.setDatabaseName(QString("%1%2settings.db").arg(path).arg(QDir::separator()));

  if (!db.open()) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error opening database").toUtf8().data());
    return NULL;
  }

  QSqlQuery q = db.exec(QString("UPDATE %1 SET value='%2' WHERE key='%3'").arg(table).arg(value).arg(key));
  db.close();

  if (q.lastError().isValid())
    Py_RETURN_TRUE;
  else Py_RETURN_FALSE;
}

PyObject* saveScript(PyObject* /* self */, PyObject* args) {
  /*
    @staticmethod
    def saveScript(modulename):
        """
        Stores the module build with the manually executed commands to a file in the scripts directory.
        @param modulename: the name of the module (without filename extension)
        @type modulename: string
        """
  */
  char* modulename;

  if (!PyArg_ParseTuple(args, "s", &modulename))
    return NULL;

  PyObject* loc = PyEval_GetLocals();
  if (!loc || !PyDict_Check(loc)) {
    PyErr_SetString(PyExc_RuntimeError, "locals dict not accessible");
    return NULL;
  }

  PyObject* name = PyDict_GetItemString(loc, "__name__");
  if (!name || !PyUnicode_Check(name)) {
    PyErr_SetString(PyExc_RuntimeError, "module name not found in locals dict");
    return NULL;
  }

  PyObject* pystr = PyUnicode_AsUTF8String(name);
  if (!pystr) {
    PyErr_SetString(PyExc_RuntimeError, "error converting module name to cstring");
    return NULL;
  }

  QString modname = PyBytes_AsString(pystr);
  Py_DECREF(pystr);

  if (user_scripts.contains(modname)) {
    char path[1024];
    funcs.getPluginPath(path, 1024);

    QDir dir(path);

    if (!dir.cd("python_plugin") || !dir.cd("scripts")) {
      PyErr_SetString(PyExc_RuntimeError, "scripts directory not found");
      return NULL;
    }

    QString fname = QString("%1.py").arg(dir.absoluteFilePath(modulename));
    QFile f(fname);

    if (f.open(QIODevice::WriteOnly | QIODevice::Text)) {
      f.write(user_scripts[modname].join("\n").toUtf8());

      f.close();
    }
    else {
      PyErr_SetString(PyExc_RuntimeError, QObject::tr("filewrite error to %1").arg(fname).toUtf8().data());
      return NULL;
    }

    Py_RETURN_NONE;
  }
  else {
    PyErr_SetString(PyExc_RuntimeError, "this module is not a valid user module");
    return NULL;
  }
}

PyObject* clearScript(PyObject* /* self */, PyObject* args) {
  /*
    @staticmethod
    def clearScript():
        """
        Clears the module build with the manually executed commands.
        """
  */
  if (!PyArg_ParseTuple(args, ""))
    return NULL;

  PyObject* loc = PyEval_GetLocals();
  if (!loc || !PyDict_Check(loc)) {
    PyErr_SetString(PyExc_RuntimeError, "locals dict not accessible");
    return NULL;
  }

  PyObject* name = PyDict_GetItemString(loc, "__name__");
  if (!name || !PyUnicode_Check(name)) {
    PyErr_SetString(PyExc_RuntimeError, "module name not found in locals dict");
    return NULL;
  }

  PyObject* pystr = PyUnicode_AsUTF8String(name);
  if (!pystr) {
    PyErr_SetString(PyExc_RuntimeError, "error converting module name to cstring");
    return NULL;
  }

  QString modname = PyBytes_AsString(pystr);
  Py_DECREF(pystr);

  if (user_scripts.contains(modname)) {
    user_scripts.insert(modname, QStringList());
    Py_RETURN_NONE;
  }
  else {
    PyErr_SetString(PyExc_RuntimeError, "this module is not a valid user module");
    return NULL;
  }
}
