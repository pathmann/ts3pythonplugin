#include "ts3logdispatcher.h"

#include <stdio.h>

#include "public_errors.h"


ts3logdispatcher::ts3logdispatcher(): singleton<ts3logdispatcher>() {

}

ts3logdispatcher::~ts3logdispatcher() {

}

void ts3logdispatcher::init(const TS3Functions thefunctions, const QString& channel) {
  m_funcs = thefunctions;
  m_chan = channel;
}

void ts3logdispatcher::add(const QString& msg, bool printtotab, bool tologfile, LogLevel lvl) {
  add(msg, printtotab, m_chan, tologfile, lvl);
}

void ts3logdispatcher::add(const QString& msg, bool printtotab, const QString& channel, bool tologfile,
                           LogLevel lvl) {
  if (printtotab) {
    QString tmsg = QString("%1 :: %2").arg(channel).arg(msg);
    m_funcs.printMessageToCurrentTab(tmsg.toUtf8().data());
  }

  if (tologfile) {
    if (unsigned int ret = m_funcs.logMessage(msg.toUtf8().data(), lvl, channel.toUtf8().data(), 0) != ERROR_ok) {
      char err[256];
      sprintf(err, "Unable log message (error=%u)", ret);
      m_funcs.printMessageToCurrentTab(err);
    }
  }
}
