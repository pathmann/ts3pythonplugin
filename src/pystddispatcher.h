#ifndef PYSTDDISPATCHER_H
#define PYSTDDISPATCHER_H

#include <QObject>
#include <QString>

class pystddispatcher: public QObject {
    Q_OBJECT

  public:
    static pystddispatcher* instance() {
      return ((m_watcher.m_object) ? m_watcher.m_object : m_watcher.m_object = new pystddispatcher);
    }

  public slots:
    void messageArrived(const QString& context, const QString& message);
  signals:
    void onMessage(QString, QString);

  protected:
    pystddispatcher(QObject* parent = 0);
    ~pystddispatcher();

    struct watcher {
        pystddispatcher* m_object;
        watcher() {
          m_object = 0;
        }

        ~watcher() {
          if (m_object)
            delete m_object;
        }
    };

    static watcher m_watcher;
};

#endif // PYSTDDISPATCHER_H
