#include "pysignalslotconnection.h"

#include "pythonhost.h"

pysignalslotconnection::pysignalslotconnection(QObject *sender, const char *signal, PyObject *slot, QObject *parent): QObject(parent), m_sender(sender), m_signal(signal), m_callable(slot) {
  Py_INCREF(m_callable);
}

pysignalslotconnection::~pysignalslotconnection() {
  Py_DECREF(m_callable);
}

bool pysignalslotconnection::connect() {
  return QObject::connect(m_sender, m_signal.toLatin1().data(), this, SLOT(onTriggered()));
}

void pysignalslotconnection::onTriggered() {
  printf("before calling callable\n");
  PyObject* ret = PyObject_CallObject(m_callable, NULL);
printf("callable called\n");
  if (!ret)
    printf("Error calling python slot from signal %s of object %s: %s\n", m_signal.toUtf8().data(), m_sender->objectName().toUtf8().data(), PythonHost::instance()->formatError("").toUtf8().data());
  else Py_DECREF(ret);
  printf("onTriggered durch\n");
}
