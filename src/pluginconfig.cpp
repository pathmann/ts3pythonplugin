#include "pluginconfig.h"

#include <QSettings>
#include <QFontDatabase>

#define CONFIG_NAME "python_plugin.conf"

#define ACTIVEPLUGINS_KEY "activeplugins"
#define FORCEDPLUGINS_KEY "forcedplugins"
#define SHOWINFOTITLE_KEY "showinfotitle"
#define SHELL_GROUP_KEY "shell"
#define TABCOMPLETION_KEY "tabcompletion"
#define INSERTSPACES_KEY "insertspaces"
#define TABWIDTH_KEY "tabwidth"
#define FONTFAMILY_KEY "fontfamily"
#define FONTSIZE_KEY "fontsize"

#define SHOWINFOTITLE_DEFAULT false
#define TABCOMPLETION_DEFAULT true
#define INSERTSPACES_DEFAULT true
#define TABWIDTH_DEFAULT 4
#define FONTFAMILY_DEFAULT "Monospace"
#define FONTSIZE_DEFAULT 8

PluginConfig::PluginConfig(): singleton<PluginConfig>(), m_observ(new Observable) {
  setDefaults();
}

PluginConfig::PluginConfig(const PluginConfig &copy): singleton<PluginConfig>(), m_observ(copy.m_observ) {
  m_activeplugins = copy.activatedPlugins();
  m_forcedplugins = copy.forcedPlugins();
}

PluginConfig::~PluginConfig() {

}

void PluginConfig::setDefaults() {
  m_showinfotitle = SHOWINFOTITLE_DEFAULT;
  m_tabcompletion = TABCOMPLETION_DEFAULT;
  m_insertspaces = INSERTSPACES_DEFAULT;
  m_tabwidth = TABWIDTH_DEFAULT;
  m_fontfamily = FONTFAMILY_DEFAULT;
  m_fontsize = FONTSIZE_DEFAULT;
}

void PluginConfig::load() {
  char path[256];
  funcs.getConfigPath(path, 256);

  QSettings set(QString("%1%2%3").arg(path).arg(QDir::separator()).arg(CONFIG_NAME), QSettings::IniFormat);

  m_activeplugins = set.value(ACTIVEPLUGINS_KEY, QStringList()).toStringList();
  m_forcedplugins = set.value(FORCEDPLUGINS_KEY, QStringList()).toStringList();

  m_showinfotitle = set.value(SHOWINFOTITLE_KEY, SHOWINFOTITLE_DEFAULT).toBool();

  set.beginGroup(SHELL_GROUP_KEY);
  m_tabcompletion = set.value(TABCOMPLETION_KEY, TABCOMPLETION_DEFAULT).toBool();
  m_insertspaces = set.value(INSERTSPACES_KEY, INSERTSPACES_DEFAULT).toInt();
  m_tabwidth = set.value(TABWIDTH_KEY, TABWIDTH_DEFAULT).toInt();
  m_fontfamily = set.value(FONTFAMILY_KEY, FONTFAMILY_DEFAULT).toString();
  if (!QFontDatabase().families().contains(m_fontfamily))
    m_fontfamily = FONTFAMILY_DEFAULT;
  m_fontsize = set.value(FONTSIZE_KEY, FONTSIZE_DEFAULT).toInt();
  set.endGroup();
}

void PluginConfig::save() const {
  char path[256];
  funcs.getConfigPath(path, 256);

  QSettings set(QString("%1%2%3").arg(path).arg(QDir::separator()).arg(CONFIG_NAME), QSettings::IniFormat);

  set.setValue(ACTIVEPLUGINS_KEY, m_activeplugins);
  set.setValue(FORCEDPLUGINS_KEY, m_forcedplugins);

  set.setValue(SHOWINFOTITLE_KEY, m_showinfotitle);

  set.beginGroup(SHELL_GROUP_KEY);
  set.setValue(TABCOMPLETION_KEY, m_tabcompletion);
  set.setValue(INSERTSPACES_KEY, m_insertspaces);
  set.setValue(TABWIDTH_KEY, m_tabwidth);
  set.setValue(FONTFAMILY_KEY, m_fontfamily);
  set.setValue(FONTSIZE_KEY, m_fontsize);
  set.endGroup();
}

const QStringList& PluginConfig::activatedPlugins() const {
  return m_activeplugins;
}

void PluginConfig::setActivatedPlugins(const QStringList& list) {
  m_activeplugins = list;
  m_observ->setChanged();
}

void PluginConfig::activatePlugin(const QString& name, bool set) {
  if (set)
    m_activeplugins.append(name);
  else m_activeplugins.removeOne(name);
  m_observ->setChanged();
}

bool PluginConfig::isActivated(const QString &name) const {
  return m_activeplugins.contains(name);
}

const QStringList& PluginConfig::forcedPlugins() const {
  return m_forcedplugins;
}

void PluginConfig::setForcedPlugins(const QStringList& list) {
  m_forcedplugins = list;
  m_observ->setChanged();
}

void PluginConfig::forcePlugin(const QString& name, bool set) {
  if (set)
    m_forcedplugins.append(name);
  else m_forcedplugins.removeOne(name);
  m_observ->setChanged();
}

bool PluginConfig::isForced(const QString& name) const {
  return m_forcedplugins.contains(name);
}

bool PluginConfig::showInfoTitle() const {
  return m_showinfotitle;
}

void PluginConfig::setShowInfoTitle(bool set) {
  m_showinfotitle = set;
  m_observ->setChanged();
}

bool PluginConfig::tabCompletion() const {
  return m_tabcompletion;
}

void PluginConfig::setTabCompletion(bool set) {
  m_tabcompletion = set;
  m_observ->setChanged();
}

bool PluginConfig::insertSpaces() const {
  return m_insertspaces;
}

void PluginConfig::setInsertSpaces(bool set) {
  m_insertspaces = set;
  m_observ->setChanged();
}

int PluginConfig::tabWidth() const {
  return m_tabwidth;
}

void PluginConfig::setTabWidth(int val) {
  m_tabwidth = val;
  m_observ->setChanged();
}

QString PluginConfig::fontFamily() const {
  return m_fontfamily;
}

void PluginConfig::setFontFamily(const QString& val) {
  m_fontfamily = val;
  m_observ->setChanged();
}

int PluginConfig::fontSize() const {
  return m_fontsize;
}

void PluginConfig::setFontSize(int val) {
  m_fontsize = val;
  m_observ->setChanged();
}
