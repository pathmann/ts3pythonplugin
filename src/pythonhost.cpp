#include "pythonhost.h"

#include <stdarg.h>

#include <QDir>

#include "python_helpers.h"
#include "ts3module.h"
#include "pystddispatcher.h"
#include "global_shared.h"
#include "pluginconfig.h"

#ifdef DEBUG_BUILD
  #define DEBUG_PRINT(x) printf("%s\n", x.toUtf8().data())
#else
  #define DEBUG_PRINT(x)
#endif

PythonHost::PythonHost() {
  m_pyhost = NULL;
  m_pmod = NULL;
  m_trace = NULL;
  m_redirect = NULL;
}

PythonHost::~PythonHost() {
  shutdown();
}

bool PythonHost::getDirectories(QString& error) {
  char path[256];
  funcs.getPluginPath(path, 256);

  m_plugindir.setPath(path);
  if (!m_plugindir.cd("python_plugin")) {
    if (!m_plugindir.mkdir("python_plugin")) {
      error = QObject::tr("Error creating plugin directory");
      return false;
    }
    else if (!m_plugindir.cd("python_plugin")) {
      error = QObject::tr("Error changing directory to plugin directory");
      return false;
    }
  }

  m_incldir = QDir(m_plugindir);
  if (!m_incldir.cd("includes")) {
    if (!m_incldir.mkdir("includes")) {
      error = QObject::tr("Error creating includes directory");
      return false;
    }
    else if (!m_incldir.cd("includes")) {
      error = QObject::tr("Error changing directory to includes directory");
      return false;
    }
  }

  m_moddir = QDir(m_plugindir);
  if (!m_moddir.cd("scripts")) {
    if (!m_moddir.mkdir("scripts")) {
      error = QObject::tr("Error creating scripts directory");
      return false;
    }
    else if (!m_moddir.cd("scripts")) {
      error = QObject::tr("Error changing directory to scripts directory");
      return false;
    }
  }

  return true;
}

void PythonHost::shutdown() {
  foreach (PyObject* m, m_modules)
    Py_DECREF(m);
  m_modules.clear();

  foreach (PyObject* m, m_usermodules)
    Py_DECREF(m);
  m_usermodules.clear();

  Py_XDECREF(m_pyhost);
  m_pyhost = NULL;

  Py_XDECREF(m_pmod);
  m_pmod = NULL;

  Py_XDECREF(m_trace);
  m_trace = NULL;

  Py_XDECREF(m_redirect);
  m_redirect = NULL;
}

bool PythonHost::isReady() const {
  return (m_pyhost && m_pmod && m_trace && m_redirect);
}

bool PythonHost::setup(QString& error) {
  if (!getDirectories(error))
    return false;

  PyObject* sys_path;
  PyObject* modpath;
  PyObject* inclpath;

  sys_path = PySys_GetObject("path");
  if (!sys_path) {
    error = QObject::tr("Error retrieving sys module");
    return false;
  }

  modpath = PyBytes_FromString(m_moddir.absolutePath().toUtf8().data());
  inclpath = PyBytes_FromString(m_incldir.absolutePath().toUtf8().data());

  if (!modpath || !inclpath) {
    error = QObject::tr("Error creating python directory string");
    Py_XDECREF(modpath);
    Py_XDECREF(inclpath);
    return false;
  }

  if (PyList_Append(sys_path, modpath) < 0) {
    error = QObject::tr("Error adding scripts directory to sys.path");
    Py_DECREF(modpath);
    Py_DECREF(inclpath);
    return false;
  }
  else Py_DECREF(modpath);

  if (PyList_Append(sys_path, inclpath) < 0) {
    error = QObject::tr("Error adding includes directory to sys.path");
    Py_DECREF(inclpath);
    Py_DECREF(modpath);
    return false;
  }
  else Py_DECREF(inclpath);

  m_trace = PyImport_ImportModule("traceback");
  if (!m_trace) {
    error = QObject::tr("Error importing traceback module");
    return false;
  }

  m_redirect = PyImport_ImportModule("redirector");
  if (!m_redirect) {
    error = QObject::tr("Error importing redirect module");
    return false;
  }

  m_pmod = PyImport_ImportModule("ts3plugin");
  if (!m_pmod) {
    error = formatError(QObject::tr("Error importing ts3plugin module"));
    return false;
  }

  m_pyhost = PyObject_GetAttrString(m_pmod, "PluginHost");
  if (!m_pyhost) {
    error = formatError(QObject::tr("Error getting PluginHost class"));
    return false;
  }

  foreach (QString plugin, m_moddir.entryList(QStringList() << "*.py")) {
    if (plugin == "ts3plugin.py")
      continue;

    PyObject* m = PyImport_ImportModule(plugin.section(".", 0, -2).toUtf8().data());

    if (m)
      m_modules.append(m);
    else {
      QString ferr = QObject::tr("Importing module failed with %1").arg(plugin);

      if (error.isEmpty())
        error = formatError(ferr);
      else error.append("\n" + formatError(ferr));
    }
  }

  return true;
}

QString PythonHost::getNextModuleName() const {
  if (m_usermodules.isEmpty())
    return QString("__ts3_userModule_1");

  QStringList modules = m_usermodules.keys();
  modules.sort();

  int last = modules.last().section('_', -1).toInt();

  return QString("__ts3_userModule_%1").arg(last +1);
}

bool PythonHost::addUserscriptModule(QString& name, QString& error, bool forcename) {
  if (!forcename)
    name = getNextModuleName();

  PyObject* newmod = PyModule_New(name.toLatin1().data());
  if (!newmod) {
    error = QObject::tr("Error creating module");
    return false;
  }

  if (PyModule_AddStringConstant(newmod, "__file__", "") == -1) {
    error = QObject::tr("Error adding __file__ attribute to module");
    Py_DECREF(newmod);
    return false;
  }

  PyObject* d = PyModule_GetDict(newmod);
  if (!d) {
    error = QObject::tr("Error getting module dict");
    Py_DECREF(newmod);
    return false;
  }

  if (PyDict_SetItemString(d, "__builtins__", PyEval_GetBuiltins()) != 0) {
    error = QObject::tr("Error adding __builtins__ to module");
    Py_DECREF(newmod);
    return false;
  }

  PyObject* modules = PyImport_GetModuleDict();
  if (PyDict_SetItemString(modules, name.toUtf8().data(), newmod) != 0) {
    error = QObject::tr("Error adding module to modules");
    Py_DECREF(newmod);
    return false;
  }

  m_usermodules.insert(name, newmod);

  if (!execUserString(QString("import %1, rlcompleter").arg(name), name, error)) {
    deleteUserscriptModule(name);
    return false;
  }

  if (!execUserString(QString("__tabcompleter__ = rlcompleter.Completer(%1.__dict__)").arg(name), name, error)) {
    deleteUserscriptModule(name);
    return false;
  }

  return true;
}

void PythonHost::setupUserscriptModule(const QString& module, QString& error) {
  if (QFile::exists(m_incldir.absoluteFilePath("usermodbootstrap.py"))) {
    QFile f(m_incldir.absoluteFilePath("usermodbootstrap.py"));

    if (f.open(QIODevice::ReadOnly | QIODevice::Text)) {
      execUserString(f.readAll(), module, error);

      f.close();
    }
    else error = QObject::tr("Error opening usermodbootstrap.py");
  }
}

bool PythonHost::reloadUserscriptModule(const QString& module, QString& error) {
  deleteUserscriptModule(module);

  QString refmodule = module;
  if (addUserscriptModule(refmodule, error, true)) {
    setupUserscriptModule(module, error);
    return true;
  }

  return false;
}

void PythonHost::deleteUserscriptModule(const QString& module) {
  if (!m_usermodules.contains(module))
    return;

  Py_DECREF(m_usermodules[module]);
  m_usermodules.remove(module);
}

QString PythonHost::formatError(const QString& fallback) {
  if (!PyErr_Occurred())
    return fallback;

  QString error;
  PyObject* pyerrtype = NULL, *pyerr = NULL, *pytrace = NULL;
  PyErr_Fetch(&pyerrtype, &pyerr, &pytrace);
  PyErr_NormalizeException(&pyerrtype, &pyerr, &pytrace);

  bool success = false;
  PyObject* formatfunc = PyObject_GetAttrString(m_trace, "format_exception");

  if (formatfunc && PyCallable_Check(formatfunc)) {
    PyObject* ret = PyObject_CallFunction(formatfunc, const_cast<char*>("OOO"), pyerrtype, pyerr, pytrace);

    if (ret && PyList_Check(ret)) {
      PyObject* utfstr;

      for (int i = 0; i < PyList_Size(ret); ++i) {
        utfstr = PyUnicode_AsUTF8String(PyList_GetItem(ret, i));

        if (utfstr) {
          error.append(PyBytes_AsString(utfstr));
          Py_DECREF(utfstr);
        }
      }

      success = true;
    }
    Py_XDECREF(ret);
  }
  Py_XDECREF(formatfunc);


  Py_XDECREF(pyerrtype);
  Py_XDECREF(pyerr);
  Py_XDECREF(pytrace);

  if (success)
    return error;
  else return fallback;
}

bool PythonHost::refresh(QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  foreach (PyObject* mod, m_modules)
    Py_DECREF(mod);
  m_modules.clear();

  QString longerr = QObject::tr("Init module failed in %1 (%2)");
  QString shorterr = QObject::tr("Init module failed in %1");

  foreach (QString plugin, m_moddir.entryList(QStringList() << "*.py")) {
    if (plugin == "ts3plugin")
      continue;

    PyObject* m = PyImport_ImportModule(plugin.section(".", 0, -2).toUtf8().data());

    if (m)
      m_modules.append(m);
    else {
      QString merr = formatError("");

      if (merr.isEmpty()) {
        if (error.isEmpty())
          error = shorterr.arg(plugin);
        else error.append("\n" + shorterr.arg(plugin));
      }
      else {
        if (error.isEmpty())
          error = longerr.arg(plugin).arg(merr);
        else error.append("\n" + longerr.arg(plugin).arg(merr));
      }
    }
  }

  return true;
}

bool PythonHost::start(QString& error) {
  if (!isReady()) {
    error = QObject::tr("PluginHost is not running");
    return false;
  }

  PyObject* names = qstringlistToPyList(PluginConfig::instance()->activatedPlugins(), error);
  if (!names) {
    error.append(QObject::tr(" (creating pylist of plugins to start)"));
    return false;
  }

  PyObject* forced = qstringlistToPyList(PluginConfig::instance()->forcedPlugins(), error);
  if (!forced) {
    error.append(QObject::tr(" (creating pylist of plugins to force start)"));
    Py_DECREF(names);
    return false;
  }

  PyObject* params = Py_BuildValue("(iOO)", API_VER, names, forced);
  Py_DECREF(names);
  Py_DECREF(forced);

  if (!params) {
    error = QObject::tr("Error creating parameters for PluginHost.start");
    return false;
  }

  PyObject* ret = PyObject_CallMethod(m_pyhost, const_cast<char *>("start"), const_cast<char *>("O"), params);
  Py_DECREF(params);

  if (!ret) {
    error = formatError(QObject::tr("Error calling PluginHost.start"));
    return false;
  }

  //if some module throws an exception while importing
  error = formatError(QString());

  QString merr;
  QStringList lowapis;
  if (!pyListToQstringlist(ret, &lowapis, merr)) {
    if (error.isEmpty())
      error.append("\n" + merr + QObject::tr(" (creating clist of low api version plugins)"));
    else error = merr + QObject::tr(" (creating clist of low api version plugins)");

    Py_DECREF(ret);
    return false;
  }
  Py_DECREF(ret);

  if (!lowapis.isEmpty()) {
    if (error.isEmpty())
      error = QObject::tr("Some modules have a lower api version than your client, so they wasn't loaded: %1").arg(lowapis.join(","));
    else error.append(QObject::tr("\nSome modules have a lower api version than your client, so they wasn't loaded: %1").arg(lowapis.join(",")));
  }

  return true;
}

bool PythonHost::stop(QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return true;
  }

  PyObject* stopmeth = PyObject_GetAttrString(m_pyhost, "stop");
  if (!stopmeth) {
    error = QObject::tr("Error getting method PluginHost.stop");
    return false;
  }

  PyObject* ret = PyObject_CallObject(stopmeth, NULL);
  Py_DECREF(stopmeth);

  if (!ret) {
    error = formatError(QObject::tr("Error calling PluginHost.stop"));
    return false;
  }

  Py_DECREF(ret);

  return true;
}

bool PythonHost::startPlugin(const QString& name, QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  PyObject* ret = PyObject_CallMethod(m_pyhost, const_cast<char*>("startPlugin"), const_cast<char *>("s"), name.toUtf8().data());
  if (!ret) {
    error = formatError(QObject::tr("Error calling PluginHost.startPlugin with module %1").arg(name));
    return false;
  }
  Py_DECREF(ret);

  return true;
}

bool PythonHost::stopPlugin(const QString& name, QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  PyObject* ret = PyObject_CallMethod(m_pyhost, const_cast<char*>("stopPlugin"), const_cast<char *>("s"), name.toUtf8().data());

  if (!ret) {
    error = formatError(QObject::tr("Error calling PluginHost.stopModule with module %1").arg(name));
    return false;
  }
  Py_DECREF(ret);

  return true;
}

bool PythonHost::configurePlugin(const QString& name, QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  PyObject* ret = PyObject_CallMethod(m_pyhost, const_cast<char*>("configurePlugin"), const_cast<char*>("s"), name.toUtf8().data());

  if (!ret) {
    error = formatError(QObject::tr("Error calling PluginHost.configurePlugin with module %1").arg(name));
    return false;
  }
  Py_DECREF(ret);

  return true;
}

bool PythonHost::isRunning(const QString& name, QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  PyObject* ret = PyObject_CallMethod(m_pyhost, const_cast<char *>("isRunning"), const_cast<char *>("s"), name.toUtf8().data());

  if (!ret) {
    error = formatError(QObject::tr("Error calling PluginHost.isRunning with module %1").arg(name));
    return false;
  }

  bool runs = false;

  switch (PyObject_IsTrue(ret)) {
    case -1:
      error = QObject::tr("Error getting result of PluginHost.isRunning with module %1").arg(name);
      break;
    case 1:
      runs = true;
  }

  Py_DECREF(ret);

  return runs;
}

bool PythonHost::pluginInfo(const QString& name, PythonPluginInfo* info, QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  PyObject* pyret = PyObject_CallMethod(m_pyhost, const_cast<char *>("pluginInfo"), const_cast<char *>("s"), name.toUtf8().data());
  if (!pyret) {
    error = formatError(QObject::tr("Error calling PluginHost.pluginInfo with module %1").arg(name));
    return false;
  }

  if (pyret == Py_None) {
    error = QObject::tr("Plugin %s was not found").arg(name);
    Py_DECREF(pyret);
    return false;
  }

  char* pname, *pversion, *pdescription, *pauthor;
  int papiver, hasconfigure;

  if (!PyArg_ParseTuple(pyret, "ssssii", &pname, &pversion, &pdescription, &pauthor, &papiver, &hasconfigure)) {
    error = QObject::tr("Error getting result of PluginHost.pluginInfo with module %1").arg(name);
    Py_DECREF(pyret);
    return false;
  }
  Py_DECREF(pyret);

  info->name = pname;
  info->author = pauthor;
  info->description = pdescription;
  info->version = pversion;
  info->apiVersion = papiver;
  info->hasConfigure = (hasconfigure == 1);

  return true;
}

QStringList PythonHost::allPlugins(QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return QStringList();
  }

  PyObject* pyret = PyObject_CallMethod(m_pyhost, const_cast<char *>("allPlugins"), NULL);
  if (!pyret) {
    error = formatError(QObject::tr("Error calling PluginHost.allPlugins"));
    return QStringList();
  }

  if (!PyList_Check(pyret)) {
    error = QObject::tr("Error getting result of PluginHost.allPlugins (no list returned)");
    Py_DECREF(pyret);
    return QStringList();
  }

  QStringList ret;
  for (int i = 0; i < PyList_Size(pyret); ++i) {
    PyObject* it = PyList_GetItem(pyret, i);

    if (!it) {
      error = QObject::tr("Error getting item out of list returned by PluginHost.allPlugins");
      Py_DECREF(pyret);
      return QStringList();
    }

    if (!PyUnicode_Check(it)) {
      error = QObject::tr("Error getting item out of list returned by PluginHost.allPlugins (no unicode value)");
      Py_DECREF(pyret);
      Py_DECREF(it);
      return QStringList();
    }

    PyObject* pystring = PyUnicode_AsUTF8String(it);
    Py_DECREF(it);

    if (!pystring) {
      error = QObject::tr("Error getting item out of list returned by PluginHost.allPlugins (no string value)");
      Py_DECREF(pyret);
      return QStringList();
    }

    ret << QString(PyBytes_AsString(pystring));
    Py_DECREF(pystring);
  }
  Py_DECREF(pyret);

  return ret;
}

/* Format strings
 * K: unsigned long long (used for uint64)
 * i: int
 * I: unsigned int (used for anyID)
 * s: char*
 * d: double
 * f: float (only used as array, will be transformed to array of double)
 * h: short (only used as array, will be transformed to array of int)
 * //currently not needed ax: starting an null-terminated array of type x (x elem of [h,i,I,f]
*/

bool PythonHost::call(const QString& func, bool* ret, QString& error, const char* format, ...) {
  DEBUG_PRINT(QString("PythonPlugin: Calling %1").arg(func));

  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  unsigned long long pK;
  int pi;
  unsigned int pI;
  char* ps;
  double pd;
  /*short* pah;
  int* pai;
  unsigned int* paI;
  float* paf;*/

  PyObject* arg;
  QList<PyObject*> args;
  QString fmt(strlen(format) == 1 ? "(%1)" : "%1");

  if (strlen(format) > 0) {
    va_list vl;
    va_start(vl, format);
    bool abort = false;

    while (*format && !abort) {
      switch (*format++) {
        case 'K':
          pK = va_arg(vl, unsigned long long);
          arg = Py_BuildValue(fmt.arg("K").toLatin1().data(), pK);

          if (!arg) {
            error = QObject::tr("Error building PyLongLong (from unsigned long long)");
            abort = true;
          }
          else args.append(arg);
          break;
        case 'i':
          pi = va_arg(vl, int);
          arg = Py_BuildValue(fmt.arg("i").toLatin1().data(), pi);

          if (!arg) {
            error = QObject::tr("Error building PyLong (from int)");
            abort = true;
          }
          else args.append(arg);
          break;
        case 'I':
          pI = va_arg(vl, unsigned int);
          arg = Py_BuildValue(fmt.arg("I").toLatin1().data(), pI);

          if (!arg) {
            error = QObject::tr("Error building PyLong (from unsigned int)");
            abort = true;
          }
          else args.append(arg);
          break;
        case 's':
          ps = va_arg(vl, char*);
          if (func == "onIncomingClientQueryEvent")
            DEBUG_PRINT(QString("command: %1").arg(ps));
          if (ps) {
            PyObject* str = PyUnicode_FromString(ps);
            if (str) {
              arg = Py_BuildValue(fmt.arg("O").toLatin1().data(), str);
              Py_DECREF(str);
            }
            else arg = NULL;
          }
          else arg = Py_BuildValue(fmt.arg("s").toLatin1().data(), "");

          if (!arg) {
            error = QObject::tr("Error building PyBytesString (from char*)");
            abort = true;
          }
          else args.append(arg);
          break;
        case 'd':
          pd = va_arg(vl, double);
          arg = Py_BuildValue(fmt.arg("d").toLatin1().data(), pd);

          if (!arg) {
            error = QObject::tr("Error building python float (from double)");
            abort = true;
          }
          else args.append(arg);
          break;
        case 'a':
          switch (*format++) {
            /*case 'h':
              pah = va_arg(vl, short*);
              arg = shortListToPyList(pah);

              if (!arg) {
                perr->append(QObject::tr("Error building list of PyLong (from short*)"));
                abort = true;
              }
              else args.append(arg);
              break;
            case 'i':
              pai = va_arg(vl, int*);
              arg = intListToPyList(pai);

              if (!arg) {
                perr->append(QObject::tr("Error building list of PyLong (from int*)"));
                abort = true;
              }
              else args.append(arg);
              break;
            case 'I':
              paI = va_arg(vl, unsigned int*);
              arg = unsignedIntListToPyList(paI);

              if (!arg) {
                perr->append(QObject::tr("Error building list of PyLong (from unsigned int*)"));
                abort = true;
              }
              else args.append(arg);
              break;
            case 'f':
              paf = va_arg(vl, float*);
              arg = floatListToPyList(paf);

              if (!arg) {
                perr->append(QObject::tr("Error building list of python floats (from float*)"));
                abort = true;
              }
              else args.append(arg);
              break;*/
            default:
              error = QObject::tr("Unknown array format char %1").arg(*--format);
              abort = true;
              break;
          }

          break;
        default:
          error = QObject::tr("Unknown format char %1").arg(*--format);
          abort = true;
          break;
      }

      if (abort) {
        listDecref(args);
        va_end(vl);
        return false;
      }
    }

    va_end(vl);
  }

  PyObject* params = NULL;
  if (!args.isEmpty()) {
    if (args.count() == 1)
      params = args[0];
    else {
      params = tupleFromPyObjectQlist(args);

      if (!params) {
        error = QObject::tr("Error building parameter tuple from arguments");
        listDecref(args);
        return false;
      }
    }
  }

  PyObject* meth = PyObject_GetAttrString(m_pyhost, func.toUtf8().data());
  if (!meth) {
    error = QObject::tr("Error getting method from PluginHost");
    Py_XDECREF(params);
    return false;
  }

  PyObject* pyret = PyObject_CallObject(meth, params);
  if (!pyret) {
    error = formatError(QObject::tr("Error calling PluginHost-method %1").arg(func));

    Py_XDECREF(params);
    Py_DECREF(meth);
    return false;
  }

  Py_XDECREF(params);
  Py_DECREF(meth);

  if (ret)
    *ret = PyObject_IsTrue(pyret);

  Py_DECREF(pyret);

  return true;
}

bool PythonHost::getInfoData(uint64 schid, uint64 id, int type, QStringList& ret, QString& error) {
  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  PyObject* pyret = PyObject_CallMethod(m_pyhost, const_cast<char*>("infoData"), const_cast<char*>("KKi"), (unsigned long long)schid, (unsigned long long)id, type);
  if (!pyret) {
    error = formatError(QObject::tr("Error calling PluginHost.infoData"));
    return false;
  }

  ret = pyObjectToQVariant(pyret).toStringList();
  Py_DECREF(pyret);

  return true;
}

bool PythonHost::execUserString(const QString& str, const QString& module, QString& error) {
  error.clear();

  if (!isReady()) {
    error = QObject::tr("PythonHost is not running");
    return false;
  }

  if (!m_usermodules.contains(module)) {
    error = QObject::tr("A module with that name does not exist");
    return false;
  }

  PyObject* locals = PyModule_GetDict(m_usermodules[module]);
  if (!locals)
    return false;

  PyObject* globals = PyDict_New();
  if (!globals)
    return false;

  if (PyDict_SetItemString(globals, "__builtins__", PyEval_GetBuiltins()) == -1) {
    Py_DECREF(globals);
    return false;
  }

  PyObject* code = Py_CompileString(str.toUtf8().data(), module.toUtf8().data(), Py_single_input);
  if (!code) {
    error = formatError(QObject::tr("Error compiling string"));
    return false;
  }

  PyObject* ret = PyEval_EvalCode(code, globals, locals);
  Py_DECREF(code);

  if (!ret) {
    error = formatError("Error executing string");
    return false;
  }

  Py_DECREF(ret);

  return true;
}
