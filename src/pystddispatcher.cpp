#include "pystddispatcher.h"

pystddispatcher::watcher pystddispatcher::m_watcher;

pystddispatcher::pystddispatcher(QObject* parent): QObject(parent) {

}

pystddispatcher::~pystddispatcher() {

}

void pystddispatcher::messageArrived(const QString& context, const QString& message) {
  if (!context.startsWith("__ts3_userModule_"))
    printf("%s\n", message.toUtf8().data());

  emit onMessage(context, message);
}
