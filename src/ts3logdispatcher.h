#ifndef TS3LOGDISPATCHER_H
#define TS3LOGDISPATCHER_H

#include "singleton.h"

#include <QString>

#include "ts3_functions.h"
#include "public_definitions.h"

class ts3logdispatcher: public singleton<ts3logdispatcher> {
  friend class singleton<ts3logdispatcher>;

  public:
    void init(const struct TS3Functions thefunctions, const QString& channel = QString());

    void add(const QString& msg, bool printtotab, bool tologfile = false,
             LogLevel lvl = LogLevel_INFO);
    void add(const QString& msg, bool printtotab, const QString& channel, bool tologfile = false,
             LogLevel lvl = LogLevel_INFO);
  private:
    ts3logdispatcher();
    ~ts3logdispatcher();

    struct TS3Functions m_funcs;
    QString m_chan;
};

#endif // TS3LOGDISPATCHER_H
