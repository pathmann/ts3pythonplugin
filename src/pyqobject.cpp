#include "pyqobject.h"

#include <QVariant>
#include <QFile>
#include <QUiLoader>
#include <QDialog>

#include "python_helpers.h"
#include "pysignalslotconnection.h"

/*
class pyqobject:
    """
    This class wraps C++ QObjects (especially Qt Dialogs) to a python class.
    """
*/

static PyMethodDef pyqobject_methods[] = {
  {"property", (PyCFunction)pyqobject_getProperty, METH_VARARGS, "Return the value of a property"},
  {"setProperty", (PyCFunction)pyqobject_setProperty, METH_VARARGS, "Return the value of a property"},
  {"getChildren", (PyCFunction)pyqobject_getChildren, METH_VARARGS, "Return a children of an object"},
  {"connect", (PyCFunction)pyqobject_connect, METH_VARARGS, "Connect a python callback to a signal"},
  { NULL, NULL, 0, NULL }
};

static PyTypeObject pyqobject_type = {
  PyVarObject_HEAD_INIT(NULL, 0)
  "pyqobject.pyqobject",             /* tp_name */
  sizeof(pyqobject),             /* tp_basicsize */
  0,                         /* tp_itemsize */
  (destructor)pyqobject_dealloc,   /* tp_dealloc */
  0,                         /* tp_print */
  0,                         /* tp_getattr */
  0,                         /* tp_setattr */
  0,                         /* tp_reserved */
  0,                         /* tp_repr */
  0,                         /* tp_as_number */
  0,                         /* tp_as_sequence */
  0,                         /* tp_as_mapping */
  0,                         /* tp_hash  */
  0,                         /* tp_call */
  0,                         /* tp_str */
  0,                         /* tp_getattro */
  0,                         /* tp_setattro */
  0,                         /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT,   /* tp_flags */
  "Wrapping QObjects to python",           /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  pyqobject_methods,         /* tp_methods */
  0,                          /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)pyqobject_init,  /* tp_init */
  0,                         /* tp_alloc */
  pyqobject_new,             /* tp_new */
  0,                         /* tp_free */
  0,                         /* tp_is_gc */
  0,                         /* tp_bases */
  0,                         /* tp_mro */
  0,                         /* tp_cache */
  0,                         /* tp_subclasses */
  0,                         /* tp_weaklist */
  0,                         /* tp_del */
  0                          /* tp_version_tag */
};

static PyModuleDef pyqobject_module = {
  PyModuleDef_HEAD_INIT,
  "pyqobject",
  "Module that wraps Qt QObjects to a python type.",
  -1,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};

PyObject* pyqobject_new(PyTypeObject* type, PyObject* /* args */, PyObject* /* kwds */) {
  pyqobject* self = (pyqobject*)(type->tp_alloc(type, 0));
  if (!self)
    return NULL;

  self->ownership = false;
  self->data = NULL;

  return (PyObject*)self;
}

int pyqobject_init(pyqobject* self, PyObject* args, PyObject* /* kwds */) {
  /*
    def __init__(self, path):
        """
        Constructs a dialog by a given UI file. The corresponding QObject is deleted with the python deconstructor, so the caller owns it!
        @param path: path to the UI file, toplevel widget needs to be QDialog (or inherited from QDialog)
        @type path: string
        """
  */
  char* path;

  if (!PyArg_ParseTuple(args, "s", &path))
    return -1;

  if (!QFile::exists(path)) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("File does not exist").toUtf8().data());
    return -1;
  }

  QFile f(path);
  if (!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Unable to open file").toUtf8().data());
    return -1;
  }

  QUiLoader loader;
  QWidget* w = loader.load(&f);

  if (!w) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error loading widget").toUtf8().data());
    return -1;
  }

  QDialog* d = dynamic_cast<QDialog*>(w);
  if (!d) {
    delete w;
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Toplevel widget must be a dialog").toUtf8().data());
    return -1;
  }

  self->ownership = true;
  self->data = d;
  return 0;
}

void pyqobject_dealloc(pyqobject* self) {
  /*
    def __del__(self):
        """
        Destructor of the class. Allocated memory (the QObject) will be freed, if owned.
        """
  */
  printf("dealloc\n");
  if (self->ownership)
    delete self->data;
  Py_TYPE(self)->tp_free((PyObject*)self);
}

PyObject* pyqobject_getChildren(pyqobject* self, PyObject* args) {
  /*
    def getChildren(self, objectname):
        """
        Returns a child object (see QDialog::children()) of self object.
        @param objectname: the children's objectname
        @type objectname: string
        @return: the child object wrapped as pyqobject
        @rtype: pyqobject
        """
  */
  char* childname;

  if (!PyArg_ParseTuple(args, "s", &childname))
    return NULL;

  QObjectList childs = self->data->children();
  foreach (QObject* c, childs) {
    if (c->objectName() == childname) {
      pyqobject* pychild = (pyqobject*)pyqobject_new(&pyqobject_type, NULL, NULL);
      if (!pychild) {
        PyErr_SetString(PyExc_AttributeError, QObject::tr("Error creating child").toUtf8().data());
        return NULL;
      }

      pychild->data = c;

      return (PyObject*)pychild;
    }
  }

  PyErr_SetString(PyExc_AttributeError, QObject::tr("Unknown children").toUtf8().data());
  return NULL;
}

PyObject* pyqobject_getProperty(pyqobject* self, PyObject* args) {
  /*
    def getProperty(self, propertyname):
        """
        Returns the value of the objects property (see QObject::property(const char* name)).
        @param propertyname: the name of the property
        @type propertyname: string
        @return: the value
        @rtype: list or int or string
        """
  */
  char* propname;

  if (!PyArg_ParseTuple(args, "s", &propname))
    return NULL;

  QVariant ret = self->data->property(propname);

  if (ret.isValid())
    return qvariantToPyObject(ret);
  else {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Unknown property").toUtf8().data());
    return NULL;
  }
}

PyObject* pyqobject_setProperty(pyqobject* self, PyObject* args) {
  /*
    def setProperty(self, propertyname):
        """
        Sets the value of the objects property (see QObject::setProperty(const char* name, const QVariant& value)).
        @param propertyname: the name of the property
        @type propertyname: string
        """
  */
  char* propname;
  PyObject* pypropval;

  if (!PyArg_ParseTuple(args, "sO", &propname, &pypropval))
    return NULL;

  QVariant propval = pyObjectToQVariant(pypropval);
  if (!propval.isValid()) {
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error converting property value").toUtf8().data());
    return NULL;
  }

  self->data->setProperty(propname, propval);
  Py_RETURN_NONE;
}

PyObject* pyqobject_connect(pyqobject *self, PyObject *args) {
  /*
    def connect(self, signal, callback):
        """
        Connects a python callback to a signal of the object.
        @param signal: the signature of the signal (eg. if the signal "finished" should be connected, "finished(int)" should be passed)
        @type signal: string
        @param callback: the callback function
        @type callback: callable
        """
  */
  char* signal;
  PyObject* callback;

  if (!PyArg_ParseTuple(args, "sO", &signal, &callback))
    return NULL;

  if (!PyCallable_Check(callback)) {
    Py_DECREF(callback);
    PyErr_SetString(PyExc_AttributeError, QObject::tr("No valid callback function").toUtf8().data());
    return NULL;
  }

  pysignalslotconnection* slot = new pysignalslotconnection(self->data, QString::fromLatin1("2%1").arg(QString::fromLatin1(signal)).toLatin1().data(), callback, self->data);
  if (slot->connect())
    Py_RETURN_NONE;
  else {
    delete slot;
    PyErr_SetString(PyExc_AttributeError, QObject::tr("Error connecting to signal %1").arg(signal).toUtf8().data());
    return NULL;
  }
}

PyMODINIT_FUNC PyInit_pyqobject(void) {
  pyqobject_type.tp_new = PyType_GenericNew;
  if (PyType_Ready(&pyqobject_type) < 0)
    return NULL;

  PyObject* m = PyModule_Create(&pyqobject_module);
  if (!m)
    return NULL;

  Py_INCREF(&pyqobject_type);
  PyModule_AddObject(m, "pyqobject", (PyObject*)&pyqobject_type);
  return m;
}
