#ifndef TS3HELPERSMODULE_H
#define TS3HELPERSMODULE_H

#include <Python.h>

PyObject* getPluginID(PyObject* self, PyObject* args);
PyObject* getSettingsTables(PyObject* self, PyObject* args);
PyObject* getSettingsKeys(PyObject* self, PyObject* args);
PyObject* getSettingsVariable(PyObject* self, PyObject* args);
PyObject* setSettingsVariable(PyObject* self, PyObject* args);
PyObject* saveScript(PyObject* self, PyObject* args);
PyObject* clearScript(PyObject* self, PyObject* args);


PyMODINIT_FUNC PyInit_ts3helpers(void);

#endif // TS3HELPERSMODULE_H
