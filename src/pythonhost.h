#ifndef PYTHONHOST_H
#define PYTHONHOST_H

#include <Python.h>

#include "singleton.h"

#include "public_definitions.h"

#include <QList>
#include <QStringList>
#include <QDir>
#include <QHash>

typedef struct {
  QString name;
  QString version;
  QString description;
  QString author;
  int apiVersion;
  bool hasConfigure;
} PythonPluginInfo;

class PythonHost: public singleton<PythonHost> {
    friend class singleton<PythonHost>;
  public:
    bool setup(QString&      error);
    void shutdown();

    bool isReady() const;

    bool refresh(QString& error);
    bool start(QString& error);
    bool stop(QString& error);

    bool startPlugin(const QString& name, QString& error);
    bool stopPlugin(const QString& name, QString& error);

    bool configurePlugin(const QString& name, QString& error);

    bool isRunning(const QString& name, QString& error);

    bool pluginInfo(const QString& name, PythonPluginInfo* info, QString& error);

    QStringList allPlugins(QString& error);

    bool call(const QString& func, bool* ret, QString& error, const char* format, ...);

    bool getInfoData(uint64 schid, uint64 id, int type, QStringList& ret, QString& error);

    bool addUserscriptModule(QString& name, QString& error, bool forcename = false);
    void setupUserscriptModule(const QString& module, QString& error);
    bool execUserString(const QString& str, const QString& module, QString& error);
    bool reloadUserscriptModule(const QString& module, QString& error);
    void deleteUserscriptModule(const QString& module);

    QString formatError(const QString& fallback);
  protected:
    bool getDirectories(QString& error);

    QString getNextModuleName() const;
  private:
    PythonHost();
    ~PythonHost();

    PyObject* m_pmod;
    PyObject* m_pyhost;
    PyObject* m_trace;
    PyObject* m_redirect;
    QList<PyObject*> m_modules;
    QHash<QString, PyObject*> m_usermodules;

    QDir m_plugindir;
    QDir m_incldir;
    QDir m_moddir;
};

#endif // PYTHONHOST_H
