#include "pyredirector.h"

#include "pystddispatcher.h"

static PyMethodDef redirectorfuncs[] = {
  {"write", writeToRedirector, METH_VARARGS, "Redirects the output of stdout"},
  {"flush", flushRedirector, METH_VARARGS, "Placeholder for the flush method"},
  {NULL, NULL, 0, NULL}
};

static PyModuleDef redirectordef = {
  PyModuleDef_HEAD_INIT,
  "redirector",
  "Redirects the output to the hosting c library",
  -1,
  redirectorfuncs,
  NULL,
  NULL,
  NULL,
  NULL
};

PyMODINIT_FUNC PyInit_redirector(void) {
  PyObject* m = PyModule_Create(&redirectordef);
  if (!m)
    return NULL;

  PySys_SetObject("stdout", m);
  PySys_SetObject("stderr", m);

  return m;
}

PyObject* writeToRedirector(PyObject* /* self*/ , PyObject* args) {
  char* str;

  if (!PyArg_ParseTuple(args, "s", &str))
    return NULL;

  PyObject* loc = PyEval_GetLocals();
  if (!loc || !PyDict_Check(loc)) {
    printf(str);
    Py_RETURN_NONE;
  }

  PyObject* name = PyDict_GetItemString(loc, "__name__");
  if (!name || !PyUnicode_Check(name)) {
    printf(str);
    Py_RETURN_NONE;
  }

  PyObject* pystr = PyUnicode_AsUTF8String(name);
  if (!pystr) {
    printf(str);
    Py_RETURN_NONE;
  }

  QString modname = PyBytes_AsString(pystr);
  Py_DECREF(pystr);

  pystddispatcher::instance()->messageArrived(modname, str);

  Py_RETURN_NONE;
}

PyObject* flushRedirector(PyObject* /* self */, PyObject* /* args*/) {
  return Py_BuildValue("");
}
