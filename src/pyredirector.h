#ifndef PYREDIRECTOR_H
#define PYREDIRECTOR_H

#include <Python.h>

PyObject* writeToRedirector(PyObject* self, PyObject* args);
PyObject* flushRedirector(PyObject* self, PyObject* args);


PyMODINIT_FUNC PyInit_redirector(void);

#endif // PYREDIRECTOR_H
