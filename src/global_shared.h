#ifndef GLOBAL_SHARED_H__
#define GLOBAL_SHARED_H__

#include <Python.h>

#include <QHash>
#include <QString>
#include <QStringList>

#include "pythonhost.h"

#include "ts3_functions.h"

extern struct TS3Functions funcs;
extern char* pluginid;
extern QHash<QString, QStringList> user_scripts;

#endif
