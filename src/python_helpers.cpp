#include "python_helpers.h"

PyObject* qstringlistToPyList(const QStringList& inlist, QString& error) {
  PyObject* pylist = PyList_New(0);

  if (!pylist) {
    error = QObject::tr("Error creating pylist");
    return NULL;
  }

  foreach (QString item, inlist) {
    PyObject* pyitem = Py_BuildValue("s", item.toUtf8().data());

    if (!pyitem) {
      error = QObject::tr("Error creating pystring %1").arg(item);
      Py_DECREF(pylist);
      return NULL;
    }

    if (PyList_Append(pylist, pyitem) != 0) {
      error = QObject::tr("Error appending string %1 to pylist").arg(item);
      Py_DECREF(pyitem);
      Py_DECREF(pylist);
      return NULL;
    }
    else Py_DECREF(pyitem);
  }

  return pylist;
}

bool pyListToQstringlist(PyObject* pylist, QStringList* ret, QString& error) {
  if (!PyList_Check(pylist)) {
    error = QObject::tr("not a valid pylist");
    return false;
  }

  for (Py_ssize_t i = 0; i < PyList_Size(pylist); ++i) {
    PyObject* pyitem = PyList_GetItem(pylist, i);

    if (!pyitem) {
      error = QObject::tr("Error getting string from pylist");
      return false;
    }

    ret->append(PyBytes_AsString(pyitem));
    Py_DECREF(pyitem);
  }

  return true;
}

void listDecref(const QList<PyObject*>& list) {
  foreach (PyObject* o, list)
    Py_DECREF(o);
}

PyObject* tupleFromPyObjectQlist(const QList<PyObject*>& list) {
  PyObject* ret = PyTuple_New(list.count());

  for (int i = 0; i < list.count(); ++i) {
    if (PyTuple_SetItem(ret, i, list[i]) != 0) {
      Py_DECREF(ret);
      return NULL;
    }
  }

  return ret;
}

PyObject* floatListToPyList(float* inp) {
  PyObject* pylist = PyList_New(0);

  if (!pylist)
    return NULL;

  for (int i = 0; inp[i] != 0; ++i) {
    PyObject* item = Py_BuildValue("f", *inp);

    if (!item) {
      Py_DECREF(pylist);
      return NULL;
    }

    if (PyList_Append(pylist, item) != 0) {
      Py_DECREF(item);
      Py_DECREF(pylist);
      return NULL;
    }
    else Py_DECREF(item);
  }

  return pylist;
}

PyObject* unsignedIntListToPyList(unsigned int* inp) {
  PyObject* pylist = PyList_New(0);

  if (!pylist)
    return NULL;

  for (int i = 0; inp[i] != 0; ++i) {
    PyObject* item = Py_BuildValue("i", (int)*inp);

    if (!item) {
      Py_DECREF(pylist);
      return NULL;
    }

    if (PyList_Append(pylist, item) != 0) {
      Py_DECREF(item);
      Py_DECREF(pylist);
      return NULL;
    }
    else Py_DECREF(item);
  }

  return pylist;
}

PyObject* shortListToPyList(short* inp) {
  PyObject* pylist = PyList_New(0);

  if (!pylist)
    return NULL;

  for (int i = 0; inp[i] != 0; ++i) {
    PyObject* item = Py_BuildValue("i", (int)*inp);

    if (!item) {
      Py_DECREF(pylist);
      return NULL;
    }

    if (PyList_Append(pylist, item) != 0) {
      Py_DECREF(item);
      Py_DECREF(pylist);
      return NULL;
    }
    else Py_DECREF(item);
  }

  return pylist;
}

PyObject* intListToPyList(int* inp) {
  PyObject* pylist = PyList_New(0);

  if (!pylist)
    return NULL;

  for (int i = 0; inp[i] != 0; ++i) {
    PyObject* item = Py_BuildValue("i", *inp);

    if (!item) {
      Py_DECREF(pylist);
      return NULL;
    }

    if (PyList_Append(pylist, item) != 0) {
      Py_DECREF(item);
      Py_DECREF(pylist);
      return NULL;
    }
    else Py_DECREF(item);
  }

  return pylist;
}

bool PyTupleToTS3Vector(PyObject *tuple, TS3_VECTOR *ret) {
  if (!PyTuple_Check(tuple))
    return false;

  if (PyTuple_Size(tuple) != 3)
    return false;

  if (PyTuple_Size(tuple) != 3)
    return false;

  PyObject* x = PyTuple_GetItem(tuple, 0);
  if (!x || !PyLong_Check(x)) {
    Py_XDECREF(x);
    return false;
  }
  ret->x = (float)PyLong_AsDouble(x);
  Py_DECREF(x);

  x = PyTuple_GetItem(tuple, 1);
  if (!x || !PyLong_Check(x)) {
    Py_XDECREF(x);
    return false;
  }
  ret->y = (float)PyLong_AsDouble(x);
  Py_DECREF(x);

  x = PyTuple_GetItem(tuple, 1);
  if (!x || !PyLong_Check(x)) {
    Py_XDECREF(x);
    return false;
  }
  ret->y = (float)PyLong_AsDouble(x);
  Py_DECREF(x);

  return true;
}

bool PyListToCharArray(PyObject* list, char*** ret) {
  if (!PyList_Check(list))
    return false;

  int size = PyList_Size(list);

  if (size == 0) {
    *ret = NULL;
    return true;
  }

  *ret = (char**)malloc((size +1) * sizeof(char*));

  for (int i = 0; i < size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      for (int k = i -1; k >= 0; --k)
        free(*ret[k]);
      free(*ret);
      return false;
    }

    char* str = PyBytes_AsString(it);
    Py_DECREF(it);

    *ret[i] = (char*)malloc((strlen(str) +1) * sizeof(char));
    strncpy(*ret[i], str, strlen(str));
    *ret[i][strlen(str)] = '\0';
  }

  *ret[size] = NULL;

  return true;
}

bool PyListToUint64Array(PyObject* list, uint64** ret) {
  if (!PyList_Check(list))
    return false;

  int size = PyList_Size(list);

  if (size == 0) {
    *ret = NULL;
    return true;
  }

  *ret = (uint64*)malloc((size +1) * sizeof(uint64));

  for (int i = 0; i < size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = (uint64)PyLong_AsUnsignedLongLong(it);
  }

  *ret[size] = 0;

  return true;
}

bool PyListToAnyIDArray(PyObject* list, anyID** ret) {
  if (!PyList_Check(list))
    return false;

  int size = PyList_Size(list);

  if (size == 0) {
    *ret = NULL;
    return true;
  }

  *ret = (anyID*)malloc((size +1) * sizeof(anyID));

  for (int i = 0; i < size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = (anyID)PyLong_AsUnsignedLong(it);
  }

  *ret[size] = 0;

  return true;
}

bool deviceListToPyList(char*** list, PyObject** ret) {
  *ret = PyList_New(0);

  if (!ret)
    return false;

  PyObject* tuple;
  for (int i = 0; list[i] != NULL; ++i) {
    tuple = PyTuple_New(2);

    if (!tuple) {
      Py_DECREF(*ret);
      return false;
    }

    if (PyTuple_SetItem(tuple, 0, PyBytes_FromString(list[i][0])) != 0) {
      Py_DECREF(tuple);
      Py_DECREF(*ret);
      return false;
    }

    if (PyTuple_SetItem(tuple, 1, PyBytes_FromString(list[i][1])) != 0) {
      Py_DECREF(tuple);
      Py_DECREF(*ret);
    }

    if (PyList_Append(*ret, tuple) != 0) {
      Py_DECREF(tuple);
      Py_DECREF(*ret);
      return false;
    }
    Py_DECREF(tuple);
  }

  return true;
}

bool charArrayToPyList(char** list, PyObject** ret) {
  *ret = PyList_New(0);

  if (!ret)
    return false;

  for (int i = 0; list[i] != NULL; ++i) {
    PyObject* str = PyBytes_FromString(list[i]);

    if (!str) {
      Py_DECREF(*ret);
      return false;
    }

    if (PyList_Append(*ret, str) != 0) {
      Py_DECREF(str);
      Py_DECREF(*ret);
      return false;
    }
    Py_DECREF(str);
  }

  return true;
}

bool anyIDArrayToPyList(anyID* list, PyObject** ret) {
  *ret = PyList_New(0);

  if (!ret)
    return false;

  for (int i = 0; list[i] != 0; ++i) {
    PyObject* it = Py_BuildValue("I", (unsigned int)list[i]);

    if (!it) {
      Py_DECREF(*ret);
      return false;
    }

    if (PyList_Append(*ret, it) != 0) {
      Py_DECREF(it);
      Py_DECREF(*ret);
      return false;
    }
    Py_DECREF(it);
  }

  return true;
}

bool uint64ArrayToPyList(uint64* list, PyObject** ret) {
  *ret = PyList_New(0);

  if (!ret)
    return false;

  for (int i = 0; list[i] != 0; ++i) {
    PyObject* it = Py_BuildValue("K", (unsigned long long)list[i]);

    if (!it) {
      Py_DECREF(*ret);
      return false;
    }

    if (PyList_Append(*ret, it) != 0) {
      Py_DECREF(it);
      Py_DECREF(*ret);
      return false;
    }
    Py_DECREF(it);
  }

  return true;
}

bool PyListToShortArray(PyObject* list, short** ret) {
  if (!PyList_Check(list))
    return false;

  int size = PyList_Size(list);
  *ret = (short*)malloc((size +1) * sizeof(short));

  for (int i = 0; i < size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = (short)PyLong_AsLong(it);
    Py_DECREF(it);
  }

  *ret[size] = 0;

  return true;
}

bool shortArrayToPyList(short* list, PyObject** ret, int size) {
  *ret = PyList_New(size);

  if (!ret)
    return false;

  for (int i = 0; i < size; ++i) {
    if (PyList_SetItem(*ret, i, Py_BuildValue("h", list[i])) != 0) {
      Py_DECREF(*ret);
      return false;
    }
  }

  return true;
}

bool PyListToIntArray(PyObject* list, int** ret, int* size) {
  if (!PyList_Check(list))
    return false;

  *size = PyList_Size(list);
  *ret = (int*)malloc(*size * sizeof(int));

  for (int i = 0; i < *size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = (int)PyLong_AsLong(it);
    Py_DECREF(it);
  }

  return true;
}

bool PyListToUnsignedIntArray(PyObject* list, unsigned int** ret, int* size) {
  if (!PyList_Check(list))
    return false;

  *size = PyList_Size(list);
  *ret = (unsigned int*)malloc(*size * sizeof(unsigned int));

  for (int i = 0; i < *size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = (unsigned int)PyLong_AsUnsignedLong(it);
    Py_DECREF(it);
  }

  return true;
}

bool PyListToUint64Array(PyObject *list, uint64 **ret, int* size) {
  if (!PyList_Check(list))
    return false;

  *size = PyList_Size(list);
  *ret = (uint64*)malloc(*size * sizeof(uint64));

  for (int i = 0; i < *size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = (uint64)PyLong_AsUnsignedLongLong(it);
    Py_DECREF(it);
  }

  return true;
}

bool PyListToCharArray(PyObject* list, char*** ret, int* size) {
  if (!PyList_Check(list))
    return false;

  *size = PyList_Size(list);
  *ret = (char**)malloc(*size * sizeof(char*));

  for (int i = 0; i < *size; ++i) {
    PyObject* it = PyList_GetItem(list, i);

    if (!it) {
      free(*ret);
      return false;
    }

    *ret[i] = PyBytes_AsString(it);
    Py_DECREF(it);
  }

  return true;
}

bool bookMarksToPyList(struct PluginBookmarkList* bm, PyObject** ret) {
  *ret = PyList_New(bm->itemcount);

  for (int i = 0; i < bm->itemcount; ++i) {
    PyObject* it;

    it = PyTuple_New(4);

    if (!it) {
      Py_DECREF(*ret);
      return false;
    }

    if (PyTuple_SetItem(it, 0, PyBytes_FromString(bm->items[i].name)) != 0) {
      Py_DECREF(it);
      Py_DECREF(*ret);
      return false;
    }

    if (PyTuple_SetItem(it, 1, PyLong_FromLong(bm->items[i].isFolder ? 1 : 0)) != 0) {
      Py_DECREF(it);
      Py_DECREF(*ret);
      return false;
    }

    if (bm->items[i].isFolder) {
      if (PyTuple_SetItem(it, 2, Py_None) != 0) {
        Py_DECREF(it);
        Py_DECREF(*ret);
        return false;
      }

      PyObject* childs;
      if (!bookMarksToPyList(bm->items[i].folder, &childs)) {
        Py_DECREF(it);
        Py_DECREF(*ret);
        return false;
      }

      if (PyTuple_SetItem(it, 3, childs) != 0) {
        Py_DECREF(childs);
        Py_DECREF(it);
        Py_DECREF(*ret);
        return false;
      }
      Py_DECREF(childs);
    }
    else {
      if (PyTuple_SetItem(it, 2, PyBytes_FromString(bm->items[i].uuid)) != 0) {
        Py_DECREF(it);
        Py_DECREF(*ret);
        return false;
      }

      if (PyTuple_SetItem(it, 3, Py_None) != 0) {
        Py_DECREF(it);
        Py_DECREF(*ret);
        return false;
      }
    }
  }

  return true;
}

QString pyObjectToOutputString(PyObject* o, bool incontainer) {
  QString ret;

  if (o == Py_None) {
    if (incontainer)
      ret = "None";
  }
  else if (PyList_Check(o)) {
    ret = "[";
    for (int i = 0; i < PyTuple_Size(o); ++i) {
      ret += pyObjectToOutputString(PyList_GetItem(o, i));
      ret += ", ";
    }
    ret = ret.left(-2);
    ret += "]";
  }
  else if (PyTuple_Check(o)) {
    ret = "(";
    for (int i = 0; i < PyTuple_Size(o); ++i) {
      ret += pyObjectToOutputString(PyTuple_GetItem(o, i));
      ret += ", ";
    }
    ret = ret.left(-2);
    ret += ")";
  }
  else if (PyUnicode_Check(o)) {
    PyObject* pystr = PyUnicode_AsUTF8String(o);
		
    if (pystr) {
      ret = QString("'%1'").arg(PyBytes_AsString(pystr));
      Py_DECREF(pystr);
    }
  }
  else {
    PyObject* asstr = PyObject_Str(o);
    
    if (asstr) {
      ret = PyBytes_AsString(asstr);
      Py_DECREF(asstr);
    }
  }
	
  return ret;
}

PyObject* qvariantToPyObject(const QVariant& var) {
  if (!var.isValid())
    return NULL;

  if (var.canConvert(QMetaType::Double))
    return Py_BuildValue("d", var.toDouble());
  else if (var.canConvert(QMetaType::LongLong))
    return Py_BuildValue("k", var.toLongLong());
  else if (var.canConvert(QMetaType::QString))
    return Py_BuildValue("s", var.toString().toUtf8().data());
  else if (var.canConvert(QMetaType::QVariantList)) {
    PyObject* ret = PyList_New(0);
    foreach (QVariant v, var.toList())
      PyList_Append(ret, qvariantToPyObject(v));

    return ret;
  }
  else return NULL;
}

QVariant pyObjectToQVariant(PyObject* o) {
  QVariant ret;

  if (PyList_Check(o)) {
    QVariantList list;
    for (int i = 0; i < PyList_Size(o); ++i)
      list.append(pyObjectToQVariant(PyList_GetItem(o, i)));

    ret = list;
  }
  else if (PyLong_Check(o)) {
    ret = PyLong_AsDouble(o);
  }
  else if (PyUnicode_Check(o)) {
    PyObject* pystr = PyUnicode_AsUTF8String(o);

    if (pystr) {
      ret = PyBytes_AsString(pystr);
      Py_DECREF(pystr);
    }
  }

  return ret;
}
