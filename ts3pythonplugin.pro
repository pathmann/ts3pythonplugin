#-------------------------------------------------
#
# Project created by QtCreator 2013-12-31T02:35:12
#
#-------------------------------------------------

QT += sql uitools
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = ts3pythonplugin
TEMPLATE = lib

API_VER = 20
DEFINES += API_VER=$${API_VER}

DEFINES += DEBUG_BUILD

PYTHON_VER = 3.2

INCLUDEPATH += includes/ts3_pluginsdk/$${API_VER} \
  generated/$${API_VER} \
  src \
  src/ui

SOURCES += \
  generated/$${API_VER}/auto_plugin.cpp \
  generated/$${API_VER}/ts3module.cpp

HEADERS += \
  generated/$${API_VER}/auto_plugin.h \
  generated/$${API_VER}/ts3module.h

unix {
  INCLUDEPATH += /usr/include/python$${PYTHON_VER}
  LIBS += /usr/lib/libpython$${PYTHON_VER}.a
}

SOURCES += \
  src/plugin.cpp \
  src/pythonhost.cpp \
  src/pluginconfig.cpp \
  src/python_helpers.cpp \
  src/global_shared.cpp \
  src/ts3logdispatcher.cpp \
  src/ts3helpersmodule.cpp \
  src/pyredirector.cpp \
  src/pystddispatcher.cpp \
  src/pyqobject.cpp \
  src/pysignalslotconnection.cpp \
  src/ui/configdialog.cpp \
  src/ui/pyshell.cpp \
  src/ui/shelldialog.cpp

HEADERS += \
  src/global_shared.h \
  src/plugin.h \
  src/pythonhost.h \
  src/singleton.h \
  src/pluginconfig.h \
  src/python_helpers.h \
  src/ts3logdispatcher.h \
  src/ts3helpersmodule.h \
  src/pyredirector.h \
  src/pystddispatcher.h \
  src/pyqobject.h \
  src/pysignalslotconnection.h \
  src/ui/configdialog.h \
  src/ui/pyshell.h \
  src/ui/shelldialog.h

OTHER_FILES += \
  scripts/gencode.py \
  scripts/simplec.py \
  scripts/gendocs.py \
  templates/auto_plugin.cpp.tpl \
  templates/auto_plugin.h.tpl \
  templates/ts3module.cpp.tpl \
  templates/ts3module.h.tpl \
  templates/ts3plugin.py.tpl \
  templates/ts3defines.py.tpl \
    python_plugin/scripts/ipcplugin.py \
    python_plugin/scripts/autopreviewimages.py \
    python_plugin/scripts/webdavsettings.py

FORMS += \
  src/ui/configdialog.ui \
  src/ui/shelldialog.ui
